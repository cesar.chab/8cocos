## Acerca de Marcación
 - Proyecto usando laravel version 7
 - Php 7.3
 - MariaDB 10.3

## Pasos para instalar local
- Clonar proyecto git clone git@bitbucket.org:asvnets/marcacion.git
- En la raiz del proyecto en el terminal colocar composer install
- Luego copiar el archivo .env-example y crear .env 
- Configurar los parametros de la base de datos
- luego en el terminal hacer php artisan migrate --seed 
