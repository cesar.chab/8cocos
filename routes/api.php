<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

// Route::namespace('Api')->middleware('api')->group(function() {
//     Route::group(['prefix' => 'v1'], function() {
//         Route::group(['namespace' =>'Assistance', 'prefix' =>'assistance'], function () {
//             Route::apiResource('specialities-api', 'SpecialityController');
//             Route::apiResource('project-specialities-api', 'ProjectSpecialityController');
//             Route::apiResource('assistances', 'AssistanceController');
//             Route::apiResource('activities-api', 'ActivityController');
//             Route::apiResource('job-initial', 'JobInitialController');
//             Route::apiResource('job-end', 'JobEndController');
//         });
//         Route::group(['namespace' => 'Materials', 'prefix' => 'materials'], function() {
//             Route::apiResource('quantity-available', 'QuantityController');
//         });
//         Route::group(['namespace' => 'Administrative', 'prefix' => 'administrative'], function() {
//             Route::apiResource('income-expense-amount-api', 'IncomeExpenseAmountReportController');
//         });
//         Route::group(['namespace' => 'Quotations', 'prefix' => 'quotations'], function() {
//             Route::apiResource('quotations-api', 'RegisterQuotationController');
//             Route::apiResource('lines-quotations-api', 'LineQuotationController');
//             Route::apiResource('customer-invoice-api', 'CustomerInvoiceController');
//             Route::apiResource('provider-quotation-api', 'ProviderController');
//             Route::apiResource('supplier-line-items-api', 'SupplierLineItemController');
//         });
//         Route::group(['namespace' => 'General', 'prefix' => 'general'], function() {
//             Route::get('uom-api', 'UomController@index')->name('uom-api.index');
//             Route::get('provider-api', 'ProviderController@index')->name('provider-api.index');
//             Route::apiResource('customer-project-api', 'CustomerController');
//         });



//     });
// });



