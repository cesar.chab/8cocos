<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

// Route::get('/', function () {
//     return view('contenido/contenido');
// });

Route::get('acceso', 'Auth\LoginController@showLoginForm')->name('acceso');
Route::get('tipo_perfil', 'Auth\LoginController@profile')->name('tipo_perfil');
Route::get('presonal', 'Auth\LoginController@personal')->name('personal');
Route::get('business', 'Auth\LoginController@business')->name('business');
Route::post('login', 'Auth\LoginController@login')->name('login');


Route::get('/', 'Web\IndexController@index')->name('inicio');
Route::get('nosotros', 'Web\IndexController@nosotros')->name('nosotros');
Route::get('blog', 'Web\IndexController@blog')->name('blog');
Route::get('ayuda', 'Web\IndexController@ayuda')->name('ayuda');


// Auth::routes(['verify' => true]);

// Route::get('/home', 'HomeController@index')->name('home')->middleware('verified');
// Route::get('/user/profile/show/{user}','HomeController@show')->name('home.user.profile.show');
// Route::put('/user/profile/{user}','HomeController@update')->name('home.user.profile');
// Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
// Route::get('/load-files','HomeController@upload')->name('upload.files');
// Route::get('test', function() {
//     return view('exports.example');
// });
// Route::get('help', 'HomeController@help')->name('page.help');

// Route::group(['middleware' => ['auth','isAdmin']], function () {
//     Route::group(['namespace' =>'General', 'prefix' => 'general'], function() {
//             Route::resource('speciality', 'SpecialityController');
// 		    Route::resource('dailyrate', 'DailyRateController');
//             Route::resource('position', 'PositionController');
//             Route::resource('employee', 'EmployeeController');
//             Route::resource('reason', 'ReasonController');
//             Route::resource('accountingrecord', 'AccountingPeriodController');
//             Route::resource('typedocument', 'TypeDocumentController');
//             Route::resource('customers', 'CustomerController');
//             Route::resource('fndlookup', 'FndLookupController');
//             Route::resource('fndlookup_value', 'FndLookupValueController');
//             Route::resource('files', 'FileController');
//             Route::resource('user', 'UserController');
//             Route::resource('expenses-fixed', 'ExpenseFixedController');
//             Route::resource('providers', 'ProviderController');
//             Route::resource('payment_term', 'PaymentTermController');
//             Route::resource('unit_measurement', 'UnitMeasurementController');
//             Route::resource('company_registration', 'CompanyRegistrationController');
//     });

//     Route::group(['namespace'=>'Projects', 'prefix' => 'projects'], function(){
//             Route::resource('registry-projects', 'ProjectRegistrationController');
//             Route::resource('project-speciality', 'ProjectSpecialityController');
//             Route::resource('activities', 'ActivityController');


//             Route::resource('my-report-hours', 'ProjectHourController');
//     });

//     Route::group(['namespace'=>'Administrative', 'prefix' => 'administrative'], function(){
//             Route::resource('fixed-output', 'FixedOutputController');
//             Route::resource('income-expense', 'IncomeExpenseController');
//             Route::resource('expense-project-report','ExpensesProjectController');
//             Route::get('income-expense-report', 'IncomeExpenseAmountReportController@index')->name('income-expense-report.index');

//             Route::group(['namespace'=>'AccountReceivable', 'prefix'=>'accountreceivable'], function(){
//                 Route::resource('project_payment', 'ProjectPaymentController');
//                 Route::resource('resume_project_payment', 'ResumeProjectPaymentController');
//             });

//             Route::group(['namespace'=>'ShoppingRecord', 'prefix'=>'shoppingrecord'], function(){
//                 Route::resource('purchase_made', 'PurchaseMadeController');
//             });

//     });

//     Route::group(['namespace' => 'Export', 'prefix' => 'export'], function() {
//         Route::group(['namespace' =>'Projects'], function() {
//             Route::get('report-projects', 'ProjectRegistrationController@export')->name('export.project.registration');
//             Route::get('my-report-project','MyReportProjectController@export')->name('export.my.project');
//             Route::get('report-hour-project','ProjectController@export')->name('export.hour.project');

//         });

//         Route::group(['namespace' =>'Administrative'], function() {
//             Route::get('expense-by-project', 'ExpenseProjectController@export')->name('export.expense.project');
//             Route::get('project-payment-report','ProjectPaymentController@export')->name('export.project.payment');
//         });

//         Route::group(['namespace' =>'Reports'], function() {
//             Route::get('report-activity-project','ReportActivityProjectController@export')->name('export.project.activity');
//             Route::get('report-hour-project','ProjectHourController@export')->name('export.hour.project');
//             Route::get('report-purchase-made', 'PurchaseMadeController@export')->name('export.reports.purchase');
//         });

//     });



//     Route::group(['namespace' => 'Reports', 'prefix' => 'reports'], function() {
//          Route::get('report-hours','ReportHoursController@index')->name('report.hours');
//          Route::get('report-project-activity','ReportProjectActivityController@index')->name('report.project.activity');
//     });

//     Route::group(['namespace' => 'Quotations', 'prefix' => 'quotations'], function() {
//          Route::resource('registration-quoations', 'RegisterQuotationController');
//          Route::resource('pay-services', 'PayServiceController');
//          Route::resource('invoice-receivable', 'InvoiceReceivableController');
//          Route::resource('pay-invoice', 'PayInvoiceHeaderController');
//     });

// });


// Route::group(['middleware' => 'auth'], function () {

//     Route::group(['namespace' =>'General'], function() {
//         Route::resource('directories', 'DirectoryController');
//     });
//     Route::group(['namespace'=>'Projects', 'prefix' => 'projects'], function(){
//         Route::resource('advance-projects', 'AdvanceProjectController');
//         Route::resource('my-report', 'ReportProjectController');
//     });

//     Route::group(['namespace' => 'Assistances', 'prefix' => 'assistances'], function() {
//         Route::resource('assistance', 'AssistanceController');
//     });

//     Route::group(['namespace' => 'Materials', 'prefix' => 'materials'], function() {
//         Route::resource('master_material', 'MasterMaterialController');
//         Route::resource('material_movement', 'MaterialMovementController');
//         Route::resource('type_movements', 'TypeMovementMaterialController');
//         Route::resource('material-reservation', 'MaterialReservationController');
//         Route::resource('material-stock', 'StockController');
//         Route::resource('reception-material', 'ReceptionMaterialReservationController');
//     });

// });



