<?php

namespace App;

use App\Entities\General\Directory;
use App\Entities\General\Employee;
use App\Entities\Projects\ProjectSpeciality;
use App\Notifications\MyResetPassword;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    protected $guard_name = ['web'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function employee()
    {
        return $this->hasOne(Employee::class, 'id', 'employee_id');
    }


    protected $fillable = [
        'name', 'email', 'password', 'employee_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new MyResetPassword($token));
    }
}
