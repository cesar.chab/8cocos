<?php

namespace App\Exports\Projects;

use App\Entities\Projects\ProjectRegistration;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class ProjectRegistrationExport implements FromCollection, WithHeadings, WithMapping, ShouldAutoSize
{

      /*  public function view(): View
        {
            return view('exports.project', [
                'projects_registrations' => ProjectRegistration::with([
                    'customer',
                    'employee',
                    'priority',
                    'state',
                    'type_project',
                    'project_specialities',
                    'assistances'
                ])->get()
            ]);
        }*/

    public function collection()
    {
        $projects = ProjectRegistration::query()
                    ->with([
                        'customer',
                        'employee',
                        'priority',
                        'state',
                        'type_project',
                        'project_specialities',
                        'assistances'
                    ])->get();
        return $projects;
    }

    public function map($projects) :array
    {
        $data = [
                $projects->name,
                $projects->customer->business_name,
                $projects->number_mobile,
                $projects->date_contract,
                $projects->employee->name,
                $projects->delivery_date,
                $projects->priority->name,
                '0'
            ];
            foreach($projects->project_specialities as $speciality)
            {
                    $specialities = $speciality->name;
                    $data[] = $specialities;
            }
            return $data;
    }

    public function headings(): array
    {
        $data = [
                'Nombre Proyecto',
                'Cliente',
                'Celular',
                'Fecha de Contrato',
                'Responsable del Proyecto',
                'Entrega de fecha',
                'Prioridad',
                'Porcentaje del Proyecto',
                'Especialidades',
                'Responsable',
                'Entregable',
                'Estado',
                '% De Avance Especialidades',
                'Links De Archivos',
                'Fecha de Entrega',
                'Fecha de Inicio',
                'Fecha Concluido',
                'Tiempo dedicado',
                'Observacion',
        ];



        return $data;
    }

}
