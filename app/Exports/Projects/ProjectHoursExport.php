<?php

namespace App\Exports\Projects;

use App\Entities\Assistances\Assistance;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class ProjectHoursExport implements FromCollection, ShouldAutoSize, WithHeadings, WithMapping
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $assistances = Assistance::query()->with('projects','employees','specialities','activities')
            ->employees()
            ->project()
            ->bydate()
            ->get();

        return $assistances;
    }

    public function map($assistances) :array
    {
        return [
            $assistances->date_assistance_label,
            $assistances->time_initial_label,
            $assistances->time_end_label,
            $assistances->description
        ];
    }

    public function headings(): array
    {
        return [
            'Fecha Actividad',
            'Inicio',
            'Final',
            'Descripcion'
        ];
    }

}
