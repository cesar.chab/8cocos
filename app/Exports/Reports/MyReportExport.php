<?php

namespace App\Exports\Reports;

use App\Entities\Assistances\Assistance;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class MyReportExport implements FromCollection, ShouldAutoSize, WithHeadings, WithMapping
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $assistances = Assistance::query()->with('projects','employees','specialities','activities')

            ->project()
            ->bydate()
            ->get();

        return $assistances;
    }

    public function map($assistances) :array
    {
        return [
            $assistances->date_time_initial_label,
            $assistances->date_time_end_label,
            $assistances->projects[0]->name,
            $assistances->employees[0]->name,
            $assistances->specialities[0]->name,
            $assistances->hours_label,
            $assistances->description,
            $assistances->activities->isNotEmpty() ? $assistances->activities[0]->code : ''
        ];

    }

    public function headings(): array
    {
        return [
            'Inicio',
            'Final',
            'Proyecto',
            'Personal',
            'Especialidad',
            'Horas',
            'Descripcion',
            'Actividad'
        ];
    }


}
