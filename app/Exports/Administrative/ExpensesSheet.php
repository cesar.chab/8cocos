<?php


namespace App\Exports\Administrative;


use App\Entities\Administrative\IncomeExpense;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;

class ExpensesSheet implements FromCollection, WithTitle, WithHeadings, WithMapping, ShouldAutoSize
{

    private $start;
    private $end;
    private $project_id;

    public function __construct($start, $end, int $project_id = null)
    {
        $this->start = $start;
        $this->end = $end;
        $this->project_id = $project_id;
    }

    public function collection()
    {
         $expenses = IncomeExpense::query()
                            ->with('project','reason')
                            ->whereBetween('movement_date', [Carbon::parse($this->start), Carbon::parse($this->end)])
                             ->whereHas('reason', function($q) {
                                 $q->where('reason', 'egresos');
                             })
                            ->orWhere('project_id', $this->project_id)
                            ->get();
        return $expenses;
    }

    public function headings(): array
    {
        return [
            'fecha',
            'detalle',
            'monto'
        ];
    }


    public function map($expenses): array
    {
        return [
            $expenses->movement_date,
            $expenses->detail,
            $expenses->amount
        ];
    }

    public function title(): string
    {
        return 'Gastos';
    }

}
