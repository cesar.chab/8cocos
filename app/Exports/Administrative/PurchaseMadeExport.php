<?php

namespace App\Exports\Administrative;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

use App\Entities\Administrative\ShoppingRecord\PurchaseMade;

class PurchaseMadeExport implements FromCollection, ShouldAutoSize, WithHeadings, WithMapping
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $pucharses=PurchaseMade::get();

        return $pucharses;
    }

    public function map($pucharses) :array{

        return[
            $pucharses->date,
            $pucharses->serie,
            $pucharses->number,
            $pucharses->provider->number_document,
            $pucharses->provider->provider_name,
            $pucharses->project->name,
            $pucharses->amount,
            $pucharses->IGV,
            $pucharses->purchase_total,
            $pucharses->declaved ? 'Si' : 'No'
        ];
    }

    public function headings() :array{
        return[
            'Fecha',
            'Serie',
            'Número',
            'RUC',
            'Proveedor',
            'Proyecto',
            'Base',
            'IGV',
            'Total',
            'Declarado'
        ];
    }
}
