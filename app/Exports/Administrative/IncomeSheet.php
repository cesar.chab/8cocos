<?php


namespace App\Exports\Administrative;


use App\Entities\Administrative\IncomeExpense;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;

class IncomeSheet implements FromCollection, WithTitle, WithHeadings, WithMapping, ShouldAutoSize
{
    private $start;
    private $end;
    private $project_id;

    public function __construct( $start, $end, int $project_id = null)
    {
        $this->start = $start;
        $this->end = $end;
        $this->project_id = $project_id;
    }

    public function collection()
    {
            $incomes = IncomeExpense::query()
                ->with('project','reason')
                ->whereBetween('movement_date', [Carbon::parse($this->start), Carbon::parse($this->end)])
                ->whereHas('reason', function($q) {
                    $q->where('reason', 'ingresos');
                })
                ->orWhere('project_id', $this->project_id)
                ->get();

            return $incomes;
    }

    public function map($incomes): array
    {
        return [
                $incomes->movement_date,
                $incomes->detail,
                $incomes->amount
        ];
    }

    public function headings(): array
    {
        return [
            'fecha',
            'detalle',
            'monto'
        ];
    }


    public function title(): string
    {
        return 'Ingresos';
    }

}
