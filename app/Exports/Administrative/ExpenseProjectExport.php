<?php

namespace App\Exports\Administrative;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class ExpenseProjectExport implements WithMultipleSheets
{
    use Exportable;

    protected $start;
    protected $end;
    protected $project_id;

    public function __construct($start, $end, int $project_id = null)
    {
        $this->start = $start;
        $this->end = $end;
        $this->project_id = $project_id;
    }

    public function sheets(): array
    {
        $sheets = [];

        $sheets[] = new IncomeSheet($this->start, $this->end, $this->project_id);
        $sheets[] = new ExpensesSheet($this->start, $this->end, $this->project_id);

        return $sheets;
    }


}
