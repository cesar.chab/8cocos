<?php


namespace App\Http\Middleware;


use App\User;
use Illuminate\Support\Facades\Auth;

class AdminMiddleware
{

    public function handle($request, \Closure $next)
    {
            $user = User::all()->count();
            if(!($user === 1)) {
                if(!Auth::user()->hasRole('Administrador'))
                {
                    abort('401');
                }
            }
            return $next($request);
    }

}
