<?php

namespace App\Http\Requests\Administrative\AccountReceivable;

use Illuminate\Foundation\Http\FormRequest;

class ProjectPaymentCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'project_id' => 'required',
            'amount' => 'required', 
            'number_constancy' => 'required',
            'responsable_id' => 'required',
            'comment' => 'required',
            'type_payment_id' => 'required',
            'type_voucher_id' => 'required',
            'voucher_number' => 'required'

        ];
    }

    public function messages()
    {
        return [
            'project_id.required' => 'El proyecto es requerido',
            'amount.required' => 'El importe a pagar es requerido',
            'number_constancy.required' => 'El numero de constancia es requerido',
            'responsable_id.required' => 'El responsable del cobro es requerido',
            'comment.requerid' => 'El comentario es requerido',
            'type_payment_id.required' => 'El tipo de pago es requerido',
            'type_voucher_id.required' => 'El tipo de comprobante es requerido',
            'voucher_number.required' => 'El numero de comprobante es requerido'
        ];
    }
}
