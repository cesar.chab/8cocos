<?php

namespace App\Http\Requests\Administrative;

use Illuminate\Foundation\Http\FormRequest;

class IncomeExpensesUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
            'project_id' => 'required',
            'reason_id' => 'required',
            'amount' => 'required',
            'detail' => 'required'
        ];
    }

    public function messages()
    {
        return [
           
            'project_id.required' => 'El proyecto es requerido',
            'reason_id.required' => 'El motivo es requerido',
            'amount.required' => 'El importe es requerido',
            'detail.required' => 'El detalle es requerido'
        ];
    }
}
