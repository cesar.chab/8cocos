<?php

namespace App\Http\Requests\Administrative;

use Illuminate\Foundation\Http\FormRequest;

class FixedOutputUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'expenses_id' => 'required',
            'type_expenses' => 'required',
            'amount' => 'required',
            'date' => 'required',
            'project_id' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'expenses_id.required' => 'La Categoria del Gasto fijo es requerido',
            'type_expenses.required' => 'El tipo de Gasto es requerido',
            'amount.required' => 'El importe es requerido',
            'date.required' => 'La fecha es requerida',
            'project_id.required' => 'El proyecto es requerido'
        ];
    }
}
