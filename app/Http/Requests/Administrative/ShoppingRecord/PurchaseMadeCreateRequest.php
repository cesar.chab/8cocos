<?php

namespace App\Http\Requests\Administrative\ShoppingRecord;

use Illuminate\Foundation\Http\FormRequest;

class PurchaseMadeCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date'   =>'required',
            'serie'  =>'required',
            'number' =>'required',
            'provider_id'  =>'required',
            'project_id'  =>'required',
            'amount'  =>'required',
            'IGV' =>'required'
            
        ];
    }

    public function messages()
    {
        return [
            'data.required' => 'La fecha es requerida',
            'serie.required' => 'La serie es requerida',
            'number.required' => 'El numero es requerido',
            'provider_id.required' => 'El proveedor es requerido',
            'project_id.required' => 'El proyecto es requerido',
            'amount.requerid' => 'La base es requerida',
            'IGV.required' => 'El IGV es requerido'
            
        ];
    }
}
