<?php

namespace App\Http\Requests\Projects;

use Illuminate\Foundation\Http\FormRequest;

class ActivityCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code'          => 'required|max:200',
            'description'   => 'required'
        ];
    }

    public function messages()
    {
        return [
            'code.required'         => 'La actividad es requerida',
            'code.max'              => 'La actividad soporta maximo 200 caracteres',
            'description.required'  => 'La descripcion es requerida'
        ];
    }
}
