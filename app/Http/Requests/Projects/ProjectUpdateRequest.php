<?php

namespace App\Http\Requests\Projects;

use Illuminate\Foundation\Http\FormRequest;

class ProjectUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customer_id' => 'required',
            'date_contract' => 'required',
            'employee_id' => 'required',
            'priority_id' => 'required',
            'state_id' => 'required',
            'type_project_id' => 'required',
            'delivery_date' => 'required',
            'total_project_cost' => 'required|numeric|between:0,999999.99'
        ];
    }

    public function messages()
    {
        return [
            'name.max' => 'El nombre del empleado soporta maximo 200 caracteres',
            'customer_id.required' => 'El nombre del cliente es requerido',
            'date_contract.required' => 'La fecha del contrato es requerida',
            'employee_id.required' => 'El nombre del responsable es requerido',
            'priority_id.required' => 'La prioridad del proyecto es requerida',
            'state_id.required' => 'El estado del proyecto es requerido',
            'type_project_id.required' => 'El tipo de proyecto es requerido',
            'delivery_date.required' => 'La fecha de entrega del proyecto es requerida',
            'total_project_cost.required' => 'El total del costo del proyecto es requerido',
            'total_project_cost.numeric' => 'El total del costo del proyecto solo acepta dos decimales, ejemplo:12.50'
        ];
    }
}
