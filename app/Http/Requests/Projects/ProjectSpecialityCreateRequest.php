<?php

namespace App\Http\Requests\Projects;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProjectSpecialityCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        /*
         * return [
            'employee_id' => 'required',
            'project_id'      => [
                'required',
                Rule::unique('projects_project_specialities')->where(function ($query) {
                    return $query->where('speciality_id', $this->speciality_id);
                })
            ],
            'comments'      => 'required|max:300'
        ];
         *
         * */
        return [
            'employee_id' => 'required',
            'speciality_id' => 'required',
            'comments'      => 'required|max:300'
        ];
    }

    public function messages()
    {
        return [
            'employee_id.required'   => 'El nombre del responsable es requerido',
            'speciality_id.required' => 'La especialidad del proyecto es requerida',
            'comments.required'      => 'El comentario es requerido',
            'comments.max'           => 'El comentario no debe superar los 300 caracteres'
        ];
    }
}
