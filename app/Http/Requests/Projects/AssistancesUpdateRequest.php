<?php

namespace App\Http\Requests\Projects;

use Illuminate\Foundation\Http\FormRequest;

class AssistancesUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'description'     => 'required|max:300',
            'time_initial'    => 'required',
            'time_end'        => 'required',
            'project_id'      => 'required',
            'speciality_id'   => 'required'
        ];
    }

    public function messages()
    {
        return [
            'description.required'     => 'La descripcion es requerido',
            'description.max'          => 'La descripcion  no debe superar los 300 caracteres',
            'time_initial.required'    => 'La hora inicial es requerida',
            'time_end.required'        => 'La hora final es requerido',
            'project_id.required'      => 'El proyecto es requerido',
            'speciality_id.required'   => 'La especialidad es requerida',
        ];
    }
}
