<?php

namespace App\Http\Requests\General;

use Illuminate\Foundation\Http\FormRequest;

class PaymentTermCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'     => 'required|max:200|unique:general_payments_terms',
            'time_days'    => 'required'
        ];
    }

    public function messages()
    {
        return [
            'name.required'    => 'El nombre es requerido',
            'name.unique'    => 'El nombre es unico',
            'time_days.required'          => 'El tiempo en Dias es requerido'
        ];
    }
}
