<?php

namespace App\Http\Requests\General;

use Illuminate\Foundation\Http\FormRequest;

class FndLookCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:25|unique:general_fnd_lookups',
            'description' => 'required|max:50'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'El nombre de la tabla es requerida',
            'name.max' => 'El nombre de la tabla solo soporta maximo 25 caracteres',
            'name.unique' => 'El nombre de la tabla ya existe',
            'description.required' => 'La descrepcion de la tabla es requerida'
        ];
    }
}
