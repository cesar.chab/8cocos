<?php

namespace App\Http\Requests\General;

use Illuminate\Foundation\Http\FormRequest;

class ExpenseFixedConceptUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_category' => 'required|max:100|unique:general_expenses_fixeds_concept,name_category,' . $this->expenses_fixed->id,
            'type_expenses' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'name_category.required' => 'El nombre del gasto fijo es requerido',
            'name_category.max' => 'El nombre del gasto fijo soporta maximo 100 caracteres',
            'name_category.unique' => 'El nombre del gasto fijo ya existe',
            'type_expenses' => 'Debe seleccionar el tipo de gastos fijos'
        ];
    }
}
