<?php

namespace App\Http\Requests\General;

use Illuminate\Foundation\Http\FormRequest;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|unique:users,email,'.$this->user->id,
            'name' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'email.required' => 'El email es requerido',
            'email.unique' => 'El email ya existe',
            'name.required' => 'El nombre es requerido',
        ];
    }
}
