<?php

namespace App\Http\Requests\General;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:100',
            'last_name' => 'required|max:100',
            'position_id' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'El nombre del empleado es requerido',
            'last_name.required' => 'El apellido del empleado es requerido',
            'name.max' => 'El nombre del empleado soporta maximo 100 caracteres',
            'last_name.max' => 'El apellido del empleado soporta maximo 100 caracteres',
            'position_id.required' => 'El cargo es requerido'
        ];
    }
}
