<?php

namespace App\Http\Requests\General;

use Illuminate\Foundation\Http\FormRequest;

class UnitMeasurementCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code'     => 'required|unique:general_units_measurements',
            'description'    => 'required'
        ];
    }

    public function messages()
    {
        return [
            'code.required'    => 'El codigo es requerido',
            'code.unique' => 'El codigo es unico',
            'description.required'          => 'La descripcion es requerido'
        ];
    }
}
