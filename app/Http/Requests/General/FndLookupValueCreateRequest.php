<?php

namespace App\Http\Requests\General;

use Illuminate\Foundation\Http\FormRequest;

class FndLookupValueCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:25|unique:general_fnd_lookups_values',
            'code' => 'required',
            'description' => 'required|max:50'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'El nombre del detalle es requerido',
            'name.max' => 'El nombre de la tabla solo soporta maximo 25 caracteres',
            'name.unique' => 'El nombre de la tabla ya existe',
            'description.required' => 'La descrepcion de la tabla es requerida',
            'description.max' => 'La descripcion del destalle solo soporta un maximo de 50 caracteres',
            'code.required' => 'El codigo es requerido'
        ];
    }
}
