<?php

namespace App\Http\Requests\General;

use Illuminate\Foundation\Http\FormRequest;

class AccountingPeriodUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'name' => 'required|max:100|unique:general_accounting_periods,name,' . $this->accountingrecord->id,
            'date_to' =>'required|after_or_equal:date_from',
            'date_from' =>'required'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'El nombre del periodo contable es requerido',
            'name.max' => 'El nombre del periodo contable soporta maximo 100 caracteres',
            'name.unique' => 'El nombre del periodo contable ya existe',
            'date_to.after_or_equal' => 'La fecha final no debe ser menor que la fecha inicial',
            'date_from.required' => 'La fecha del periodo contable debe ser requerida'
        ];
    }
}
