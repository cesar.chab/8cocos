<?php

namespace App\Http\Requests\General;

use Illuminate\Foundation\Http\FormRequest;

class ProviderUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'provider_name' => 'required|max:200',
            'type_document_id' => 'required',
            'number_document' => 'required|max:50|unique:general_providers,number_document,'. $this->provider->id,
            'address' => 'required',
            'phones' => 'required',
            'email' => 'required|email'
        ];
    }

    public function messages()
    {
        return [
            'provider_name.required' => 'La razon social es requerido',
            'type_document_id.required' => 'El tipo de documento es requerido',
            'number_document.max' => 'El numero de documento soporta maximo 50 caracteres',
            'number_document.unique' => 'El numero de documento ya existe',
            'provider_name.max' => 'La razon social soporta maximo 200 caracteres',
            'number_document.required' => 'El numero de documento es requerido',
            'address.required' => 'La direccion  es requerido',
            'phones.required' => 'El telefono es requerido',
            'email.required' => 'El email es requerido'
        ];
    }
}
