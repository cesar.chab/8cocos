<?php

namespace App\Http\Requests\General;

use Illuminate\Foundation\Http\FormRequest;

class CustomerUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'business_name' => 'required|max:200|unique:general_customers,business_name,'. $this->customer->id,
            'type_document_id' => 'required',
            'number_document' => 'required|max:50|unique:general_customers,number_document,'. $this->customer->id,
            'address' => 'required',
            'phones' => 'required',
            'email' => 'required|email'
        ];
    }

    public function messages()
    {
        return [
            'business_name.required' => 'La razon social es requerido',
            'business_name.unique' => 'La razon social ya se encuentra registrado',
            'type_document_id.required' => 'El tipo de documento es requerido',
            'number_document.max' => 'El numero de documento soporta maximo 50 caracteres',
            'business_name.max' => 'La razon social soporta maximo 200 caracteres',
            'number_document.required' => 'El numero de documento es requerido',
            'address.required' => 'La direccion  es requerido',
            'phones.required' => 'El telefono es requerido',
            'email.required' => 'El email es requerido'
        ];
    }
}
