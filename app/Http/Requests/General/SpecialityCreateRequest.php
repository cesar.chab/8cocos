<?php

namespace App\Http\Requests\General;

use Illuminate\Foundation\Http\FormRequest;

class SpecialityCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:100'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'El nombre del especialista es requerido',
            'name.max' => 'El nombre del especialista soporta maximo 100 caracteres'
        ];
    }
}
