<?php

namespace App\Http\Requests\Quotations;

use Illuminate\Foundation\Http\FormRequest;

class InvoiceReceivableCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'quotation_id'   => 'required',
            'type_doc'        => 'required',
            'number_invoice'  =>'required|unique:cot_quotation_invoice_receivables',
            'invoice_amount'  =>'required',
            'date_issue'    =>'required',
            'invoice_status' =>'required'
        ];
    }


    public function messages()
    {
        return [
            'quotation_id.required'                => 'La cotizacion es requerida',
            'type_doc.required'                    => 'El tipo de documento es requerido',
            'number_invoice.required'              => 'El  numero de factuira es requerido',
            'number_invoice.unique'                =>'El numero de factura es unico',
            'invoice_amount.required'              =>'El importe de la factura es requerido',
            'date_issue.required'                  =>'La fecha de emision es requerida',
            'invoice_status.required'              =>'El estado de la factura es requerido'
        ];
    }
}
