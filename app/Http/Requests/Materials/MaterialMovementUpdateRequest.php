<?php

namespace App\Http\Requests\Materials;

use Illuminate\Foundation\Http\FormRequest;

class MaterialMovementUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'material_id'  => 'required',
            'project_id'   => 'required',
            'employee_id'  => 'required',
            'quantity'     => 'required',
            'movement'     => 'required'
        ];
    }

    public function messages()
    {
        return [
            'material_id.required'    => 'El material fue actualizado con exito',
            'project_id.required' => 'El proyecto fue actualizado con exito',
            'employee_id.required' => 'El responsable fue actualizado con exito',
            'quantity.required' => 'La cantidad fue actualizado con exito',
            'movement.required' => 'El movimiento fue actualizado con exito'
            
        ];
    }
}
