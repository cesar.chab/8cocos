<?php

namespace App\Http\Requests\Materials;

use Illuminate\Foundation\Http\FormRequest;

class TypeMovementUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'code' => 'required',
            'description' => 'required',
            'stock' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'name.required'    => 'El nombre es requerido',
            'code.required' => 'El codigo es requerido',
            'description.required' => 'La descripcion es requerida',
            'stock.required'  => 'El tipo de stock es requerido'
        ];
    }
}
