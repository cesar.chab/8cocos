<?php

namespace App\Http\Requests\Materials;

use Illuminate\Foundation\Http\FormRequest;

class MasterMaterialCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'name'     => 'required|max:200',
                'unit_measurement_id'   => 'required',
                'unit_cost'           => 'required',
                'state_material_id' => 'required',
                'type_material_id' => 'required'
                
        ];
    }

    public function messages()
    {
        return [
            'name.required'    => 'El nombre es requerido',
            'unit_measurement_id.required' => 'La unidad de medida es requerida',
            'name.max'         => 'El nombre soporta maximo 200 caracteres',
            'unit_cost.required'  => 'El costo unitario es requerido',
            'state_material_id.required' => 'El estado del material es requerido',
            'type_material_id.required' => 'El tipo de material es requerido'
        ];
    }
}
