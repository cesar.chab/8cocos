<?php

namespace App\Http\Requests\Materials;

use Illuminate\Foundation\Http\FormRequest;

class MaterialReservationCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'information_table.*.material_id'  => 'required',
            'project_id'                       => 'required',
            'employee_id'                      => 'required',
            'comment'                          => 'required',
            'information_table.*.quantity'     => 'required',
            'type_movement_id'                 => 'required'
        ];
    }

    public function messages()
    {
        return [
            'material_id.required'    => 'El material es requerido',
            'project_id.required' => 'El proyecto es requerido',
            'employee_id.required' => 'El responsable es requerido',
            'quantity.required' => 'La cantidad es requerida',
            'type_movement_id.required' => 'El movimiento es requerido',
            'comment.required' => 'El comentario es requerido'

        ];
    }
}
