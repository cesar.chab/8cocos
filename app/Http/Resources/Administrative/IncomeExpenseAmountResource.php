<?php

namespace App\Http\Resources\Administrative;

use Illuminate\Http\Resources\Json\JsonResource;

class IncomeExpenseAmountResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'amount_income' => $this->whenLoaded('incomeexpense', function(){
                                return $this->incomeexpense->where('reason_id', 1)->sum('amount');
            }),
            'amount_expense' => $this->whenLoaded('incomeexpense', function(){
                                return $this->incomeexpense->where('reason_id', 2)->sum('amount');
            }),
            'movement_date' => '2020/03/02'
        ];
    }
}
