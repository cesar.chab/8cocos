<?php

namespace App\Http\Resources\Projects;

use Illuminate\Http\Resources\Json\JsonResource;

class AdvanceProjectResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                         => $this->id,
            'percentage'                 => $this->percentage,
            'project_specialities'       => ProjectSpecialitiesResource::collection($this->whenLoaded('project_specialities'))
        ];
    }
}
