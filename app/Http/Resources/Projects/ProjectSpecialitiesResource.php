<?php

namespace App\Http\Resources\Projects;

use App\Http\Resources\Assistance\SpecialityResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ProjectSpecialitiesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id'                       => $this->id,
            'directory_id'             => $this->directory_id,
            'speciality_id'            => $this->speciality_id,
            'project_id'               => $this->project_id,
            'employee_id'              => $this->employee_id,
            'total_avanced_percentage' => $this->advances_projects()->sum('percentage'),
            'speciality'               => $this->speciality,
            'advances_projects'        => AdvanceProjectResource::collection($this->whenLoaded('advances_projects'))
        ];
    }
}
