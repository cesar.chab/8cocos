<?php

namespace App\Http\Resources\Assistance;

use App\Http\Resources\Projects\AdvanceProjectResource;
use App\Http\Resources\Projects\ProjectSpecialitiesResource;
use Illuminate\Http\Resources\Json\JsonResource;

class SpecialityResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
                'id'                  => $this->id,
                'name'                => $this->name,
                'projects_speciality' => ProjectSpecialitiesResource::collection($this->whenLoaded('projects_speciality'))
        ];
    }
}
