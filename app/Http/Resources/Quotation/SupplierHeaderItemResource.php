<?php

namespace App\Http\Resources\Quotation;

use Illuminate\Http\Resources\Json\JsonResource;

class SupplierHeaderItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                      => $this->id,
            'supplier_category_item'  => SupplierCategoryItemResource::collection($this->whenLoaded('supplier_category_item'))
        ];
    }
}
