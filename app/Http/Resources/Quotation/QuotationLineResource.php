<?php

namespace App\Http\Resources\Quotation;

use Illuminate\Http\Resources\Json\JsonResource;

class QuotationLineResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
          'id'                  => $this->id,
          'quotation'           => new QuotationHeaderResource($this->whenLoaded('quotation')),
          'description'         => $this->description,
          'unit_price'          => $this->unit_price,
          'supplier_line_item'  => SupplierLineItemResource::collection($this->whenLoaded('supplier_line_item')),
          'total'               =>  $this->whenLoaded('supplier_line_item', function() {
                                        return $this->supplier_line_item->sum('unit_price');
                                    })
        ];
    }
}
