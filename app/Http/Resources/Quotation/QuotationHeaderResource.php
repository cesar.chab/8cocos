<?php

namespace App\Http\Resources\Quotation;

use Illuminate\Http\Resources\Json\JsonResource;

class QuotationHeaderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'project_id' => $this->project_id,
            'customer_id' => $this->customer_id,
            'terms_id'    => $this->terms_id,
            'status_quotation_id' => $this->status_quotation,
            'notes' => $this->notes,
            'reference' => $this->reference,
            'date_quotation' => $this->date_quotation,
            'customer_to' => $this->customer_to,
            'version' => $this->version,
            'terms_note' => $this->terms_note,
            'value_percent_expenses' => $this->value_percent_expenses,
            'value_utility' => $this->value_utility,
            'payment_term_id' => $this->payment_term_id,
            'id' => $this->id,
            'supplier_line_item' => SupplierLineItemResource::collection($this->whenLoaded('supplier_line_item')),
            'total' => $this->whenLoaded('supplier_line_item', function() {
                    return $this->supplier_line_item->sum('price_vendor');
            })
        ];
    }
}
