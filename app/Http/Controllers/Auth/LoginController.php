<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Validator;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Funcion para retornar la vista login
     */
    // public function showLoginForm() {
    //     return view('auth.login');
    // }

    public function login(Request $request)
    {
        $this->validateLogin($request);

        //Verificar si el usuario y la contraseña son correctas 
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            return redirect()->route('main');
        }

        return back()
            ->withErrors(['email' => 'Cuenta no encontrado o el usuario no esta activo'])
            ->withInput(request(['email']));
    }

    public function profile () {
        return view('auth.profile');
    }

    public function personal() {
        return view('auth.personal');
    }

    public function business() {
        return view('auth.business');
    }

    public function logout(Request $request)
    {
        Auth::logout();
        $request->session()->invalidate();
        return redirect('/');
    }
}
