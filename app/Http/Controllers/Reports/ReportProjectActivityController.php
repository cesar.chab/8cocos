<?php

namespace App\Http\Controllers\Reports;

use App\Entities\Assistances\Assistance;
use App\Entities\Projects\ProjectRegistration;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ReportProjectActivityController extends Controller
{
   public function index()
   {
       $assistances = Assistance::query()->with('projects','employees','specialities','activities')

           ->bydate()
           ->project()
           ->get();

       $projects = ProjectRegistration::with('state','project_specialities')
           ->state('EP')

           ->get();

       //dd($assistances);

       return view('reports.project_activities.index', compact('projects','assistances'));
   }
}