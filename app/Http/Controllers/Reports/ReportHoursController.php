<?php

namespace App\Http\Controllers\Reports;

use App\Entities\Assistances\Assistance;
use App\Entities\Projects\ProjectRegistration;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ReportHoursController extends Controller
{
    public function index()
    {
        $assistances = Assistance::query()->with('projects','employees','specialities','activities')
            //->employees()
            ->bydate()
            ->project()
            ->get();

        $projects = ProjectRegistration::with('state','project_specialities')
            ->state('EP')
            //->employee()
            ->get();

        return view('reports.project_hours.index', compact('projects','assistances'));
    }
}