<?php

namespace App\Http\Controllers\Api\General;

use App\Entities\Quotations\QuotationLine;
use App\Entities\Quotations\SupplierCategoryItem;
use App\Entities\Quotations\SupplierHeaderItem;
use App\Entities\Quotations\SupplierLineItem;
use App\Http\Controllers\Controller;

class ProviderController extends Controller
{
    public function index()
    {
        $quotations_lines = QuotationLine::with(['supplier_header'])->where('header_id', \request('quotation'))->first();

        $supplier_header = SupplierHeaderItem::where('line_id', $quotations_lines->id)->first();

        $supplier_category = SupplierCategoryItem::where('header_id', $supplier_header->id)->first();

        $supplier_lines = SupplierLineItem::with('provider')->where('category_id', $supplier_category->id)->get();

        $providers = [];

        foreach($supplier_lines as $lines)
        {
            if($lines->provider)
            {
                    if(count($providers) === 0)
                    {
                        $provider = ['label' => $lines->provider->provider_name, 'id' => $lines->provider->id];
                        array_push($providers, $provider);
                    }else{
                        for($x = 0, $xMax = count($providers); $x < $xMax; $x++)
                        {
                            if($providers[$x]["id"] !== $lines->provider->id)
                            {
                                $provider = ['label' => $lines->provider->provider_name, 'id' => $lines->provider->id];
                                array_push($providers, $provider);
                            }
                        }
                    }
            }
        }

        return response()->json(['response' => $providers]);
    }




}
