<?php

namespace App\Http\Controllers\Administrative\ShoppingRecord;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Entities\Administrative\ShoppingRecord\PurchaseMade;
use App\Entities\General\Provider;
use App\Entities\Projects\ProjectRegistration;
use App\Http\Requests\Administrative\ShoppingRecord\PurchaseMadeCreateRequest;
use App\Http\Requests\Administrative\ShoppingRecord\PurchaseMadeUpdateRequest;

class PurchaseMadeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $purchase_mades = PurchaseMade::all();

        return view('administrative.shopping_record.purchase_made.index', compact('purchase_mades'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(PurchaseMade $purchase_made)
    {
        $providers = Provider::select('provider_name', 'id')->get();
        $projects = ProjectRegistration::select('name', 'id')->get();

        return view('administrative.shopping_record.purchase_made.create', compact('purchase_made','providers', 'projects'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PurchaseMadeCreateRequest $request)
    {
        $purchase_made = PurchaseMade::create([
            'date' =>$request->get('date'),
            'serie' =>$request->get('serie'),
            'number' =>$request->get('number'),
            'provider_id' =>$request->get('provider_id'),
            'amount' =>$request->get('amount'),
            'IGV' =>$request->get('IGV'),
            'declaved' =>$request->has('declaved') ? 1 : 0,
            'project_id' =>$request->get('project_id'),
        ]);

        flash()->success('La compra ha sido registrada con exito');

        return redirect()->route('purchase_made.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(PurchaseMade $purchase_made)
    {
        $providers = Provider::select('provider_name', 'id')->get();
        $projects = ProjectRegistration::select('name', 'id')->get();
        return view('administrative.shopping_record.purchase_made.edit', compact('purchase_made','providers', 'projects'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PurchaseMadeUpdateRequest $request, PurchaseMade $purchase_made)
    {
        $purchase_made->update([
            'date' =>$request->get('date'),
            'serie' =>$request->get('serie'),
            'number' =>$request->get('number'),
            'provider_id' =>$request->get('provider_id'),
            'amount' =>$request->get('amount'),
            'IGV' =>$request->get('IGV'),
            'declaved' =>$request->has('declaved') ? 1 : 0,
            'project_id' =>$request->get('project_id')
        ]);

        flash()->success('La compra ha sido actualizada con exito');

        return redirect()->route('purchase_made.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(PurchaseMade $purchase_made)
    {
        flash()->success('El Registro de Compra ha sido eliminado con exito');

        $purchase_made->delete();

        return redirect()->route('purchase_made.index');
    }
}
