<?php

namespace App\Http\Controllers;

use App\Entities\General\Directory;
use App\Http\Requests\General\UserUpdateRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('contenido/contenido');
    }

    public function nosotros(){
        return view('web/nosotros');
    }


    public function blog() {
        return view('web/blog');
    }

    public function ayuda() {
        return view('web/ayuda');
    }

    public function login () {
        return view('login');
    }

    public function register () {
        return view('register');
    }

    public function show(User $user)
    {
        return view('general.user.profile', compact('user'));
    }

    public function update(Request $request, User $user)
    {

        if($request->filled('password'))
        {
            $user->update([
                'password' => Hash::make($request->get('password'))
            ]);
        }

        flash()->success('El usuario '.$user->name .' cambio su contraseña con exito');

        return redirect()->route('home');
    }

    /*
     * Function for upload file in directory of system storage configurated
     * parameters $request directory_id
     * */
    public function upload(Request $request)
    {
        $directory_id = $request->get('directory_id');
        $directories = Directory::find($directory_id);
        return view('projects.advance.files-load', compact('directory_id', 'directories'));
    }

    public function help()
    {
        return view('help');
    }
}
