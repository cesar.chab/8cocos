<?php

namespace App\Http\Controllers\General;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Entities\General\Provider;
use App\Entities\General\TypeDocument;
use App\Http\Requests\General\ProviderCreateRequest;
use App\Http\Requests\General\ProviderUpdateRequest;



class ProviderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $providers = Provider::with('type_document')->get();

        return view('general.provider.index', compact('providers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Provider $provider)
    {
        $type_documents = TypeDocument::where('status', 1)->get();

        return view('general.provider.create', compact('type_documents','provider'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProviderCreateRequest $request)
    {
        $providerCreate = Provider::create([
            'provider_name' => $request->provider_name,
            'type_document_id' => $request->type_document_id,
            'number_document' => $request->number_document,
            'address' => $request->address,
            'phones' => $request->phones,
            'email' => $request->email,
            'status' => $request->has('status') ? 1 : 0
        ]);
        flash()->success('El proveedor '.$providerCreate->provider_name .' ha sido registrado con exito');

        return redirect()->route('providers.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Provider $provider)
    {
        $type_documents = TypeDocument::where('status', 1)->get();

        return view('general.provider.edit', compact('type_documents','provider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProviderUpdateRequest $request, Provider $provider)
    {
        $provider->update([
            'provider_name' => $request->provider_name,
            'type_document_id' => $request->type_document_id,
            'number_document' => $request->number_document,
            'address' => $request->address,
            'phones' => $request->phones,
            'email' => $request->email,
            'status' => $request->has('status') ? 1 : 0
        ]);
        flash()->success('El proveedor '.$provider->provider_name .' ha sido actualizado con exito');

        return redirect()->route('providers.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Provider $provider)
    {
        flash()->success('El proveedor '. $provider->provider_name .' ha sido eliminado con exito');

        $provider->delete();

        return redirect()->route('providers.index');
    }
}
