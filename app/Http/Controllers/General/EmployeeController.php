<?php

namespace App\Http\Controllers\General;

use App\Entities\General\Employee;
use App\Entities\General\Position;
use App\Http\Controllers\Controller;
use App\Http\Requests\General\EmployeeCreateRequest;
use App\Http\Requests\General\EmployeeUpdateRequest;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = Employee::with('position')->get();

        return view('general.employee.index', compact('employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Employee $employee)
    {
        $positions = Position::where('status', 1)->get();

        return view('general.employee.create', compact('employee','positions'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeCreateRequest $request)
    {
        // convert first letter uppercase
        $firstUpperCaseName= ucwords($request->get('name'));
        $firstUpperCaseLastName = ucwords($request->get('last_name'));
        $full_name_complete = $firstUpperCaseName . " ". $firstUpperCaseLastName;
        $codeLetter = $this->getInitials($full_name_complete);
        $findCodeEmployee = Employee::where('code', $codeLetter)->first();

        $employee = Employee::create([
            'name'=> $request->get('name'),
            'last_name' => $request->get('last_name'),
            'apel_name' => $full_name_complete,
            'code' => is_null($findCodeEmployee) ? strtoupper($codeLetter) :  strtoupper($codeLetter) . Employee::count(),
            'position_id' => $request->get('position_id'),
            'responsable' => $request->has('responsable') ? 1 : 0,
        ]);

        flash()->success('El empleado '.$employee->name .' Ha sido registrado con exito');

        return redirect()->route('employee.index');
    }

    /**
     * Convert get full name firts letter.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    function getInitials($string = null) {
        return array_reduce(
            explode(' ', $string),
            function ($initials, $word) {
                return sprintf('%s%s', $initials, substr($word, 0, 1));
            },
            ''
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        $positions = Position::where('status', 1)->get();
        return view('general.employee.edit', compact('employee','positions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EmployeeUpdateRequest $request, Employee $employee)
    {
        $firstUpperCaseName= ucwords($request->get('name'));
        $firstUpperCaseLastName = ucwords($request->get('last_name'));
        $full_name_complete = $firstUpperCaseName . " ". $firstUpperCaseLastName;

        $employee->update([
            'name'=>$request->get('name'),
            'last_name' => $request->get('last_name'),
            'apel_name' => $full_name_complete,
            'position_id' => $request->get('position_id'),
            'responsable' => $request->has('responsable') ? 1 : 0,
        ]);

        flash()->success('El empleado '.$employee->name .' Ha sido actualizado con exito');

        return redirect()->route('employee.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        flash()->success('El empleado '. $employee->name .' Ha sido eliminada con exito');

        $employee->delete();

        return redirect()->route('employee.index');
    }
}
