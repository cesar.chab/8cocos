<?php

namespace App\Http\Controllers\General;

use App\Entities\General\CompanyRegistration;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CompanyRegistrationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $company_registrations = CompanyRegistration::all();

        return view('general.company_registration.index', compact('company_registrations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(CompanyRegistration $company_registration)
    {
        return view('general.company_registration.create', compact('company_registration'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $base = 'settings';
        $path_image_logo = $request->file('path_image_logo');
       
        $path_image_bg = $request->file('path_image_bg');
        $path_name = 'general';
        $path = storage_path($base.'/'.$path_name);

        if($path_image_logo)
        {
            $path_image_logo->move($path, $path_image_logo->getClientOriginalName());
           $imagen_logo =  $path .'/'.$path_image_logo->getClientOriginalName();
        }

        if($path_image_bg)
        {
            $path_image_bg->move($path, $path_image_bg->getClientOriginalName());
            $imagen_bg = $path .'/'.$path_image_bg->getClientOriginalName();
        }

        $company_registration = CompanyRegistration::create([
            'name'                       => $request->get('name'),
            'bill_to_name'               => $request->get('bill_to_name'),
            'number_identification'      => $request->get('number_identification'),
            'address'                    => $request->get('address'),
            'city'                       => $request->get('city'),
            'code_postal'                => $request->get('code_postal'),
            'email'                      => $request->get('email'),
            'currency_code'              => $request->get('currency_code'),
            'tax_value'                  => $request->get('tax_value'),
            'desc_long_currency'         => $request->get('desc_long_currency'),
            'invoice_number'             => $request->get('invoice_number'),
            'invoice_serial'             => $request->get('invoice_serial'),
            'invoice_last_number'        => $request->get('invoice_last_number'),
            'invoice_bol_number'         => $request->get('invoice_bol_number'),
            'invoice_bol_serial'         => $request->get('invoice_bol_serial'),
            'invoice_bol_last_number'    => $request->get('invoice_bol_last_number'),
            'item_include_tax'           =>  $request->has('item_include_tax') ? 'Y' : 'N',
            'path_image_bg'              => $path_image_bg ? $imagen_bg : null,
            'path_image_logo'            => $path_image_logo ? $imagen_logo : null,
            'longitud'                   => $request->get('longitud'),
            'telephone'                  => $request->get('telephone'),
            'condition'                  => 1,
            'description'                => $request->get('description'),
            'last_updated_by'            => auth()->id(),
            'created_by'                 => auth()->id()
        ]);

        flash()->success('Ha sido registrado con exito');

        return redirect()->route('company_registration.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(CompanyRegistration $company_registration)
    {
        return view('general.company_registration.edit', compact('company_registration'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CompanyRegistration $company_registration)
    {
        $base = 'settings';
        $path_image_logo = $request->file('path_image_logo');
       
        $path_image_bg = $request->file('path_image_bg');
        $path_name = 'general';
        $path = storage_path($base.'/'.$path_name);

        if($path_image_logo)
        {
            $path_image_logo->move($path, $path_image_logo->getClientOriginalName());
           $imagen_logo =  $path .'/'.$path_image_logo->getClientOriginalName();
        }

        if($path_image_bg)
        {
            $path_image_bg->move($path, $path_image_bg->getClientOriginalName());
            $imagen_bg = $path .'/'.$path_image_bg->getClientOriginalName();
        }


        $company_registration->update([
            'name'                       => $request->get('name'),
            'bill_to_name'               => $request->get('bill_to_name'),
            'number_identification'      => $request->get('number_identification'),
            'address'                    => $request->get('address'),
            'city'                       => $request->get('city'),
            'code_postal'                => $request->get('code_postal'),
            'email'                      => $request->get('email'),
            'currency_code'              => $request->get('currency_code'),
            'tax_value'                  => $request->get('tax_value'),
            'desc_long_currency'         => $request->get('desc_long_currency'),
            'invoice_number'             => $request->get('invoice_number'),
            'invoice_serial'             => $request->get('invoice_serial'),
            'invoice_last_number'        => $request->get('invoice_last_number'),
            'invoice_bol_number'         => $request->get('invoice_bol_number'),
            'invoice_bol_serial'         => $request->get('invoice_bol_serial'),
            'invoice_bol_last_number'    => $request->get('invoice_bol_last_number'),
            'item_include_tax'           =>  $request->has('item_include_tax') ? 'Y' : 'N',
            'path_image_bg'              => $path_image_bg ? $imagen_bg : $company_registration->path_image_bg,
            'path_image_logo'            => $path_image_logo ? $imagen_logo : $company_registration->$imagen_logo,
            'longitud'                   => $request->get('longitud'),
            'telephone'                  => $request->get('telephone'),
            'condition'                  => 1,
            'description'                => $request->get('description'),
            'last_updated_by'            => auth()->id(),
            'created_by'                 => auth()->id()
        ]);

        flash()->success(' Ha sido actualizado con exito');

        return redirect()->route('company_registration.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(CompanyRegistration $company_registration)
    {
        flash()->success('El registro ha sido eliminado con exito');

        $company_registration->delete();

        return redirect()->route('company_registration.index');
    }
}
