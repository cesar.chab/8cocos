<?php

namespace App\Http\Controllers\General;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Requests\General\PaymentTermCreateRequest;
use App\Http\Requests\General\PaymentTermUpdateRequest;
use App\Entities\General\PaymentTerm;

class PaymentTermController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $payment_terms = PaymentTerm::all();

        return view('general.paymentsterms.index', compact('payment_terms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(PaymentTerm $payment_term)
    {
        return view('general.paymentsterms.create', compact('payment_term'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PaymentTermCreateRequest $request)
    {
        $paymenttermCreate = PaymentTerm::create([
            'name' => $request->name,
            'time_days' => $request->time_days,
            'status' => $request->has('status') ? 1 : 0
        ]);
        flash()->success('El termino de pago '.$paymenttermCreate->name .' ha sido registrado con exito');

        return redirect()->route('payment_term.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(PaymentTerm $payment_term)
    {
        return view('general.paymentsterms.edit', compact('payment_term'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PaymentTermUpdateRequest $request, PaymentTerm $payment_term)
    {
        $payment_term->update([
            'name' => $request->name,
            'time_days' => $request->time_days,
            'status' => $request->has('status') ? 1 : 0
        ]);
        flash()->success('El Termino de Pago '.$payment_term->name .' ha sido actualizado con exito');

        return redirect()->route('payment_term.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(PaymentTerm $payment_term)
    {
        flash()->success('El Termino de Pago '. $payment_term->name .' ha sido eliminada con exito');

        $payment_term->delete();

        return redirect()->route('payment_term.index');
    }
}
