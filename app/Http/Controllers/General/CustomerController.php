<?php

namespace App\Http\Controllers\General;

use App\Entities\General\Customer;
use App\Entities\General\TypeDocument;
use App\Http\Controllers\Controller;
use App\Http\Requests\General\CustomerCreateRequest;
use App\Http\Requests\General\CustomerUpdateRequest;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = Customer::with('type_document')->get();

        return view('general.customers.index', compact('customers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Customer $customer)
    {
        $type_documents = TypeDocument::where('status', 1)->get();

        return view('general.customers.create', compact('type_documents','customer'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CustomerCreateRequest $request)
    {
        $customerCreate = Customer::create([
            'business_name' => $request->business_name,
            'type_document_id' => $request->type_document_id,
            'number_document' => $request->number_document,
            'address' => $request->address,
            'phones' => $request->phones,
            'email' => $request->email,
            'status' => $request->has('status') ? 1 : 0
        ]);
        flash()->success('El cliente '.$customerCreate->business_name .' Ha sido registrado con exito');

        return redirect()->route('customers.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Customer $customer)
    {
        $type_documents = TypeDocument::where('status', 1)->get();

        return view('general.customers.edit', compact('type_documents','customer'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CustomerUpdateRequest $request, Customer $customer)
    {
        $customer->update([
            'business_name' => $request->business_name,
            'type_document_id' => $request->type_document_id,
            'number_document' => $request->number_document,
            'address' => $request->address,
            'phones' => $request->phones,
            'email' => $request->email,
            'status' => $request->has('status') ? 1 : 0
        ]);
        flash()->success('El cliente '.$customer->business_name .' Ha sido actualizado con exito');

        return redirect()->route('customers.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customer $customer)
    {
        flash()->success('El cliente '. $customer->name .' Ha sido eliminada con exito');

        $customer->delete();

        return redirect()->route('customers.index');
    }
}
