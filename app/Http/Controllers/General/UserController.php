<?php

namespace App\Http\Controllers\General;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Http\Requests\General\UserCreateRequest;
use App\Http\Requests\General\UserUpdateRequest;
use Illuminate\Support\Facades\Hash;
use App\Entities\General\Employee;
use Spatie\Permission\Models\Role;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::get();

        return view('general.user.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(User $user)
    {
        $employees=Employee::all();
        $roles = Role::all();

        return view('general.user.create', compact('user', 'employees', 'roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserCreateRequest $request)
    {
        $user = User::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password')),
            'employee_id' => $request->get('employee_id')
        ]);

        if($request->get('rol'))
        {
            $user->roles()->attach($request->get('rol')); // $user->syncRoles('Administrador');
        }


        flash('El registro '. $user->name.' Ha sido creado con exito')->success();

        return redirect()->route('user.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('general.user.profile', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $users = User::select('employee_id')->where('employee_id', '<>', null)->get();
        $employees = Employee::whereNotIn('id', $users)->get();
        $roles = Role::all();

        return view('general.user.edit', compact('user','employees', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $request, User $user)
    {
        $user->update([
            'name'  => $request->get('name'),
            'email' => $request->get('email')
        ]);

        if(!is_null($request->get('password')))
        {
            $user->update([
                'password' => Hash::make($request->get('password'))
            ]);
        }

        if(!is_null($request->get('employee_id')))
        {
            $user->update([
                'employee_id' => $request->get('employee_id')
            ]);
        }

        flash()->success('El nombre '.$user->name .' Ha sido actualizado con exito');

        return redirect()->route('user.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
