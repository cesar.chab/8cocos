<?php

namespace App\Http\Controllers\General;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Entities\General\DailyRate;
use App\Entities\General\Http\Requests;
use App\Entities\General\ConversionType;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\DailyRateFormRequest;
use DB;

use Fpdf;

class DailyRateController extends Controller
{
    
    public function index(Request $request)
    {
         
        $daily = DailyRate::orderBy('conversion_date', 'desc')->get();
        return view('general.dailyrate.index')->with('dailys', $daily);
      
    }

     

    public function create()
    {   // $type=DB::table('gl_daily_conversion_types')->where('enable_flag','=','1')->get();
        $type = ConversionType::where('enable_flag','=','1')->lists('conversion_type', 'rowid');

        return view('general.dailyrate.create')->with('convtype', $type) ;
    }
    public function store (DailyRateFormRequest $request)
    {
        $request = $request->all();

        $request['created_by'] = Auth()->user()->id;
        $request['last_updated_by'] = Auth()->user()->id;

        $daily = new DailyRate($request);
        $daily->save();

        Flash::success("Se ha registrado de manera exitosa! Tipo de Cambio")->important();
        return redirect()->route('cg.dailyrate.index');

    }

    public function storePeriodo (DailyRateFormRequest $request)
    {
        $request = $request->all();
        $date = $request['conversion_date'];
        $end_date = $request['conversion_date2'];

        dd($date);
        while (strtotime($date) <= strtotime($end_date)) {
                echo "$date\n";
                $date = date ("Y-m-d", strtotime("+1 day", strtotime($date)));
    }


        $request['created_by'] = Auth()->user()->id;
        $request['last_updated_by'] = Auth()->user()->id;

        $daily = new DailyRate($request);
        $daily->save();

        Flash::success("Se ha registrado de manera exitosa! Tipo de Cambio")->important();
        return redirect()->route('cg.dailyrate.index');

    }

    public function edit($id)
    {

        $convtype = ConversionType::where('enable_flag','=','1')->lists('conversion_type', 'rowid');
        $daily = DailyRate::find($id);


        return view('cg.dailyrate.edit', compact('daily', 'convtype' ));
    }

    public function update(DailyRateFormRequest $request, DailyRate $daily)

    {   $request = $request->all();
        $request['last_updated_by'] = Auth()->user()->id;

        //RETURN $request;

        $daily->update($request);
        //Flash::success("El Tipo de Cambio ha sido editado con exito!")->important();

        return redirect()->route('cg.dailyrate.index');

    }

    public function destroy($id)
    {
        $categoria=DailyRate::findOrFail($id);
        $categoria->condicion='0';
        $categoria->update();
        return Redirect::to('cg/dailyrate');
    }
    public function reporte(){
         //Obtenemos los registros
         $registros=DB::table('categoria')
            ->where ('condicion','=','1')
            ->orderBy('nombre','asc')
            ->get();

         $pdf = new Fpdf();
         $pdf::AddPage();
         $pdf::SetTextColor(35,56,113);
         $pdf::SetFont('Arial','B',11);
         $pdf::Cell(0,10,utf8_decode("Listado Categorías"),0,"","C");
         $pdf::Ln();
         $pdf::Ln();
         $pdf::SetTextColor(0,0,0);  // Establece el color del texto 
         $pdf::SetFillColor(206, 246, 245); // establece el color del fondo de la celda 
         $pdf::SetFont('Arial','B',10); 
         //El ancho de las columnas debe de sumar promedio 190        
         $pdf::cell(50,8,utf8_decode("Nombre"),1,"","L",true);
         $pdf::cell(140,8,utf8_decode("Descripción"),1,"","L",true);
         
         $pdf::Ln();
         $pdf::SetTextColor(0,0,0);  // Establece el color del texto 
         $pdf::SetFillColor(255, 255, 255); // establece el color del fondo de la celda
         $pdf::SetFont("Arial","",9);
         
         foreach ($registros as $reg)
         {
            $pdf::cell(50,6,utf8_decode($reg->nombre),1,"","L",true);
            $pdf::cell(140,6,utf8_decode($reg->descripcion),1,"","L",true);
            $pdf::Ln(); 
         }

         $pdf::Output();
         exit;
    }

    //view information history record
    public function showDataModal($id)
    {
        $data = DailyRate::where('rowid', $id)->first();
        $creator = \sisVentas\User::where('id', $data->created_by)->first();
        $updator = \sisVentas\User::where('id', $data->last_updated_by)->first();
        
        return view('modal.modal_body', compact('data', 'creator', 'updator' ));
    }

}
