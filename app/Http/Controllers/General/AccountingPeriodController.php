<?php

namespace App\Http\Controllers\General;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Requests\General\AccountingPeriodCreateRequest;
use App\Http\Requests\General\AccountingPeriodUpdateRequest;
use App\Entities\General\AccountingPeriod;

class AccountingPeriodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $accountingrecords = AccountingPeriod::all();

        return view('general.periods.index', compact('accountingrecords'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(AccountingPeriod $accountingrecord)
    {
        return view('general.periods.create', compact('accountingrecord'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AccountingPeriodCreateRequest $request)
    {
        $accountingrecord = AccountingPeriod::create([
            'name' => $request->get('name'),
            'status' => $request->has('status') ? 1 : 0,
            'date_from' => $request->get('date_from'),
            'date_to' => $request->get('date_to')
        ]);

        flash('El periodo contable '. $accountingrecord->name.' ha sido creado con éxito')->success();

        return redirect()->route('accountingrecord.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(AccountingPeriod $accountingrecord)
    {
        return view('general.periods.edit', compact('accountingrecord'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AccountingPeriodUpdateRequest $request, AccountingPeriod $accountingrecord)
    {
        $accountingrecord->update([
            'name' => $request->get('name'),
            'status' => $request->has('status') ? 1 : 0,
            'date_from' => $request->get('date_from'),
            'date_to' => $request->get('date_to')
        ]);

        flash()->success('El periodo contable '. $accountingrecord->name .' ha sido actualizado con éxito');

        return redirect()->route('accountingrecord.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(AccountingPeriod $accountingrecord)
    {
        flash()->success('El periodo contable '. $accountingrecord->name .' ha sido eliminada con éxito');

        $accountingrecord->delete();

        return redirect()->route('accounting_record.index');
    }
}
