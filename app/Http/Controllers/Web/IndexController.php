<?php

namespace App\Http\Controllers\Web;
use App;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class IndexController extends Controller
{
  
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('contenido/contenido');
    }

    public function nosotros() {
        return view('web/nosotros');
    }

    public function blog() {
        return view('web/blog');
    }

    public function ayuda() {
        return view('web/ayuda');
    }

    public function login () {
        return view('login');
    }

    public function register () {
        return view('register');
    }
}
