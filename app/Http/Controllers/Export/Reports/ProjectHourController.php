<?php

namespace App\Http\Controllers\Export\Reports;

use App\Exports\Reports\ProjectHoursExport;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;

class ProjectHourController extends Controller
{
    public function export()
    {
        return Excel::download(new ProjectHoursExport, 'project-hour.xlsx');
    }

}
