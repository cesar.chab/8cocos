<?php

namespace App\Http\Controllers\Export\Reports;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Exports\Administrative\PurchaseMadeExport;
use Maatwebsite\Excel\Facades\Excel;


class PurchaseMadeController extends Controller
{
    public function export(){
        return Excel::download(new PurchaseMadeExport, 'Listado_de_Compras.xlsx');
    }
}
