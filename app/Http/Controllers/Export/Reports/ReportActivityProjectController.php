<?php

namespace App\Http\Controllers\Export\Reports;

use App\Exports\Reports\MyReportExport;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;

class ReportActivityProjectController extends Controller
{
    public function export()
    {
        return Excel::download(new MyReportExport, 'reports.xlsx');
    }
}
