<?php


use App\Entities\General\Directory;
use App\Entities\General\File;
use Illuminate\Support\Facades\Storage;



if(!function_exists('create_directory'))
{
    function create_directory(String $name_directory, String $url_directory)
    {

        try {
            $exist_directory = Directory::where('url', $url_directory.$name_directory.'/')->first();

            if (!$exist_directory) {

                $path_actuality = $url_directory;
                $directory = new Directory();
                $directory->name = $name_directory;
                $directory->url = $path_actuality.$directory->name.'/';

                if ($path_actuality !== '/root/') {
                    $parent = Directory::where('url', $path_actuality)->first();
                    $directory->parent = $parent->id;
                }

                if(!file_exists($directory->url)){

                    Storage::makeDirectory($directory->url, 0775, true);
                    $directory->save();
                    return $directory->id;
                }

            } else {
                //$message = "La ruta " . $directory->name ." ya existe";
                return false;
            }

        }catch (\Exception $exception){
            return "Fatal Error -".$exception->getMessage();
        }
    }
}

/*
 * Function uploading files a directory speciality with id project
 * */
if(!function_exists('upload_files'))
{
    function upload_files($directory_id, Array $files_list)
    {
        if($directory_id > 0)   {
            $directory = Directory::find($directory_id);
            $base = $directory->url;
            $id_directory = $directory_id;
        }else{
            $base = '/root/';
            $id_directory = null;
        }

        //$files = $request->file('file');
        $files = $files_list;
        $path = storage_path('marcacionfiles'.$base);

            foreach($files as $file)
            {
                $extension = $file->getClientOriginalExtension();
                $size_file = $file->getSize();

                $store_file = File::create([
                    'mime'              => $file->getClientMimeType(),
                    'original_filename' => $file->getClientOriginalName(),
                    'filename'          => $file->getFilename().'.'.$extension,
                    'url'               => 'marcacionfiles'.$base,
                    'directory_id'      => $id_directory,
                    'size'              => $size_file,
                    'employee_id'       => auth()->user()->employee_id
                ]);

                $file->move($store_file->url, $store_file->original_filename);
            }
            $message = "Guardado con exito";
            return response()->json(array('msg'=> $message), 200);

    }
}


if(!function_exists('upload_file_constancy')) {
    function upload_file_constancy($path_name, $file_constancy)
    {
        $base = '/constancy_files';
        $file = $file_constancy;
        $path = storage_path($base.'/'.$path_name);

        if($file)
        {
            $extension = $file->getClientOriginalExtension();
            $size_file = $file->getSize();

            $file->move($path, $file->getClientOriginalName());

            $message = "Guardado con exito";

            return response()->json(array('msg'=> $message, 'path' => $base.'/'.$path_name, 'file_name' => $file->getClientOriginalName()), 200);
        }
        return false;
    }
}
