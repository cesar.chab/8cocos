<?php


namespace App\Entities\General;

use App\Entities\Quotations\InvoiceReceivable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TypeDocument extends Model
{

    use SoftDeletes;

    protected $table = 'general_type_documents';

    protected $guarded = [];


    /* Relations */
    
    public function invoce()
    {
        return $this->belongsTo(InvoiceReceivable::class, 'type_doc');
    }


    /* Muttators */

    public function getStatusLabelAttribute()
    {
        if($this->status === 1)
        {
            return "<span class='label label-success'>Activo</span>";
        }else{
            return "<span class='label label-danger'>Inactivo</span>";
        }
    }
}
