<?php


namespace App\Entities\General;


use App\Entities\Assistances\Assistance;
use App\Entities\Projects\ProjectSpeciality;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Entities\Materials\MaterialMovement;
use App\Entities\Administrative\AccountReceivable\ProjectPayment;
use App\Entities\Materials\MaterialReservation;

class Employee extends Model
{
    use SoftDeletes;

    protected $table = 'general_employees';

    protected $guarded = [];


    public function position()
    {
        return $this->hasOne(Position::class, 'id', 'position_id');
    }

    public function project_speciality()
    {
        return $this->hasOne(ProjectSpeciality::class, 'employee_id');
    }

    public function user()
    {
        return $this->hasOne(User::class);
    }

    public function employees()
    {
        return $this->belongsToMany(Assistance::class, 'assistances_speciality_employees_projects','assistance_id','employee_id');
    }

    public function materialmovement(){
        return $this->belongsTo(MaterialMovement::class, 'id', 'employee_id');
    }

    public function responsable()
    {
        return $this->hasMany(ProjectPayment::class, 'responsable_id');
    }

    public function materialreservation(){
        return $this->belongsTo(MaterialReservation::class, 'id', 'project_id');
    }

}
