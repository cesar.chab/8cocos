<?php

namespace App\Entities\General;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyRegistration extends Model
{
    
    use SoftDeletes;

    protected $table = 'general_company_registration';

    protected $guarded = [];
}
