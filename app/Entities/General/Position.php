<?php

namespace App\Entities\General;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Position extends Model{

    use SoftDeletes;

    protected $table = 'general_positions';
    protected $guarded = [];

    /* Muttators */

    public function getStatusNameAttribute (){
        if ($this->status === 1)
        {
            return "<span class='label label-success'>Activo</span>";
        }else{
            return "<span class='label label-danger'>Inactivo</span>";
        }
    }

}