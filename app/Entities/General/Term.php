<?php

namespace App\Entities\General;

use Illuminate\Database\Eloquent\Model;

class Term extends Model
{
    protected $table = 'generals_terms';
}
