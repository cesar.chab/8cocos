<?php

namespace App\Entities\General;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;

class DailyRate extends Model
{
    protected $table='gl_daily_rate';
 
    public $timestamps=true;
 
    protected $fillable =[
    	'from_currency',
    	'to_currency',
    	'conversion_date',
        'conversion_type',
        'conversion_rate',
        'condicion',
        'last_updated_by',
        'created_by'

    ];

    protected $guarded =[

    ];

 
}
