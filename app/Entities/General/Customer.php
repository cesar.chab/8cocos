<?php


namespace App\Entities\General;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{

    use SoftDeletes;

    protected $table = 'general_customers';

    protected $guarded = [];


    public function type_document()
    {
        return $this->hasOne(TypeDocument::class, 'id', 'type_document_id');
    }



    /* Muttators */

    public function getStatusLabelAttribute (){
        if ($this->status === 1)
        {
            return "<span class='label label-success'>Activo</span>";
        }else{
            return "<span class='label label-danger'>Inactivo</span>";
        }
    }

}
