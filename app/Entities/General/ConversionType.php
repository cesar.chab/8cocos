<?php

namespace App\Entities\General;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;

class ConversionType extends Model
{
    protected $table='gl_daily_conversion_types';

    protected $primaryKey='id';

    public $timestamps=true;


    protected $fillable =[
    	'conversion_type',
    	'description',
    	'enable_flag',
		'created_by',
		'last_updated_by'
    ];

    protected $guarded =[

    ];


}
