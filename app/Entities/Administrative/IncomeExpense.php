<?php

namespace App\Entities\Administrative;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Entities\General\Reason;
use App\Entities\Projects\ProjectRegistration;

class IncomeExpense extends Model
{

    protected $table = 'administrative_incomes_expenses';
    protected $guarded = [];


    public function project()
    {
        return $this->hasOne(ProjectRegistration::class, 'id', 'project_id');
    }

    public function reason()
    {
        return $this->hasOne(Reason::class, 'id','reason_id');
    }

    /* scope */
    public function scopeProject($query)
    {
        return $query->when(request()->filled('project_id'), function ($query_project) {
                $query_project->where('project_id', request()->get('project_id'));
        });
    }

    public function scopeBydate($query)
    {
        return $query->whereBetween('movement_date', [Carbon::parse(request()->input('start')), Carbon::parse(request()->input('end'))]);
    }

    public function scopeExpense($query)
    {
        return $query->whereHas('reason', function($q) {
            $q->where('reason', 'egresos');
        });
    }

    public function scopeIncome($query)
    {
        return $query->whereHas('reason', function($q) {
            $q->where('reason', 'ingresos');
        });
    }

}


