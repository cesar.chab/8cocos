<?php

namespace App\Entities\Administrative;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Entities\General\ExpenseFixedConcept;

class FixedOutput extends Model
{
    use SoftDeletes;

    protected $table = 'administrative_fixed_outputs';
    protected $guarded = [];

    /* Muttators */

    public function getTypeExpensesLabelAttribute (){
        if ($this->type_expenses === "work")
        {
            return "<span class=''>Obras</span>";
        }else{
            return "<span class=''>Oficina</span>";
        }
    }

    /* relationship */

    public function expenses()
    {
        return $this->belongsTo(ExpenseFixedConcept::class);
    }
}
