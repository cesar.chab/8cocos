<?php

namespace App\Entities\Administrative\ShoppingRecord;

use Illuminate\Database\Eloquent\Model;
use App\Entities\General\Provider;
use App\Entities\Projects\ProjectRegistration;

class PurchaseMade extends Model
{
    protected $table = 'administrative_purchases_mades';
    protected $guarded = [];


    public function provider()
    {
        return $this->belongsTo(Provider::class, 'provider_id');
    }

    public function project()
    {
        return $this->belongsTo(ProjectRegistration::class, 'project_id');
    }

    public function getPurchaseTotalAttribute(){
        return $this->amount + (($this->amount * $this->IGV)/100);
    }

    public function getDeclavedLabelAttribute(){
        if ($this->declaved === 1)
        {
            return "<span class='label label-success'>Si</span>";
        }else{
            return "<span class='label label-danger'>No</span>";
        }
    }

}
