<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Registra tus datos de perfil</title>

    <link rel="stylesheet" type="text/css" href="{{asset('8cocos/dist/plugins/font-awesome/4.7.0/css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('8cocos/dist/css/login.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('8cocos/dist/css/datosperfil.css')}}">
    
</head>
<body class="with-bg-white">
    <div class="layout">
        <div class="ivu-layout">
            <div class="ivu-layout-header">
                <div class="ivu-row-flex ivu-row-flex-center">
                    <div class="ivu-col ivu-col-span-xs-24 ivu-col-span-lg-24">
                        <a href="/" hreflang="es-pe" class="logoPersonal">
                            <img src="{{asset('8cocos/imagenes/logo/logofinal.png')}}" width="150" alt="8COCOS">
                        </a>
                    </div>
                    <div class="ivu-col ivu-col-span-xs-24 ivu-col-span-lg-8">
                        <div class="ivu-steps ivu-steps-horizontal">
                            <div class="ivu-steps-item ivu-steps-status-undefined">
                                <div class="ivu-steps-tail"><i></i></div>
                                <div class="ivu-steps-head">
                                    <div class="ivu-steps-head-inner"><span></span></div>
                                </div>
                                <div class="ivu-steps-main">
                                    <div class="ivu-steps-title"></div>
                                </div>
                            </div>
                            <div class="ivu-steps-item ivu-steps-status-undefined">
                                <div class="ivu-steps-tail"><i></i></div>
                                <div class="ivu-steps-head">
                                    <div class="ivu-steps-head-inner"><span></span></div>
                                </div>
                                <div class="ivu-steps-main">
                                    <div class="ivu-steps-title"></div>
                                </div>
                            </div>
                            <div class="ivu-steps-item ivu-steps-status-undefined">
                                <div class="ivu-steps-tail"><i></i></div>
                                <div class="ivu-steps-head">
                                    <div class="ivu-steps-head-inner"><span></span></div>
                                </div>
                                <div class="ivu-steps-main">
                                    <div class="ivu-steps-title"></div>
                                </div>
                            </div>
                            <div class="ivu-steps-item ivu-steps-status-undefined">
                                <div class="ivu-steps-tail"><i></i></div>
                                <div class="ivu-steps-head">
                                    <div class="ivu-steps-head-inner"><span></span></div>
                                </div>
                                <div class="ivu-steps-main">
                                    <div class="ivu-steps-title"></div>
                                </div>
                            </div>
                            <div class="ivu-steps-item ivu-steps-status-undefined">
                                <div class="ivu-steps-tail"><i></i></div>
                                <div class="ivu-steps-head">
                                    <div class="ivu-steps-head-inner"><span></span></div>
                                </div>
                                <div class="ivu-steps-main">
                                    <div class="ivu-steps-title"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ivu-layout-content">
                <section class="datos-perfil">
                    <div class="code-row-bg ivu-row">
                        <div class="steps">
                            <div class="left-column"></div>
                            <div class="col-full">
                                <div class="ivu-col" style="text-align:center">
                                    <div class="col-full-title"><img src="{{asset('8cocos/imagenes/svg/registro_step01.svg')}}" width="" alt="cuenta">
                                        <h3>Crea tu cuenta</h3>
                                    </div>
                                    <form id="regisgter" onsubmit="return false" method="POST" action="{{ url('/register') }}" class="needs-validation ivu-form ivu-form-label-right ivu-form-inline">
                                        @csrf
                                        <div class="ivu-row-flex ivu-row-flex-center" style="margin-left:-8px;margin-right:-8px">
                                            <div class="ivu-col ivu-col-span-xs-24 ivu-col-span-lg-24 ivu-col-span-xl-10">
                                                <div class="ivu-form-item ivu-form-item-required">
                                                    <input autocomplete="off" spellcheck="false" type="email" id="email" name="email" placeholder="Email" class="ivu-input ivu-input-default">
                                                    <div class="ivu-form-item-error-tip">No puede estar vacío</div>
                                                </div>

                                            </div>
                                            <div class="ivu-col ivu-col-span-xs-24 ivu-col-span-lg-24 ivu-col-span-xl-10">
                                                <div class="ivu-form-item ivu-form-item-required">
                                                    <input autocomplete="off" spellcheck="false" type="email" id="valid-email" name="valid-email" placeholder="Revalidar Email" class="ivu-input ivu-input-default">
                                                    <div class="ivu-form-item-error-tip">No puede estar vacío</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="ivu-row-flex ivu-row-flex-center" style="margin-left:-8px;margin-right:-8px">
                                            <div class="ivu-col ivu-col-span-xs-24 ivu-col-span-lg-24 ivu-col-span-xl-10">
                                                <div class="ivu-form-item ivu-form-item-required">
                                                    <div class="ivu-form-item-content">
                                                        <div class="ivu-input-wrapper ivu-input-wrapper-default ivu-input-type-password">
                                                            <input id="password" name="password" autocomplete="off" spellcheck="false" type="password" placeholder="Contraseña" class="ivu-input ivu-input-default">
                                                            <div class="ivu-form-item-error-tip">No puede estar vacío</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="ivu-col ivu-col-span-xs-24 ivu-col-span-lg-24 ivu-col-span-xl-10">
                                                <div class="ivu-form-item ivu-form-item-required">
                                                    <div class="ivu-form-item-content">
                                                        <div class="ivu-input-wrapper ivu-input-wrapper-default ivu-input-type-password">
                                                            <input id="confirm-password" name="confirm-password" autocomplete="off" spellcheck="false" type="password" placeholder="Confirmar contraseña" class="ivu-input ivu-input-default">
                                                            <div class="ivu-form-item-error-tip">No puede estar vacío</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="ivu-row-flex ivu-row-flex-center" style="margin-left:-8px;margin-right:-8px">
                                            <div class="ivu-col ivu-col-span-xs-24 ivu-col-span-lg-8 ivu-col-span-xl-6">
                                                <div class="ivu-form-item ivu-form-item-required">
                                                    <div class="ivu-form-item-content">
                                                        <select id="document" name="document" placeholder="Tipo" class="select-css">
                                                            <option value="DNI" selected>DNI</option>
                                                            <option value="CE">CE</option>
                                                            <option value="PASSPORT">PASAPORTE</option>
                                                        </select>
                                                        <div class="ivu-form-item-error-tip">No puede estar vacío</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="ivu-col ivu-col-span-xs-24 ivu-col-span-lg-15 ivu-col-span-xl-7">
                                                <div class="ivu-form-item ivu-form-item-required">
                                                    <div class="ivu-form-item-content">
                                                        <div class="ivu-input-wrapper ivu-input-wrapper-default ivu-input-type-text">
                                                            <i class="ivu-icon ivu-icon-ios-loading ivu-load-loop ivu-input-icon ivu-input-icon-validate"></i>
                                                            <input id="number-document" name="number-document" autocomplete="off" spellcheck="false" placeholder="Número de Documento" class="ivu-input ivu-input-default">
                                                            <div class="ivu-form-item-error-tip">No puede estar vacío</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="ivu-col ivu-col-span-xs-24 ivu-col-span-lg-24 ivu-col-span-xl-7">
                                                <div class="ivu-form-item ivu-form-item-required">
                                                    <div class="ivu-form-item-content">
                                                        <div class="ivu-input-wrapper ivu-input-wrapper-default ivu-input-type-text">
                                                            <i class="ivu-icon ivu-icon-ios-loading ivu-load-loop ivu-input-icon ivu-input-icon-validate"></i>
                                                            <input id="telephone" name="telephone" autocomplete="off" spellcheck="false" placeholder="Número de Teléfono" class="ivu-input ivu-input-default">
                                                            <div class="ivu-form-item-error-tip">No puede estar vacío</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="ivu-row-flex ivu-row-flex-center">
                                            <div class="ivu-col ivu-col-span-xs-24 ivu-col-span-lg-24">
                                                <p style="font-size:1rem;margin:.6rem">
                                                    Al <strong>"Registrarte"</strong>, aceptas nuestros
                                                    <strong><a href="https://8COCOS.com/ayuda/#term" rel="alternate" hreflang="es-pe" target="_blank">Términos y Condiciones</a></strong> y
                                                    <strong><a href="https://8COCOS.com/ayuda/#politicas" rel="alternate" hreflang="es-pe" target="_blank">Políticas de usos de Datos.</a></strong></p>
                                            </div>
                                        </div> <br>
                                        <div class="ivu-row-flex ivu-row-flex-space-around">
                                            <div class="ivu-col ivu-col-span-xs-11 ivu-col-span-lg-11 ivu-col-span-xl-8">
                                                <div class="button-grid">
                                                    <div class="button-content">
                                                        <a href="javscript:void(0)" id="cancel">
                                                            <i aria-hidden="true" class="fa fa-times"></i> Cancelar
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="ivu-col ivu-col-span-xs-11 ivu-col-span-lg-11 ivu-col-span-xl-8">
                                                <button id="register" type="button" class="ivu-btn ivu-btn-success ivu-btn-circle">
                                                    Registrarme
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="right-column">
                                <div></div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div id="ivumodalmask" class="ivu-modal-mask" style="z-index:1019;display:none"></div>
                        <div id="ivumodalwrap" class="ivu-modal-wrap ivu-modal-hidden" style="z-index:1019">
                            <div id="ivumodal" class="ivu-modal" style="width:360px;display:none">
                                <div class="ivu-modal-content"><a class="ivu-modal-close"><i class="ivu-icon ivu-icon-ios-close"></i></a>
                                    <div class="ivu-modal-header">
                                        <p style="color:#060f26;text-align:center"><i class="ivu-icon ivu-icon-ios-information-circle"></i> <span>Cancelar registro</span></p>
                                    </div>
                                    <div class="ivu-modal-body">
                                        <div style="text-align:center">
                                            <p>Al darle click a este botón se cancelará el registro actual</p>
                                            <p>¿Seguro que desea continuar?</p>
                                        </div>
                                    </div>
                                    <div class="ivu-modal-footer">
                                        <button id="confirmButtonCancel" type="button" class="ivu-btn ivu-btn-primary ivu-btn-long ivu-btn-large">
                                            Aceptar
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div class="ivu-layout-footer">
                <div class="footer-img">
                    <img src="{{asset('8cocos/imagenes/DatosPerfil/carrera.png')}}">
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src='{{asset('8cocos/dist/js/jquery/jquery.js')}}' id='jquery-core-js'></script>
    <script>
        let cancelButton = document.getElementById('cancel');
        let confirmButtonCancel = document.getElementById('confirmButtonCancel');
        let register = document.getElementById('register');
        cancelButton.addEventListener('click', function(e) {
            let ivumodalmask = document.getElementById('ivumodalmask');
            let ivumodalwrap = document.getElementById('ivumodalwrap');
            let ivumodal = document.getElementById('ivumodal');
            ivumodalmask.style = "display:block"
            ivumodal.style = "display:block";
            ivumodalwrap.classList.remove('ivu-modal-hidden');
        });
        confirmButtonCancel.addEventListener('click', function() {
            window.location.href = '{{route("tipo_perfil")}}';
        })

        jQuery(document).ready(function($) {
            $('#register').on('click', function (e) {
                alert('hola');
            })
        });
        //Input error class: ivu-form-item-error
        // window.addEventListener('load', function() {
        //     // Fetch all the forms we want to apply custom Bootstrap validation styles to
        //     let forms = document.getElementsByClassName('needs-validation');

        //     // Loop over them and prevent submission
        //     let validation = Array.prototype.filter.call(forms, function(form) {
        //         form.addEventListener('submit', function(event) {  
        //             console.log(form.checkValidity())                
        //             if (form.checkValidity() === false) {
        //                 event.preventDefault();
        //                 event.stopPropagation();
        //             }
        //             form.classList.add('ivu-form-item-error');
        //         }, false);
        //     });
        // }, false);

    </script>
</body>
</html>
