<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registra tu cuenta</title>    
    <link rel="stylesheet" type="text/css" href="{{asset('8cocos/dist/plugins/font-awesome/4.7.0/css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('8cocos/dist/css/tipoperfil.css')}}">    
</head>
<body>
    <div data-server-rendered="true" id="__nuxt">
        <div id="__layout">
            <div class="layout code-row-bg" data-v-56a2bdaf>
                <div class="code-row-bg__content" data-v-56a2bdaf>
                    <section data-v-5f2385bc data-v-56a2bdaf>
                        <a href="/" aria-current="page" hreflang="es-pe" class="is-active" data-v-5f2385bc><img src="{{asset('8cocos/imagenes/logo/logofinal.png')}}" width="300" alt="cocoS" class="logo" data-v-5f2385bc></a>
                        <h1 data-v-5f2385bc>¿Con qué perfil deseas operar?</h1> <br data-v-5f2385bc>
                        <article class="profiles-container" data-v-5f2385bc>
                            <ul class="list-of-profiles" data-v-5f2385bc>
                                <li class="profile personal" data-v-5f2385bc>
                                    <div data-v-5f2385bc>
                                        <a href="{{route('personal')}}" role="link" tabindex="0" hreflang="es-pe" class="profile-link" data-v-5f2385bc>
                                            <div class="avatar-wrapper" data-v-5f2385bc>
                                                <div class="profile-icon" data-v-5f2385bc></div>
                                            </div>
                                            <div class="profile-name" data-v-5f2385bc>
                                                Personal
                                            </div>
                                        </a>
                                    </div>
                                </li>
                                <li class="profile companies" data-v-5f2385bc>
                                    <div data-v-5f2385bc>
                                        <a href="{{route('business')}}" role="link" tabindex="1" hreflang="es-pe" class="profile-link" data-v-5f2385bc>
                                            <div class="avatar-wrapper" data-v-5f2385bc>
                                                <div class="profile-icon" data-v-5f2385bc></div>
                                            </div>
                                            <div class="profile-name" data-v-5f2385bc>Empresa</div>
                                        </a>
                                    </div>
                                </li>
                            </ul>
                            {{-- <div data-transfer="true" data-v-5f2385bc>
                                <div class="ivu-modal-mask" style="z-index:1029;display:block"></div>
                                <div class="ivu-modal-wrap ivu-modal-hidden" style="z-index:1029">
                                    <div class="ivu-modal" style="width:360px;display:block">
                                        <div class="ivu-modal-content"><a class="ivu-modal-close"><i class="ivu-icon ivu-icon-ios-close"></i></a>
                                            <div class="ivu-modal-header">
                                                <p style="color:#060f26;text-align:center" data-v-5f2385bc><i class="ivu-icon ivu-icon-ios-information-circle" data-v-5f2385bc></i> <span data-v-5f2385bc>Información</span></p>
                                            </div>
                                            <div class="ivu-modal-body">
                                                <div style="text-align:center;color:#000" data-v-5f2385bc>
                                                    <p data-v-5f2385bc>Recuerda que para generar un perfil de</p>
                                                    <p data-v-5f2385bc>empresa es necesario registrarse primero</p>
                                                    <p data-v-5f2385bc>como persona natural</p>
                                                </div>
                                            </div>
                                            <div class="ivu-modal-footer">
                                                <div data-v-5f2385bc><button type="button" class="ivu-btn ivu-btn-success ivu-btn-long ivu-btn-circle ivu-btn-large" data-v-5f2385bc>  <span>Continuar</span></button></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> --}}
                        </article>
                    </section>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
