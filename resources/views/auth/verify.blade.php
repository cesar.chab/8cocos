@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-7" style="margin-top: 2%">
                <div class="box">
                    <h3 class="box-title" style="padding: 2%">Verifica tu direccion de correo</h3>

                    <div class="box-body">
                        @if (session('resent'))
                            <div class="alert alert-success" role="alert">Se a enviado un enlace para verificar su correo
                            </div>
                        @endif
                        <p>Antes de proceder, por favor verifica si tu correo esta el enlace para verificar,</p>
                        <a href="{{ route('verification.resend') }}">Si no ha recibido, presione para volver a solicitar'</a>.
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
