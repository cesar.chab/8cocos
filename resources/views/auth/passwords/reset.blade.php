<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Reestablecer contraseña</title>
    <link rel="stylesheet" type="text/css" href="{{asset('8cocos/dist/css/login.css')}}">

</head>
<body>
    <div data-server-rendered="true" id="__nuxt">
        <div id="__layout">
            <div class="layout code-row-bg" data-v-7ff8b1c4>
                <div class="code-row-bg__content" data-v-7ff8b1c4>
                    <div id="form-login" data-v-7ff8b1c4>
                        <a href="{{url('/')}}" hreflang="es-pe"><img src="{{asset('8cocos/imagenes/logo/logofinal.png')}}" alt="8COCOS" class="logo" width="202px" ; height="50px"></a>
                        <h1>Reestablecer contraseña</h1> <br>
                        <form method="POST" action="{{ route('password.update', ['token' => $token]) }}" autocomplete="off" class="ivu-form ivu-form-label-right">
                            @csrf
                            <div class="ivu-form-item">
                                <div class="ivu-form-item-content">
                                    <div class="ivu-input-wrapper ivu-input-wrapper-default ivu-input-type-text ivu-input-group ivu-input-group-default ivu-input-group-with-append ivu-input-hide-icon">
                                        <i class="ivu-icon ivu-icon-ios-loading ivu-load-loop ivu-input-icon ivu-input-icon-validate"></i>
                                        <input autocomplete="off" id="email" name="email" spellcheck="false" placeholder="Correo electrónico" class="ivu-input ivu-input-default" value="{{old('email')}}">
                                        <div class="ivu-input-group-append">
                                            <i class="fa fa-user" style="font-size:18px"></i>
                                        </div>
                                    </div>
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="ivu-form-item">
                                <div class="ivu-form-item-content">
                                    <div class="ivu-input-wrapper ivu-input-wrapper-default ivu-input-type-password ivu-input-group ivu-input-group-default ivu-input-group-with-append ivu-input-hide-icon">
                                        <i class="ivu-icon ivu-icon-ios-loading ivu-load-loop ivu-input-icon ivu-input-icon-validate"></i>
                                        <input autocomplete="off" spellcheck="false" type="password" id="password" name="password" placeholder="Nueva Contraseña" class="ivu-input ivu-input-default">
                                        <div class="ivu-input-group-append"><i class="fa fa-eye" style="font-size:20px"></i></div>
                                    </div>
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="ivu-form-item">
                                <div class="ivu-form-item-content">
                                    <div class="ivu-input-wrapper ivu-input-wrapper-default ivu-input-type-password ivu-input-group ivu-input-group-default ivu-input-group-with-append ivu-input-hide-icon">
                                        <i class="ivu-icon ivu-icon-ios-loading ivu-load-loop ivu-input-icon ivu-input-icon-validate"></i>
                                        <input autocomplete="off" spellcheck="false" type="password" id="password_confirmation" name="password_confirmation" placeholder="Confirmar Contraseña" class="ivu-input ivu-input-default">
                                        <div class="ivu-input-group-append"><i class="fa fa-eye" style="font-size:20px"></i></div>
                                    </div>
                                </div>
                            </div>
                            <div class="ivu-form-item">
                                <div class="ivu-form-item-content">
                                    <button type="submit" class="ivu-btn ivu-btn-success ivu-btn-circle">
                                        <span>Enviar</span>
                                    </button>
                                </div>
                            </div>
                            <div>
                                <a href="{{ url('/') }}" hreflang="es-pe" style="color: #fff; font-weight: bold">
                                    Ir al inicio
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
