<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ingresa a tu cuenta</title>
    <link rel="stylesheet" type="text/css" href="8cocos/dist/css/login.css">
</head>
<body>
    <div data-server-rendered="true" id="__nuxt">
        <div id="__layout">
            <div class="layout code-row-bg" data-v-7ff8b1c4>
                <div class="code-row-bg__content" data-v-7ff8b1c4>
                    <div id="form-login" data-v-7ff8b1c4>
                        <a href="{{url('/')}}" hreflang="es-pe"><img src="8cocos/imagenes/logo/logofinal.png" alt="8COCOS" class="logo" width="202px" ; height="50px"></a>
                        <h1>Ingresa a tu cuenta</h1> <br>
                        <form action="{{route('login')}}" method="POST" autocomplete="off" class="ivu-form ivu-form-label-right">
                            {{ csrf_field() }}
                            <div class="ivu-form-item">
                                <div class="ivu-form-item-content">
                                    <div class="ivu-input-wrapper ivu-input-wrapper-default ivu-input-type-text ivu-input-group ivu-input-group-default ivu-input-group-with-append ivu-input-hide-icon">
                                        <i class="ivu-icon ivu-icon-ios-loading ivu-load-loop ivu-input-icon ivu-input-icon-validate"></i>
                                        <input autocomplete="off" id="email" name="email" spellcheck="false" placeholder="Correo electrónico" class="ivu-input ivu-input-default" value="{{old('email')}}">
                                        <div class="ivu-input-group-append">
                                            <i class="fa fa-user" style="font-size:18px"></i>
                                        </div>

                                    </div>
                                    {!!$errors->first('email','<span class="help-block">:message</span>')!!}
                                </div>
                            </div> <br>
                            <div class="ivu-form-item">
                                <div class="ivu-form-item-content">
                                    <div class="ivu-input-wrapper ivu-input-wrapper-default ivu-input-type-password ivu-input-group ivu-input-group-default ivu-input-group-with-append ivu-input-hide-icon">
                                        <i class="ivu-icon ivu-icon-ios-loading ivu-load-loop ivu-input-icon ivu-input-icon-validate"></i>
                                        <input autocomplete="off" spellcheck="false" type="password" id="password" name="password" placeholder="Contraseña" class="ivu-input ivu-input-default">
                                        <div class="ivu-input-group-append"><i class="fa fa-eye" style="font-size:20px"></i></div>

                                    </div>
                                    {!!$errors->first('password','<span>:message</span>')!!}
                                </div>
                            </div> <br>
                            <div class="ivu-form-item">
                                <div class="ivu-form-item-content">
                                    <button type="submit" class="ivu-btn ivu-btn-success ivu-btn-circle">
                                        <span>Iniciar sesión</span>
                                    </button>
                                </div>
                            </div>
                            <div><a href="{{ url('/password/reset') }}" hreflang="es-pe" style="font-weight: bold;color: #fff; ">¿Olvidaste tu contraseña?</a></div> <br>
                            <div>
                                <p class="account__info">¿No tienes una cuenta para operar?</p>
                                <a href="{{ url('/register') }}" hreflang="es-pe" style="color: #fff; font-weight: bold"><br>
                                    Crea una cuenta
                                    <span class="text-green" style="color: #6DB964">Aquí</span></a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
