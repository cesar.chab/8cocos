<table>
    <caption>REPORTE DE TRABAJOS</caption>
    <thead>
    <tr>
        <th>Nombre Proyecto</th>
        <th>Cliente</th>
        <th>Celular</th>
        <th>Fecha de Contrato</th>
        <th>Responsable del Proyecto</th>
        <th>Entrega de fecha</th>
        <th>Prioridad</th>
        <th>Porcentaje del Proyecto</th>
        <th>Especialidades</th>
        <th>Responsable</th>
        <th>Entregable</th>
        <th>Estado</th>
        <th>% De Avance Especialidades</th>
        <th>Links De Archivos</th>
        <th>Fecha de Entrega</th>
        <th>Fecha de Inicio</th>
        <th>Fecha Concluido</th>
        <th>Tiempo dedicado</th>
        <th>Observacion</th>

    </tr>
    </thead>
    <tbody>
    @foreach($projects_registrations as $project)
        <tr>
            <td ROWSPAN="{{ count($project->project_specialities) + 1 }}">{{ $project->name }}</td>
            <td ROWSPAN="{{ count($project->project_specialities) + 1}}">{{ $project->customer->business_name }}</td>
            <td ROWSPAN="{{ count($project->project_specialities) + 1}}">{{ $project->number_mobile }}</td>
            <td ROWSPAN="{{ count($project->project_specialities) + 1}}">{{ $project->date_contract }}</td>
            <td ROWSPAN="{{ count($project->project_specialities) + 1}}">{{ $project->employee->name }}</td>
            <td ROWSPAN="{{ count($project->project_specialities) + 1}}">{{ $project->delivery_date }}</td>
            <td ROWSPAN="{{ count($project->project_specialities) + 1}}">{{ $project->priority->name }}</td>
            <td ROWSPAN="{{ count($project->project_specialities) + 1}}"><!-- Procentaje del proyecto --></td>


        </tr>
        @foreach($project->project_specialities as  $key => $specialities)
            <tr>
                <td>{{ $specialities->speciality->name }} - {{ $key }}</td>
            </tr>
        @endforeach
            
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>

    @endforeach
    </tbody>
</table>
