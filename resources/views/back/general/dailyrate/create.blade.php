@extends ('layouts.admin')
@section ('contenido')
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<h3>Nuevo Tipo de Cambio</h3>
			@if (count($errors)>0)
			<div class="alert alert-danger">
				<ul>
				@foreach ($errors->all() as $error)
					<li>{{$error}}</li>
				@endforeach
				</ul>
			</div>
			@endif

			{!!Form::open(array('url'=>'cg/dailyrate','method'=>'POST','autocomplete'=>'off'))!!}
            {{Form::token()}}
			<!--Contenido-->
			@include('cg.dailyrate.partials.datosgenerales')

		</div>
	</div>
	<div class="for text-center">
		{!! Form::submit('Registrar', ['class'=> 'btn btn-primary']) !!}
		<a class="btn btn-danger" href="{{ route('cg.dailyrate.index')}}">
			Cancelar
		</a>
	</div>

	{!! Form::close() !!}
@push ('scripts')
	<script>
        $('#licg').addClass("treeview active");
        $('#lismdailyrate').addClass("active");
	</script>
@endpush
@endsection

@section('js')
	<script>
        $(".inputmask1").inputmask("(999) 9999999");
        $(".inputmask2").inputmask("(999) 999999999");
        $('.datepicker').datepicker({
            format: "dd-mm-yyyy",
            language: "es",
            autoclose: true
        });

	</script>
@endsection