<div class="col-md-6">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Datos generales</h3>
        </div>
        <div class="form-group">
            	<label for="from_currency">Divisa Desde</label>
				<select name="from_currency" class="form-control select" style="width: 100%;" tabindex="-1" aria-hidden="true">
                  <option value="USD" selected="selected">USD</option>
                  <option value="PEN">PEN</option>
                </select>
				
		</div>
		<div class="form-group">
			<label for="to_currency">Divisa Hasta</label>
			<select name="to_currency" class="form-control select" style="width: 100%;" tabindex="-1" aria-hidden="true">
				<option value="USD" >USD</option>
				<option value="PEN" selected="selected">PEN</option>
			</select>
		</div>
		<div class="form-group">
			{!! Form::label('conversion_date', 'Fecha de TC') !!}
			<div class="input-group date">
                <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </div>
                {!! Form::text('conversion_date', null, ['class' => 'form-control datepicker', 'placeholder' => '', 'required']) !!}
            </div>
		</div>

		<div class="form-group">
			{!! Form::label('conversion_type', 'Tipo de Conversion') !!}
			{!! Form::select('conversion_type', $convtype, null, ['class' => 'form-control', 'required'])!!}
		</div>

		<div class="form-group">

			{!! Form::label('conversion_rate', 'Tipo de Cambio') !!}
			{!! Form::text('conversion_rate',  old('conversion_rate'), ['class' => 'form-control', 'placeholder' => 'Tipo de Cambio...', 'required']) !!}
		</div>
    </div>
</div>