@extends('layouts.app')

@section('title', 'Listado de Clientes')

@section('content')

    <div class="box" style="margin-top: 10px;">
    @include('flash::message') <!-- VISUALIZACION DE MENSAJES RECIBIDOS DEL BACKEND -->
        <div class="box-header with-border">
            <h3 class="box-title">
                Listado de Tipo de Cambios
            </h3>
            <div class="box-tools">
                <div class="text-center">
                    <a class="btn btn-danger btn-sm" href="{{ route('customers.create') }}">
                       <i class="fa fa-plus"></i> Nuevo registro
                    </a>
                </div>

            </div>
        </div>

        <div class="box-body">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover display table-responsive table-condensed" id="table">
                            <thead>
                            <tr>
                                <th>Div. Desde</th>
                                <th>Div. Hasta</th>
                                <th>Fecha TC</th>
                                <th>Tipo</th>
                                <th>Valor TC.</th>
                                <th>ESTADO</th>                                
                                <th>ACCIONES</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($dailys as $cat)
                                <tr>
                                    <td>{{ $cat->from_currency}}</td>
                                    <td>{{ $cat->to_currency}}</td>
                                    <td>{{ $cat->conversion_date}}</td>
                                    <td>{{ $cat->conversion_type}}</td>
                                    <td>{{ $cat->conversion_rate}}</td>

                                    <td>@if($cat->condicion == 1)
                                          Activo
                                      @else
                                          Inactivo
                                      @endif
                                    </td> 
                                    <td>
                                         
                                         
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <!-- footer box-->
        </div>
    </div>

@endsection
@push('scripts')
    <script>
        $(document).ready(function () {

            $('#table').DataTable({
                "language": {
                    "sProcessing": "Procesando...",
                    "sLengthMenu": "Mostrar _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados",
                    "sEmptyTable": "Ningún dato disponible en esta tabla =(",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    },
                    "buttons": {
                        "copy": "Copiar",
                        "colvis": "Visibilidad"
                    }
                }
            });

             


        });

        $('body').on("keydown", function(e) { 
                

                if (e.ctrlKey && e.altKey && e.which === 78) {
                    console.log("You pressed Ctrl + Shift + n");
    
                    e.preventDefault();
    
                    location.href = '{{ route('customers.create') }}';
                }
            });

    </script>
@endpush

 