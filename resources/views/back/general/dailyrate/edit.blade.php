@extends ('layouts.admin')
@section ('contenido')

	{!! Form::model($daily, ['route' => ['cg.dailyrate.update', $daily], 'method' => 'PUT']) !!}
	<div class="row">
		<div class="col-md-12">
			<div class="box">
				<div class="box-header with-border">
					@include('hr.partials.errors')
					<h3 class="box-title">Editar Tipo de Cambio</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="row">
						<div class="col-md-12">
							<!--Contenido-->
							@include('cg.dailyrate.partials.datosgenerales')
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="for text-center">
		{!! Form::submit('Editar', ['class'=> 'btn btn-primary']) !!}
		<a class="btn btn-danger" href="{{ route('cg.dailyrate.index')}}">
			Cancelar
		</a>
	</div>
	{!! Form::close() !!}
@push ('scripts')
<script>
    $('#licg').addClass("treeview active");
    $('#lismdailyrate').addClass("active");
</script>
@endpush
@endsection