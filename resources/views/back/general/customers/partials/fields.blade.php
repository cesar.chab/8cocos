<div class="col-md-3">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Datos Personales</h3>
        </div>
		
		<div class="form-group">
            {!! Form::label('bill_to_name', 'Primer Bombre') !!}
			{!! Form::text('bill_to_name', null, ['class' => 'form-control', 'placeholder' => '', 'required']) !!}
        </div>
		<div class="form-group">
            {!! Form::label('bill_to_name', 'Segundo Bombre') !!}
			{!! Form::text('bill_to_name', null, ['class' => 'form-control', 'placeholder' => '', 'required']) !!}
        </div>
		<div class="form-group">
            {!! Form::label('bill_to_name', 'Primer Apellido') !!}
			{!! Form::text('bill_to_name', null, ['class' => 'form-control', 'placeholder' => '', 'required']) !!}
        </div>
		<div class="form-group">
            {!! Form::label('bill_to_name', 'Segundo Apellido') !!}
			{!! Form::text('bill_to_name', null, ['class' => 'form-control', 'placeholder' => '', 'required']) !!}
        </div>
		<div class="form-group">
            {!! Form::label('id', 'Tipo Documento') !!}
            {!! Form::select('id', $type_documents, null, ['class' => 'form-control', ''])!!}
        </div>
		<div class="form-group">
			{!! Form::label('numdocumento', 'Num Documento') !!}
			{!! Form::text('label_tax_code', null, ['class' => 'form-control', 'placeholder' => '', 'required']) !!}
		</div>
		 
    </div>
</div>

<div class="col-md-3">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Datos Ubicacion</h3>
        </div>
		 
		<div class="form-group">
			{!! Form::label('bill_to_name', 'Nacionalidad') !!}
			{!! Form::text('bill_to_name', null, ['class' => 'form-control', 'placeholder' => '', 'required']) !!}
		</div>
		<div class="form-group">
			{!! Form::label('bill_to_name', 'Departamento') !!}
			{!! Form::text('bill_to_name', null, ['class' => 'form-control', 'placeholder' => '', 'required']) !!}
		</div>
		<div class="form-group">
			{!! Form::label('bill_to_name', 'Provincia') !!}
			{!! Form::text('bill_to_name', null, ['class' => 'form-control', 'placeholder' => '', 'required']) !!}
		</div>
		<div class="form-group">
			{!! Form::label('bill_to_name', 'Distrito') !!}
			{!! Form::text('bill_to_name', null, ['class' => 'form-control', 'placeholder' => '', 'required']) !!}
		</div>
		<div class="form-group">
			{!! Form::label('numdocumento', 'Direccion') !!}
			{!! Form::text('label_tax_code', null, ['class' => 'form-control', 'placeholder' => '', 'required']) !!}
		</div>
		
		 
    </div>
</div>



<div class="col-md-3">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Datos Contacto</h3>
        </div>
		 
		<div class="form-group">
			{!! Form::label('bill_to_name', 'Número telefónico') !!}
			{!! Form::text('bill_to_name', null, ['class' => 'form-control', 'placeholder' => '', 'required']) !!}
		</div>
		<div class="form-group">
			{!! Form::label('bill_to_name', 'Email') !!}
			{!! Form::text('bill_to_name', null, ['class' => 'form-control', 'placeholder' => '', 'required']) !!}
		</div>
		<div class="form-group">
			{!! Form::label('bill_to_name', 'Ocupación') !!}
			{!! Form::text('bill_to_name', null, ['class' => 'form-control', 'placeholder' => '', 'required']) !!}
		</div>
		
		<div class="form-group">
			{!! Form::label('numdocumento', '¿Es usted una Persona Expuesta Políticamente (P.E.P.)?') !!}
			{!! Form::text('label_tax_code', null, ['class' => 'form-control', 'placeholder' => '', 'required']) !!}
		</div>
		<div class="form-group">
			{!! Form::label('bill_to_name', 'Institución') !!}
			{!! Form::text('label_tax_code', null, ['class' => 'form-control', 'placeholder' => '', 'required']) !!}
		</div>
		
		 
    </div>
</div>


<div class="col-md-3">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Docunentacion</h3>
        </div>
		 
		<div class="form-group">
			{!! Form::label('bill_to_name', 'Ver Documento Lado 1') !!}
			{!! Form::text('bill_to_name', null, ['class' => 'form-control', 'placeholder' => '', 'required']) !!}
		</div>
		<div class="form-group">
			{!! Form::label('bill_to_name', 'Ver Documento Lado 2') !!}
			{!! Form::text('bill_to_name', null, ['class' => 'form-control', 'placeholder' => '', 'required']) !!}
		</div>
		 
		<div class="form-group">
			{!! Form::label('bill_to_name', 'Fotografis') !!}
			{!! Form::text('label_tax_code', null, ['class' => 'form-control', 'placeholder' => '', 'required']) !!}
		</div>
		
		 <div class="form-group">
			{!! Form::label('bill_to_name', 'Validación de Identidad') !!}
			{!! Form::text('bill_to_name', null, ['class' => 'form-control', 'placeholder' => '', 'required']) !!}
		</div>
    </div>
</div>

 

