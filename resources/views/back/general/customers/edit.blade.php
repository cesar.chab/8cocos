@extends('layouts.app')

@section('title', 'Editar de Cliente')

@section('content')
    <div class="container">
        <form action="{{ route('customers.update', $customer) }}" method="post">
            @csrf
            @method('put')
            <div class="box" style="margin-top: 10px;">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        Editar de Cliente
                    </h3>
                </div>

                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">

                            @include('general.customers.partials.fields')
                            <div class="row">
                                <div class="col-md-12">
                                    <a href="{{ route('customers.index') }}" class="btn btn-danger" title="cancelar"><i class="fa fa-ban"></i></a>
                                    <button type="submit" class="btn btn-success" title="actualizar"><i class="fa fa-save"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection
@push('scripts')
    <script>
        $(document).ready(function() {
            $('#type_document_id').select2({
                placeholder: "Seleccione un tipo de documento"
            });
        });
    </script>
@endpush
