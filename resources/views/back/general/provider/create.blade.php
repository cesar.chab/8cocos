@extends('layouts.app')

@section('title', 'Registro de Proveedor')

@section('content')
    <div class="col-md-12">
        <form action="{{ route('providers.store') }}" method="post" autocomplete="off">
            <div class="box" style="margin-top: 10px;">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        Registro de Proveedor
                    </h3>
                </div>

                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">
                            @csrf
                            @include('general.provider.partials.fields')
                            <div class="row">
                                <div class="col-md-12">
                                    <a href="{{ route('providers.index') }}" class="btn btn-danger" title="cancelar"><i class="fa fa-ban"></i></a>
                                    <button type="submit" class="btn btn-success" title="Guardar"><i class="fa fa-save"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection
@push('scripts')
    <script>
        $(document).ready(function() {
            $('#type_document_id').select2({
                placeholder: "Seleccione un tipo de documento"
            });
        });
    </script>
@endpush
