<div class="row">
    <div class="col-md-6">
        <div class="form-group has-feedback{{ $errors->has('provider_name') ? ' has-error' : '' }}">
            <label for="provider_name">Razón social</label>
            <input type="text" class="form-control" name="provider_name" value="{{ $provider ? $provider->provider_name : old('provider_name') }}">
            @if ($errors->has('provider_name'))
                <span class="help-block">
                <strong>{{ $errors->first('provider_name') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group has-feedback{{ $errors->has('type_document_id') ? ' has-error' : '' }}">
            <label for="type_document_id">Tipo de Documento</label>
            <select name="type_document_id" id="type_document_id" class="form-control" style="width: 100%">
                <option></option>
                @foreach($type_documents as $document)
                    <option value="{{ $document->id }}" {{ $provider->type_document_id === $document->id ? 'selected' : '' }}>{{ $document->name }}</option>
                @endforeach
            </select>
            @if ($errors->has('type_document_id'))
                <span class="help-block">
                <strong>{{ $errors->first('type_document_id') }}</strong>
            </span>
            @endif
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group has-feedback{{ $errors->has('number_document') ? ' has-error' : '' }}">
            <label for="number_document">Numero de Documento</label>
            <input type="text" class="form-control" name="number_document" value="{{ $provider ? $provider->number_document : old('number_document') }}">
            @if ($errors->has('number_document'))
                <span class="help-block">
                <strong>{{ $errors->first('number_document') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group has-feedback{{ $errors->has('address') ? ' has-error' : '' }}">
            <label for="address">Direccion</label>
            <input type="text" class="form-control" name="address" value="{{ $provider ? $provider->address : old('address') }}">
            @if ($errors->has('address'))
                <span class="help-block">
                <strong>{{ $errors->first('address') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group has-feedback{{ $errors->has('phones') ? ' has-error' : '' }}">
            <label for="phones">Telefono</label>
            <input type="text" class="form-control" name="phones" value="{{ $provider ? $provider->phones : old('phones') }}">
            @if ($errors->has('phones'))
                <span class="help-block">
                <strong>{{ $errors->first('phones') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group has-feedback{{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="email">Email</label>
            <input type="email" class="form-control" name="email" value="{{ $provider ? $provider->email : old('email') }}">
            @if ($errors->has('email'))
                <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="col-md-12">
        <div class="checkbox">
            <label>
                <input type="checkbox" name="status" {{ $provider->status === 1 ? 'checked' : '' }}>Activo
            </label>
        </div>
    </div>
</div>