<div class="row">
    <div class="col-md-6">
            <div class="col-md-12">
                <div class="form-group has-feedback{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="nombre">Nombre</label>
                    <input type="text" class="form-control" name="name" value="{{ $accountingrecord ? $accountingrecord->name : old('name') }}">
                    @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="col-md-12">
            <div class="form-group has-feedback{{ $errors->has('date_from') ? ' has-error' : ''}}" >
                <label for="date_from">Fecha Desde:</label>
            <input type= "text" class="form-control date_from" name="date_from" value="{{ $accountingrecord ? $accountingrecord->date_from : old('date_from') }}">
            @if ($errors->has('date_from'))
                <span class="help-block">
                <strong>{{$errors->first('date_from')}}</strong>
                </span>
            @endif
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group has-feedback{{ $errors->has('date_to') ? ' has-error' : ''}}">
                    <label for="date_to">Fecha Hasta:</label>
                <input type= "text" class="form-control date_to" name="date_to" value="{{ $accountingrecord ? $accountingrecord->date_to : old('date_to') }}">
                @if ($errors->has('date_to'))
                    <span class="help-block">
                    <strong>{{$errors->first('date_to')}}</strong>
                    </span>
                @endif
                    </div>
                </div>
            <div class="col-md-12">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="status" {{ $accountingrecord->status === 1 ? 'checked' : '' }}>Activo
                    </label>
                </div>
            </div>
    </div>
    <div class="col-md-6"></div>
</div>



