@extends('layouts.app')

@section('title', 'Registro de Periodos Contables')

@section('content')
    <div class="container">
        <form action="{{ route('accountingrecord.store') }}" method="post">
            <div class="box" style="margin-top: 10px;">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        Registro de Periodos Contables
                    </h3>
                </div>

                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">
                            @csrf
                            @include('general.periods.partials.fields')
                            <div class="row">
                                <div class="col-md-12">
                                    <a href="{{ route('accountingrecord.index') }}" class="btn btn-danger" title="cancelar"><i class="fa fa-ban"></i></a>
                                    <button type="submit" class="btn btn-success" title="Guardar"><i class="fa fa-save"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection

@push('scripts')
    <script>
        $(document).ready(function() {
            $('.date_from').datetimepicker({
                    format: 'YYYY-MM-DD',
                    locale: 'es'
            });
                $('.date_to').datetimepicker({
                    format: 'YYYY-MM-DD',
                    locale: 'es'
                });
        });
    </script>
@endpush
