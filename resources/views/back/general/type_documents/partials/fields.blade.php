<div class="col-md-6">
    <div class="form-group has-feedback{{ $errors->has('name') ? ' has-error' : '' }}">
        <label for="nombre">Nombre</label>
        <input type="text" class="form-control" name="name" value="{{ $typedocument ? $typedocument->name : old('name') }}">
        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="col-md-12">
    <div class="checkbox">
        <label>
            <input type="checkbox" name="status" {{ $typedocument->status === 1 ? 'checked' : '' }}>Activo
        </label>
    </div>
</div>
