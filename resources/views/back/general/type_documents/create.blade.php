@extends('layouts.app')

@section('title', 'Registro de Tipo De Documento')

@section('content')
    <div class="container">
        <form action="{{ route('typedocument.store') }}" method="post">
            <div class="box" style="margin-top: 10px;">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        Registro de Tipo De Documento
                    </h3>
                </div>

                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">
                            @csrf
                            @include('general.type_documents.partials.fields')
                            <div class="row">
                                <div class="col-md-12">
                                    <a href="{{ route('typedocument.index') }}" class="btn btn-danger" title="cancelar"><i class="fa fa-ban"></i></a>
                                    <button type="submit" class="btn btn-success" title="Guardar"><i class="fa fa-save"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection
