@extends('layouts.app')

@section('title', 'Perfil de Usuario')

@section('content')
    <div class="col-md-12">
        <form action="{{ route('home.user.profile', $user) }}" method="post">
            @method('put')
            @csrf
            <input type="hidden" class="form-control" name="name"  value="{{ $user->name }}">
            <input type="hidden" class="form-control" name="email" value="{{ $user->email }}">
            <div class="box" style="margin-top: 10px;">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        Perfil de Usuario <strong>{{ $user->name }}</strong>
                    </h3>
                </div>

                <div class="box-body">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="">Nombre:</label>
                                <input type="text" disabled value="{{ $user->name }}" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="">Email:</label>
                                <input type="text" disabled value="{{ $user->email }}" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="">Rol:</label>
                                <input type="text" disabled value="{{ $user->roles->count() > 0 ? $user->roles[0]->name : 'n/a' }}" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="">Empleado:</label>
                                <input type="text" disabled value="{{ !empty($user->employee) ? $user->employee->name : 'n/a' }}" class="form-control">
                            </div>
                        </div>
                        <div class="col-xs-6">
                                <div class="form-group has-feedback{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label for="password">Contraseña</label>
                                    <input type="password" class="form-control" name="password" required autocomplete="off">
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                             <a href="{{ route('home') }}" class="btn btn-danger" title="cancelar"><i class="fa fa-ban"></i></a>
                             <button type="submit" class="btn btn-success" title="Guardar"><i class="fa fa-save"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection
