@extends('layouts.app')

@section('title', 'Editar Usuario')

@section('content')
    <div class="container">
        <form action="{{ route('user.update', $user) }}" method="post">
            @csrf
            @method('put')
            <div class="box" style="margin-top: 10px;">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        Editar Usuario
                    </h3>
                </div>

                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">

                            @include('general.user.partials.fields')
                            <div class="row">
                                <div class="col-md-12">
                                    <a href="{{ route('user.index') }}" class="btn btn-danger" title="cancelar"><i class="fa fa-ban"></i></a>
                                    <button type="submit" class="btn btn-success" title="actualizar"><i class="fa fa-save"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection