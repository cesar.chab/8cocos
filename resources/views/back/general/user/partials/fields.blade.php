@if($user->employee_id > 0)
<div class="col-md-12">
    <label for="">Empleado asignado</label>
    <div class="form-group">
        <input type="text" class="form-control" value="{{ $user->employee->name }}" disabled>
    </div>
</div>
@endif

<div class="col-md-6">
    <div class="form-group has-feedback{{ $errors->has('name') ? ' has-error' : '' }}">
        <label for="nombre">Nombre</label>
        <input type="text" class="form-control" name="name" value="{{ $user ? $user->name : old('name') }}">
        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="col-md-6">
    <div class="form-group has-feedback{{ $errors->has('email') ? ' has-error' : '' }}">
        <label for="nombre">Correo Electronico</label>
        <input type="text" class="form-control" name="email" value="{{ $user ? $user->email : old('email') }}">
        @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    </div>
</div>



<div class="col-md-6">
    <div class="form-group has-feedback{{ $errors->has('employee_id') ? ' has-error' : '' }}">
        <label for="empleado">Empleado</label>
            <select name="employee_id" id="employee_id" class="form-control select2" style="width: 100%">
                <option></option>
                @foreach($employees as $employee)
                    <option value="{{ $employee->id }}" {{ $user->employee_id === $employee->id ? 'selected' : '' }}>{{ $employee->name }}</option>
                @endforeach
            </select>
            @if ($errors->has('employee_id'))
                <span class="help-block">
                    <strong>{{ $errors->first('employee_id') }}</strong>
                </span>
            @endif
    </div>
</div>



<div class="col-md-6">
    <div class="form-group has-feedback{{ $errors->has('password') ? ' has-error' : '' }}">
        <label for="nombre">Contraseña</label>
        <input type="text" class="form-control" name="password">
        @if ($errors->has('password'))
            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
    </div>
</div>

@role('Administrador')
<div class="col-md-6">
    <div class="form-group has-feedback{{ $errors->has('rol_id') ? ' has-error' : '' }}">
        <label for="empleado">Rol</label>
        <select name="rol" id="rol" class="form-control select2" style="width: 100%">
            <option></option>
            @foreach($roles as $role)
                <option value="{{ $role->id }}">{{ $role->name }}</option>
            @endforeach
        </select>

    </div>
</div>

@endrole
