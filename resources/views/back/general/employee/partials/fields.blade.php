@if($employee->name)
    <div class="col-md-4">
            <div class="form-group">
                <label>Codigo empleado</label>
                <input type="text" disabled value="{{$employee->code}}" class="form-control">
            </div>
    </div>

    <div class="col-md-8">
        <div class="form-group">
            <label>Nombre Completo</label>
            <input type="text" disabled value="{{$employee->apel_name}}" class="form-control">
        </div>
    </div>
@endif

<div class="col-md-6">
    <div class="form-group has-feedback{{ $errors->has('name') ? ' has-error' : '' }}">
        <label for="nombre">Nombres</label>
        <input type="text" class="form-control" name="name" value="{{ $employee ? $employee->name : old('name') }}" maxlength="100">
        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="col-md-6">
    <div class="form-group has-feedback{{ $errors->has('last_name') ? ' has-error' : '' }}">
        <label for="apellido">Apellidos</label>
        <input type="text" class="form-control" name="last_name" value="{{ $employee ? $employee->last_name : old('last_name') }}" maxlength="100">
        @if ($errors->has('last_name'))
            <span class="help-block">
                <strong>{{ $errors->first('last_name') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="col-md-6">
    <div class="form-group has-feedback{{ $errors->has('position_id') ? ' has-error' : '' }}">
        <label for="puesto">Puesto</label>
        <select name="position_id" id="position_id" class="form-control select2">
            <option></option>
            @foreach($positions as $position)
                <option value="{{ $position->id }}" {{ $employee->position_id === $position->id ? 'selected' : '' }}>{{ $position->name }}</option>
            @endforeach
        </select>
        @if ($errors->has('position_id'))
            <span class="help-block">
                <strong>{{ $errors->first('position_id') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="col-md-12">
    <div class="checkbox">
        <label>
            <input type="checkbox" name="responsable" {{ $employee->responsable === 1 ? 'checked' : '' }}>Responsable de Proyecto?
        </label>
    </div>
</div>


