@extends('layouts.app')

@section('title', 'Editar Puestos')

@section('content')
    <div class="container">
        <form action="{{ route('position.update', $position) }}" method="post">
            @csrf
            @method('put')
            <div class="box" style="margin-top: 10px;">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        Editar Puestos
                    </h3>
                </div>

                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">

                            @include('general.position.partials.fields')
                            <div class="row">
                                <div class="col-md-12">
                                    <a href="{{ route('position.index') }}" class="btn btn-danger" title="cancelar"><i class="fa fa-ban"></i></a>
                                    <button type="submit" class="btn btn-success" title="actualizar"><i class="fa fa-save"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection