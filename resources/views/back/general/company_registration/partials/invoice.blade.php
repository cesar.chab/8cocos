<div class="col-md-4">
    <div class="form-group has-feedback{{ $errors->has('invoice_number') ? ' has-error' : '' }}">
        <label for="invoice_number">Numero de Factura</label>
        <input type="text" class="form-control" name="invoice_number" value="{{ $company_registration ? $company_registration->invoice_number : old('invoice_number') }}" maxlength="100">
        @if ($errors->has('invoice_number'))
            <span class="help-block">
                <strong>{{ $errors->first('invoice_number') }}</strong>
            </span>
        @endif
    </div>
</div>


<div class="col-md-4">
    <div class="form-group has-feedback{{ $errors->has('invoice_serial') ? ' has-error' : '' }}">
        <label for="invoice_serial">Serial de Factura</label>
        <input type="text" class="form-control" name="invoice_serial" value="{{ $company_registration ? $company_registration->invoice_serial : old('invoice_serial') }}" maxlength="100">
        @if ($errors->has('invoice_serial'))
            <span class="help-block">
                <strong>{{ $errors->first('invoice_serial') }}</strong>
            </span>
        @endif
    </div>
</div>


<div class="col-md-4">
    <div class="form-group has-feedback{{ $errors->has('invoice_last_number') ? ' has-error' : '' }}">
        <label for="invoice_last_number">Numero primario de Factura</label>
        <input type="text" class="form-control" name="invoice_last_number" value="{{ $company_registration ? $company_registration->invoice_last_number : old('invoice_last_number') }}" maxlength="100">
        @if ($errors->has('invoice_last_number'))
            <span class="help-block">
                <strong>{{ $errors->first('invoice_last_number') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="col-md-4">
    <div class="form-group has-feedback{{ $errors->has('invoice_bol_number') ? ' has-error' : '' }}">
        <label for="invoice_bol_number">Numero de Boleta</label>
        <input type="text" class="form-control" name="invoice_bol_number" value="{{ $company_registration ? $company_registration->invoice_bol_number : old('invoice_bol_number') }}" maxlength="100">
        @if ($errors->has('invoice_bol_number'))
            <span class="help-block">
                <strong>{{ $errors->first('invoice_bol_number') }}</strong>
            </span>
        @endif
    </div>
</div>


<div class="col-md-4">
    <div class="form-group has-feedback{{ $errors->has('invoice_bol_serial') ? ' has-error' : '' }}">
        <label for="invoice_bol_serial">Serial de Boleta</label>
        <input type="text" class="form-control" name="invoice_bol_serial" value="{{ $company_registration ? $company_registration->invoice_bol_serial : old('invoice_bol_serial') }}" maxlength="100">
        @if ($errors->has('invoice_bol_serial'))
            <span class="help-block">
                <strong>{{ $errors->first('invoice_bol_serial') }}</strong>
            </span>
        @endif
    </div>
</div>


<div class="col-md-4">
    <div class="form-group has-feedback{{ $errors->has('invoice_bol_last_number') ? ' has-error' : '' }}">
        <label for="invoice_bol_last_number">Numero Primario de Boleta</label>
        <input type="text" class="form-control" name="invoice_bol_last_number" value="{{ $company_registration ? $company_registration->invoice_bol_last_number : old('invoice_bol_last_number') }}" maxlength="100">
        @if ($errors->has('invoice_bol_last_number'))
            <span class="help-block">
                <strong>{{ $errors->first('invoice_bol_last_number') }}</strong>
            </span>
        @endif
    </div>
</div>


<div class="col-md-12">
    <div class="checkbox">
        <label>
            <input type="checkbox" name="item_include_tax" {{ $company_registration->item_include_tax === 'Y' ? 'checked' : '' }}>Activar Impuesto
        </label>
    </div>
</div>
