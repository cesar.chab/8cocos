<div class="col-md-4">
    <div class="form-group has-feedback{{ $errors->has('name') ? ' has-error' : '' }}">
        <label for="nombre">Nombre</label>
        <input type="text" class="form-control" name="name" value="{{ $company_registration ? $company_registration->name : old('name') }}" maxlength="100">
        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="col-md-4">
    <div class="form-group has-feedback{{ $errors->has('bill_to_name') ? ' has-error' : '' }}">
        <label for="bill_to_name">Nombre Comercial</label>
        <input type="text" class="form-control" name="bill_to_name" value="{{ $company_registration ? $company_registration->bill_to_name : old('bill_to_name') }}" maxlength="100">
        @if ($errors->has('bill_to_name'))
            <span class="help-block">
                <strong>{{ $errors->first('bill_to_name') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="col-md-4">
    <div class="form-group has-feedback{{ $errors->has('number_identification') ? ' has-error' : '' }}">
        <label for="number_identification">RUC</label>
        <input type="text" class="form-control" name="number_identification" value="{{ $company_registration ? $company_registration->number_identification : old('number_identification') }}" maxlength="100">
        @if ($errors->has('number_identification'))
            <span class="help-block">
                <strong>{{ $errors->first('number_identification') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="col-md-4">
    <div class="form-group has-feedback{{ $errors->has('address') ? ' has-error' : '' }}">
        <label for="address">Direccion</label>
        <input type="text" class="form-control" name="address" value="{{ $company_registration ? $company_registration->address : old('address') }}" maxlength="100">
        @if ($errors->has('address'))
            <span class="help-block">
                <strong>{{ $errors->first('address') }}</strong>
            </span>
        @endif
    </div>
</div>


<div class="col-md-4">
    <div class="form-group has-feedback{{ $errors->has('city') ? ' has-error' : '' }}">
        <label for="city">Provincia</label>
        <input type="text" class="form-control" name="city" value="{{ $company_registration ? $company_registration->city : old('city') }}" maxlength="100">
        @if ($errors->has('city'))
            <span class="help-block">
                <strong>{{ $errors->first('city') }}</strong>
            </span>
        @endif
    </div>
</div>


<div class="col-md-4">
    <div class="form-group has-feedback{{ $errors->has('code_postal') ? ' has-error' : '' }}">
        <label for="code_postal">Codigo Postal</label>
        <input type="text" class="form-control" name="code_postal" value="{{ $company_registration ? $company_registration->code_postal : old('code_postal') }}" maxlength="100">
        @if ($errors->has('code_postal'))
            <span class="help-block">
                <strong>{{ $errors->first('code_postal') }}</strong>
            </span>
        @endif
    </div>
</div>


<div class="col-md-4">
    <div class="form-group has-feedback{{ $errors->has('telephone') ? ' has-error' : '' }}">
        <label for="telephone">Telefono</label>
        <input type="text" class="form-control" name="telephone" value="{{ $company_registration ? $company_registration->telephone : old('telephone') }}" maxlength="100">
        @if ($errors->has('telephone'))
            <span class="help-block">
                <strong>{{ $errors->first('telephone') }}</strong>
            </span>
        @endif
    </div>
</div>


<div class="col-md-4">
    <div class="form-group has-feedback{{ $errors->has('email') ? ' has-error' : '' }}">
        <label for="email">Correo</label>
        <input type="text" class="form-control" name="email" value="{{ $company_registration ? $company_registration->email : old('email') }}" maxlength="100">
        @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="col-md-4">
    <div class="form-group has-feedback{{ $errors->has('currency_code') ? ' has-error' : '' }}">
        <label for="currency_code">Codigo de Moneda</label>
        <input type="text" class="form-control" name="currency_code" value="{{ $company_registration ? $company_registration->currency_code : old('currency_code') }}" maxlength="100">
        @if ($errors->has('currency_code'))
            <span class="help-block">
                <strong>{{ $errors->first('currency_code') }}</strong>
            </span>
        @endif
    </div>
</div>


<div class="col-md-4">
    <div class="form-group has-feedback{{ $errors->has('tax_value') ? ' has-error' : '' }}">
        <label for="tax_value">Impuesto</label>
        <input type="number" class="form-control" name="tax_value" value="{{ $company_registration ? $company_registration->tax_value : old('tax_value') }}" maxlength="100">
        @if ($errors->has('tax_value'))
            <span class="help-block">
                <strong>{{ $errors->first('tax_value') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="col-md-4">
    <div class="form-group has-feedback{{ $errors->has('desc_long_currency') ? ' has-error' : '' }}">
        <label for="desc_long_currency">desc_long_currency</label>
        <input type="text" class="form-control" name="desc_long_currency" value="{{ $company_registration ? $company_registration->desc_long_currency : old('desc_long_currency') }}" maxlength="100">
        @if ($errors->has('desc_long_currency'))
            <span class="help-block">
                <strong>{{ $errors->first('desc_long_currency') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="col-md-4">
    <div class="form-group has-feedback{{ $errors->has('description') ? ' has-error' : '' }}">
        <label for="description">Descripcion</label>
        <input type="text" class="form-control" name="description" value="{{ $company_registration ? $company_registration->description : old('description') }}" maxlength="100">
        @if ($errors->has('description'))
            <span class="help-block">
                <strong>{{ $errors->first('description') }}</strong>
            </span>
        @endif
    </div>
</div>

