<div class="col-md-4">
    <div class="form-group has-feedback{{ $errors->has('path_image_bg') ? ' has-error' : '' }}">
        <label for="path_image_bg">Imagen</label>
        <input type="file" class="form-control" name="path_image_bg">
        @if ($errors->has('path_image_bg'))
            <span class="help-block">
                <strong>{{ $errors->first('path_image_bg') }}</strong>
            </span>
        @endif
    </div>
</div>


<div class="col-md-4">
    <div class="form-group has-feedback{{ $errors->has('path_image_logo') ? ' has-error' : '' }}">
        <label for="path_image_logo">Logo</label>
        <input type="file" class="form-control" name="path_image_logo">
        @if ($errors->has('path_image_logo'))
            <span class="help-block">
                <strong>{{ $errors->first('path_image_logo') }}</strong>
            </span>
        @endif
    </div>
</div>