@extends('layouts.app')

@section('title', 'Registro de Empresa')

@section('content')
    <div class="container">
        <form action="{{ route('company_registration.store') }}" method="post" enctype="multipart/form-data">
            <div class="box" style="margin-top: 10px;">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        Registro de Empresa
                    </h3>
                </div>

                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">
                            @csrf
                            <div class="nav-tabs-custom"> 
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#tab_1" data-toggle="tab">INF. GENERAL</a>
                                    </li>
                                    <li>
                                        <a href="#tab_2" data-toggle="tab">FACTURACION ELECTRONICA</a>
                                    </li>
                                    <li>
                                        <a href="#tab_3" data-toggle="tab">OTROS</a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_1">								 
                                        <div class="row">
                                            <div class="col-md-12">
                                                @include('general.company_registration.partials.fields')
                                            </div> 									
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab_2">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    @include('general.company_registration.partials.invoice')								
                                                </div>              
                                            </div>
                                    </div>
                                    <div class="tab-pane" id="tab_3">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    @include('general.company_registration.partials.others')								
                                                </div>              
                                            </div>
                                    </div>
                                </div>
                            </div>





                            <div class="row">
                                <div class="col-md-12">
                                    <a href="{{ route('company_registration.index') }}" class="btn btn-danger" title="cancelar"><i class="fa fa-ban"></i></a>
                                    <button type="submit" class="btn btn-success" title="Guardar"><i class="fa fa-save"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection
@push('scripts')
    <script>
        $(document).ready(function() {
            $('#position_id').select2({
                placeholder: "Selecciona el puesto"
            });
        });
    </script>
@endpush