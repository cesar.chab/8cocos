<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>{{ config('app.name') }}</title>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Theme style -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.3/css/AdminLTE.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.3/css/skins/_all-skins.min.css">

    <!-- iCheck -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/square/_all.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css">

    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/css/bootstrap-datetimepicker.min.css">

    <!-- Calendar -->
    <link rel="stylesheet" href="{{asset('assets/fullcalendar/dist/fullcalendar.css')}}"/>

    <!--css drpzone -->
    <link rel="stylesheet" href="{{ asset('css/dropzone.css')}}">

    <!-- timepicker -->
    <link rel="stylesheet" href="{{asset('css/jquery.clockinput.min.css')}}">

    @include('layouts.datatables_css')
    @yield('header_styles')
    @yield('css')
</head>

<body class="skin-blue sidebar-mini">
        @if (!Auth::guest())
            <div class="wrapper">
                <!-- Main Header -->
                <header class="main-header">

                    <!-- Logo -->
                    <a href="#" class="logo">
                        <b>{{ config('app.name') }}</b>
                    </a>

                    <!-- Header Navbar -->
                    <nav class="navbar navbar-static-top" role="navigation">
                        <!-- Sidebar toggle button-->
                        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                            <span class="sr-only">Toggle navigation</span>
                        </a>
                        <!-- Navbar Right Menu -->
                        <div class="navbar-custom-menu">
                            <ul class="nav navbar-nav">
                                <!-- User Account Menu -->
                                @if(\Illuminate\Support\Facades\Auth::user()->employee_id)
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-crop"></i> Opciones <span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="{{ route('assistance.index')}}"><i class="fa fa-calendar"></i>Registro de Horas</a></li>
                                        <li><a href="{{ route('my-report.index') }}"><i class="fa fa-file-excel-o"></i>Mis reportes de horas</a></li>
                                    </ul>
                                </li>
                                @endif

                                <li class="dropdown user user-menu">
                                    <!-- Menu Toggle Button -->
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <!-- The user image in the navbar-->
                                        <img src="{{ asset('images/images.jpg') }}"
                                             class="user-image" alt="User Image"/>
                                        <!-- hidden-xs hides the username on small devices so only the image appears. -->
                                        <span class="hidden-xs">{{ Auth::user()->name }}</span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <!-- The user image in the menu -->
                                        <li class="user-header">
                                            <img src="{{ asset('images/images.jpg') }}"
                                                 class="img-circle" alt="User Image"/>
                                            <p>
                                                {{ Auth::user()->name }}
                                                <small>Miembro desde {{ Auth::user()->created_at->format('M. Y') }}</small>
                                            </p>
                                        </li>
                                        <!-- Menu Footer-->
                                        <li class="user-footer">
                                            <div class="pull-left">
                                                <a href="{{ route('home.user.profile.show', Auth::user()) }}" class="btn btn-default btn-flat">Perfil</a>
                                            </div>
                                            <div class="pull-right">
                                                <a href="{{ url('/logout') }}" class="btn btn-default btn-flat"
                                                    onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                                    cerrar sesión
                                                </a>
                                                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                                    @csrf
                                                </form>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </header>

                <!-- Left side column. contains the logo and sidebar -->
                @include('layouts.sidebar')
                <!-- Content Wrapper. Contains page content -->
                <div class="content-wrapper">
                    @yield('content')
                </div>

                <!-- Main Footer -->
                <footer class="main-footer" style="max-height: 100px;text-align: center">
                    <strong>Copyright © {{ date('Y') }} <a href="#">{{ config('app.name') }}</a>.</strong> todos los derechos reservados.
                </footer>

            </div>
        @else
            <nav class="navbar navbar-default navbar-static-top">
                <div class="container">
                    <div class="navbar-header">

                        <!-- Collapsed Hamburger -->
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#app-navbar-collapse">
                            <span class="sr-only">Toggle Navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <!-- Branding Image -->
                        <a class="navbar-brand" href="{{ url('/') }}">
                            Marcación
                        </a>
                    </div>

                    <div class="collapse navbar-collapse" id="app-navbar-collapse">
                        <!-- Left Side Of Navbar -->
                        <ul class="nav navbar-nav">
                            <li><a href="{{ url('/home') }}">Escritorio</a></li>
                        </ul>

                        <!-- Right Side Of Navbar -->
                        <ul class="nav navbar-nav navbar-right">
                            <!-- Authentication Links -->
                            <li><a href="{{ url('/login') }}">Login</a></li>
                            <li><a href="{{ url('/register') }}">Registro</a></li>
                        </ul>
                    </div>
                </div>
            </nav>

            <div id="page-content-wrapper">
                <div class="container">
                    <div class="row">
                            @yield('content')
                    </div>
                </div>
            </div>
    @endif

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
        <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

        <!-- AdminLTE App -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.3/js/adminlte.min.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
        <script src="{{ asset('js/sweetalert2.all.min.js') }}"></script>
        <script src="{{ asset('js/dropzone.js') }}"></script> <!-- Plugins drop file-->
        <!-- Calendar script -->
        <script src="{{asset('assets/fullcalendar/dist/fullcalendar.js')}}"></script>
        <script src="{{ asset('assets/fullcalendar/dist/locale-all.js') }}"></script>
        <script src="{{ asset('js/jquery.clockinput.min.js') }}"></script>
        <!-- jquery calculate times -->



    @stack('scripts')
        @include('layouts.datatables_js')
</body>
</html>
