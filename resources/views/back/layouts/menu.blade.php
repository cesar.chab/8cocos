
<li class="header"></li>

<li class="{{ (request()->segment(1) == 'home') ? 'active' : '' }}">
    <a href="{{ url('/home') }}">
        <i class="fa fa-dashboard"></i> <span>Escritorio</span>
    </a>
</li>

@role('Administrador')
<li class="treeview {{ (request()->segment(1) === 'general') ? 'menu-open' : '' }}">
     
	<li class="{{ (request()->is('general/dailyrate*')) ? 'active' : '' }}"><a href="{{ route('dailyrate.index') }}"><i class="fa fa-circle-o"></i>Tipo de cambio</a></li>

	<li class="{{ (request()->is('general/customers*')) ? 'active' : '' }}"><a href="{{route('customers.index')}}"><i class="fa fa-circle-o"></i>Clientes</a></li>

   <li class="{{ (request()->is('general/dailyrate*')) ? 'active' : '' }}"><a href="{{ route('dailyrate.index') }}"><i class="fa fa-circle-o"></i>Movimientos del Dia</a></li>
		 
</li>
@endrole

 

@role('Administrador')
<li class="treeview {{ (request()->segment(1) === 'administrative') ? 'menu-open' : '' }}">
    <a href="#">
        <i class="fa fa-book"></i>
        <span>Administracion</span>
        <i class="fa fa-angle-left pull-right"></i>
    </a>
    <ul class="treeview-menu" style="{{ (request()->segment(1) === 'administrative') ? 'display: block' : '' }}">
       
		<li class="{{ (request()->is('general/user*')) ? 'active' : '' }}"><a href="{{route('user.index')}}"><i class="fa fa-circle-o"></i>Usuarios</a></li>
	
		<li class="{{ (request()->is('general/typedocument*')) ? 'active' : '' }}"><a href="{{route('typedocument.index')}}"><i class="fa fa-circle-o"></i>Tipo Documentos</a></li>
        <li class="{{ (request()->is('general/provider*')) ? 'active' : '' }}"><a href="{{route('providers.index')}}"><i class="fa fa-circle-o"></i>Ocupacion</a></li>

		<li class="{{ (request()->is('general/provider*')) ? 'active' : '' }}"><a href="{{route('providers.index')}}"><i class="fa fa-circle-o"></i>Nacioanalidad</a></li>
        <li class="{{ (request()->is('general/fndlookup*')) ? 'active' : '' }}"><a href="{{route('fndlookup.index')}}"><i class="fa fa-circle-o"></i>Registros de Tablas</a></li>
		<li class="{{ (request()->is('general/dailyrate*')) ? 'active' : '' }}"><a href="{{ route('dailyrate.index') }}"><i class="fa fa-circle-o"></i>Consulta de Movimientos</a></li>
		 
    </ul>
</li>
 
@endrole

@role('Administrador')
<li class="treeview {{ (request()->segment(1) === 'reports') ? 'menu-open' : '' }}">
    <a href="#">
        <i class="fa fa-book"></i>
        <span>Reportes</span>
        <i class="fa fa-angle-left pull-right"></i>
    </a>
    <ul class="treeview-menu" style="{{ (request()->segment(1) === 'reports') ? 'display: block' : '' }}">

        <li class="{{ (request()->is('reports/report-project-activity')) ? 'active' : '' }}"> <a href="{{ route('report.project.activity') }}"><i class="fa fa-circle-o"></i>Seguimiento de Actividades</a></li>
    </ul>
</li>
 
@endrole

<li class="{{ (request()->is('help')) ? 'active' : '' }}">
    <a href="{{ route('page.help')}}"><i class="fa fa-question"></i>Ayuda</a>
</li>
