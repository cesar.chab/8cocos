@extends('layouts.app')
@section ('content')

    <div class="box" style="margin-top: 10px;">
        <div class="row">
            <div class="col-md-12">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        Acerca de...
                    </h3>

                </div>
                <div class="box-body">
                    <div class="col-md-12">
                        <table>
                            <tr><th>Desarrollado Por:</th><td>ASVNETS S.A.C</td></tr>
                            <tr><th>Email:</th><td>info@asvnets.com</td></tr>
                            <tr><th>Whatsapp:</th><td>932 266 980</td></tr>
                            <tr><th>Facebook:</th><td></td></tr>

                            <tr><th>Página web:</th><td><a href="www.asvnets.com" target="_blank">www.asvnets.com</a></td></tr>
                            <tr><th>Otros proyectos:</th><td><a href="www.asvnets.com/p/tienda.html" target="_blank">www.asvnets.com/p/tienda.html</a></td></tr>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

