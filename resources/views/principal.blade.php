<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Inicio - 8COCOS</title>
    <link rel="stylesheet" href="8cocos/css.css?family=Open%20Sans%3A100%2C200%2C300%2C400%2C500%2C600%2C700%2C800%2C900%2C100i%2C200i%2C300i%2C400i%2C500i%2C600i%2C700i%2C800i%2C900i%7CMontserrat%3A300%2C400%2C500%2C600%2C700%2C900%7CRoboto%3A300%2C400%2C700&#038;subset=latin&#038;display=swap">
    <meta name="description" content="El mejor tipo de cambio para cambiar dólares y soles online en Lima, Perú">
    <link href='https://fonts.gstatic.com' crossorigin="" rel='preconnect'>
    <link rel='stylesheet' id='js_composer_front-css' href='{{asset('8cocos/dist/plugins/js_composer/assets/css/js_composer.min.css')}}' type='text/css' media='all'>
    <link rel='stylesheet' id='parent-style-css' href='{{asset('8cocos/dist/themes/Total/style.css')}}' type='text/css' media='all'>
    <link rel='stylesheet' id='wpex-style-css' href='{{asset('8cocos/dist/themes/total-child-theme/style.css')}}' type='text/css' media='all'>
    <link rel='stylesheet' id='wpex-visual-composer-css' href='{{asset('8cocos/dist/themes/Total/assets/css/wpex-visual-composer.css')}}' type='text/css' media='all'>
    <link rel='stylesheet' id='bsf-Defaults-css' href='{{asset('8cocos/dist/css/Defaults/Defaults.css')}}' type='text/css' media='all'>
    <link rel='stylesheet' id='ultimate-style-css' href='{{asset('8cocos/dist/plugins/Ultimate_VC_Addons/assets/min-css/style.min.css')}}' type='text/css' media='all'>
    <link rel='stylesheet' id='ult-slick-css' href='{{asset('8cocos/dist/plugins/Ultimate_VC_Addons/assets/min-css/slick.min.css')}}' type='text/css' media='all'>
    <link rel='stylesheet' id='ult-icons-css' href='{{asset('8cocos/dist/plugins/Ultimate_VC_Addons/assets/css/icons.css')}}' type='text/css' media='all'>
    <link rel='stylesheet' id='ultimate-animate-css' href='{{asset('8cocos/dist/plugins/Ultimate_VC_Addons/assets/min-css/animate.min.css')}}' type='text/css' media='all'>
    <script type="text/javascript" src='{{asset('8cocos/dist/js/jquery/jquery.js')}}' id='jquery-core-js'></script>
    <script type="text/javascript" src='{{asset('8cocos/dist/plugins/Ultimate_VC_Addons/assets/min-js/ultimate-params.min.js')}}' id='ultimate-vc-params-js' defer=""></script>
    <script type="text/javascript" src='{{asset('8cocos/dist/plugins/Ultimate_VC_Addons/assets/min-js/slick.min.js')}}' id='ult-slick-js' defer=""></script>
    <script type="text/javascript" src='{{asset('8cocos/dist/plugins/Ultimate_VC_Addons/assets/min-js/jquery-appear.min.js')}}' id='ultimate-appear-js' defer=""></script>
    <script type="text/javascript" src='{{asset('8cocos/dist/plugins/Ultimate_VC_Addons/assets/min-js/slick-custom.min.js')}}' id='ult-slick-custom-js' defer=""></script>

    <link type="text/css" href="{{asset('8cocos/dist/plugins/bootstrap/4.3.1/css/bootstrap.min.css')}}" rel="stylesheet">
    <link type="text/css" href="{{asset('8cocos/dist/plugins/font-awesome/4.7.0/css/font-awesome.min.css')}}" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="{{asset('8cocos/dist/plugins/sweetalert2/sweetalert2.min.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('8cocos/dist/plugins/animate/animate.min.css')}}">
    
    <link rel="icon" href="{{asset('8cocos/imagenes/logo/logofinal.png')}}" sizes="32x32">
    <link rel="shortcut icon" href="{{asset('8cocos/imagenes/logo/logofinal.png')}}"><noscript>
        <style>
            body .wpex-vc-row-stretched,
            body .vc_row-o-full-height {
                visibility: visible;
            }

        </style>
    </noscript>
    <style type="text/css" data-type="vc_shortcodes-custom-css">
        .vc_custom_1586238361281 {
            background-image: url("8cocos/imagenes/svg/dineroinicio.png") !important;
        }

        .vc_custom_1586238383033 {
            background-image: url("8cocos/imagenes/escarapela/icone.png") !important;
        }

        .vc_custom_1587586673409 {
            margin-top: 15px !important;
            margin-bottom: 0px !important;
            padding-bottom: 0px !important;
        }

        .vc_custom_1587586653494 {
            margin-bottom: 0px !important;
            padding-top: 15px !important;
        }

        .vc_custom_1587588436404 {
            margin-bottom: 0px !important;
            padding-top: 15px !important;
        }

    </style><noscript>
        <style>
            .wpb_animate_when_almost_visible {
                opacity: 1;
            }

        </style>
    </noscript>
    <style type="text/css" data-type="wpex-css" id="wpex-css">
        /*TYPOGRAPHY*/

        body {
            font-family: "Open Sans", "Helvetica Neue", Arial, sans-serif
        }

        /*CUSTOMIZER STYLING*/

        @media only screen and (min-width:960px) {

            body.has-sidebar .content-area,
            .wpex-content-w {
                width: 64%
            }
        }

        @media only screen and (min-width:960px) {

            body.has-sidebar .content-area,
            .wpex-content-w {
                max-width: 64%
            }
        }

        @media only screen and (min-width:960px) {
            #sidebar {
                width: 34%
            }
        }

        @media only screen and (min-width:960px) {
            #sidebar {
                max-width: 34%
            }
        }

        .full-width-main-layout .container,
        .full-width-main-layout .vc_row-fluid.container,
        .boxed-main-layout #wrap {
            width: 1300px
        }

        #site-header #site-header-inner {
            padding-top: 0
        }

        #site-header.overlay-header #site-header-inner {
            padding-top: 0;
            padding-bottom: 0
        }

        #site-header-inner {
            padding-bottom: 0
        }

        #site-logo {
            padding-top: 14px;
            padding-bottom: 14px
        }

        #site-navigation-wrap {
            background-color: #ffffff
        }

        #site-navigation-sticky-wrapper.is-sticky #site-navigation-wrap {
            background-color: #ffffff
        }

        #sidr-main {
            background-color: #ffffff
        }

        #sidr-main,
        .sidr-class-wpex-close {
            color: #060f26
        }

        .vc_column-inner {
            margin-bottom: 40px
        }

    </style><noscript>
        <style id="rocket-lazyload-nojs-css">
            .rll-youtube-player,
            [data-lazy-src] {
                display: none !important;
            }

        </style>
    </noscript>
</head>

<body class="home page-template-default page page-id-73 wp-custom-logo wpex-theme wpex-responsive full-width-main-layout has-composer wpex-live-site content-full-width sidebar-widget-icons hasnt-overlay-header page-header-disabled wpex-mobile-toggle-menu-icon_buttons has-mobile-menu wpb-js-composer js-comp-ver-6.3.0 vc_responsive">
    <div class="wpex-site-overlay"></div>
    <a href="#content" class="skip-to-content">Saltear al contenido principal</a><span data-ls_id="#site_top"></span>
    <div id="outer-wrap" class="clr">
        <div id="wrap" class="clr">
            <header id="site-header" class="header-one fixed-scroll dyn-styles clr">
                <div id="site-header-inner" class="container clr">
                    <div id="site-logo" class="site-branding clr header-one-logo">
                        <div id="site-logo-inner" class="clr">
                            <a href="{{url('/')}}" rel="home" class="main-logo"><img src="8cocos/imagenes/logo/logofinal.png" alt="8COCOS" class="logo-img" data-no-retina="data-no-retina" width="180" height="69"></a>
                        </div>
                    </div>
                    <div id="site-navigation-wrap" class="navbar-style-one wpex-dropdowns-caret hide-at-mm-breakpoint clr">
                        <nav id="site-navigation" class="navigation main-navigation clr" aria-label="Main menu">
                            <ul id="menu-principal" class="dropdown-menu sf-menu">
                                <li id="menu-item-1217" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1217"><a href="{{route('inicio')}}"><span class="link-inner">Inicio</span></a></li>
                                <li id="menu-item-1217" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1217"><a href="{{route('nosotros')}}"><span class="link-inner">Nosotros</span></a></li>
                                <li id="menu-item-1219" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1219"><a href="{{route('blog')}}"><span class="link-inner">Blog</span></a></li>
                                <li id="menu-item-1218" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1218"><a href="{{route('ayuda')}}"><span class="link-inner">Ayuda</span></a></li>
                                <li id="menu-item-1199" class="menu-login menu-item menu-item-type-custom menu-item-object-custom menu-item-1199"><a href="{{route('acceso')}}"><span class="link-inner">Iniciar sesión</span></a></li>
                                <li id="menu-item-1200" class="menu-registro menu-item menu-item-type-custom menu-item-object-custom menu-item-1200"><a href="{{route('tipo_perfil')}}"><span class="link-inner">Regístrate</span></a></li>
                            </ul>
                        </nav>
                    </div>
                    <div id="mobile-menu" class="wpex-mobile-menu-toggle show-at-mm-breakpoint wpex-clr"><a href="#" class="mobile-menu-toggle" aria-label="Toggle mobile menu"><span class="wpex-bars" aria-hidden="true"><span></span></span><span class="screen-reader-text">Open Mobile Menu</span></a></div>
                </div>
            </header>
            <!-- Main -->
            @yield('contenido')
            <!-- End Mian -->
            <div id="footer-builder" class="footer-builder clr">
                <div class="footer-builder-content clr container entry">
                    <div id="footer" data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid wpex-vc-row-stretched">
                        <div class="wpb_column vc_column_container vc_col-sm-12">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <div class="vc_row wpb_row vc_inner vc_row-fluid footer-menus no-bottom-margins">
                                        <div class="wpb_column vc_column_container vc_col-sm-3 vc_col-xs-6">
                                            <div class="vc_column-inner">
                                                <div class="wpb_wrapper">
                                                    <div class="wpb_text_column wpb_content_element ">
                                                        <div class="wpb_wrapper">
                                                            <h3>Empresa</h3>
                                                        </div>
                                                    </div>
                                                    <div class="vc_wp_custommenu wpb_content_element">
                                                        <div class="widget widget_nav_menu">
                                                            <div class="menu-empresa-container">
                                                                <ul id="menu-empresa" class="menu">
                                                                    <li id="menu-item-1214" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1214"><a href="{{url('nosotros')}}">Nosotros</a></li>
                                                                    <li id="menu-item-1220" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1220"><a href="{{url('blog')}}">Blog</a></li>
                                                                    <li id="menu-item-1193" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1193"><a href="{{url('ayuda')}}">Ayuda</a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="wpb_column vc_column_container vc_col-sm-3 vc_col-xs-6">
                                            <div class="vc_column-inner">
                                                <div class="wpb_wrapper">
                                                    <div class="wpb_text_column wpb_content_element ">
                                                        <div class="wpb_wrapper">
                                                            <h3>Ayuda</h3>
                                                        </div>
                                                    </div>
                                                    <div class="vc_wp_custommenu wpb_content_element">
                                                        <div class="widget widget_nav_menu">
                                                            <div class="menu-ayuda-container">
                                                                <ul id="menu-ayuda" class="menu">
                                                                    <li id="menu-item-1198" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1198"><a href="{{url('ayuda')}}">Atención al Cliente</a></li>
                                                                    <li id="menu-item-1215" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1215"><a href="{{url('ayuda')}}">Preguntas Frecuentes</a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="wpb_column col-legal vc_column_container vc_col-sm-3 vc_col-xs-6">
                                            <div class="vc_column-inner">
                                                <div class="wpb_wrapper">
                                                    <div class="wpb_text_column wpb_content_element ">
                                                        <div class="wpb_wrapper">
                                                            <h3>Legal</h3>
                                                        </div>
                                                    </div>
                                                    <div class="vc_wp_custommenu wpb_content_element">
                                                        <div class="widget widget_nav_menu">
                                                            <div class="menu-legal-container">
                                                                <ul id="menu-legal" class="menu">
                                                                    <li id="menu-item-1194" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1194"><a href="8cocos/imagenes/adjunto/terminos_condiciones.pdf">Términos y Condiciones</a></li>
                                                                    <li id="menu-item-1195" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1195"><a href="8cocos/imagenes/adjunto/politica_privacidad.pdf">Políticas de Privacidad</a></li>
                                                                    <li id="menu-item-1196" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1196"><a href="librodereclamaciones.html">Libro de Reclamaciones</a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="wpb_column footer-menus--last vc_column_container vc_col-sm-3 vc_col-xs-6">
                                            <div class="vc_column-inner">
                                                <div class="wpb_wrapper">
                                                    <div class="wpb_text_column wpb_content_element ">
                                                        <div class="wpb_wrapper">
                                                            <h3>Horarios</h3>
                                                        </div>
                                                    </div>
                                                    <div class="wpb_text_column wpb_content_element ">
                                                        <div class="wpb_wrapper">
                                                            <p>Lunes a viernes<br>
                                                                <strong>9:00 AM – 7:00 PM</strong></p>
                                                            <p>Sábado<br>
                                                                <strong>11:00 AM – 2:00 PM</strong></p>
                                                            <p>Atención telefónica:<br>
                                                                <strong>+51 954 158 445</strong>
                                                                <a href="https://api.whatsapp.com/send?phone=+51954158445">
                                                                    <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/6b/WhatsApp.svg/240px-WhatsApp.svg.png" width="40" height="40"></a>
                                                            </p>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="vc_row wpb_row vc_inner vc_row-fluid footer-row-logo">
                                        <div class="wpb_column vc_column_container vc_col-sm-12">
                                            <div class="vc_column-inner">

                                            </div>
                                        </div>
                                    </div>
                                    <div class="vc_row wpb_row vc_inner vc_row-fluid no-bottom-margins">
                                        <div class="wpb_column vc_column_container vc_col-sm-6">
                                            <div class="vc_column-inner">
                                                <div class="wpb_wrapper">
                                                    <div class="wpb_text_column wpb_content_element  footer-copy">
                                                        <div class="wpb_wrapper">
                                                            <p>©8COCO$ Todos los derechos reservados</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="wpb_column vc_column_container vc_col-sm-6">
                                            <div class="vc_column-inner">
                                                <div class="wpb_wrapper">
                                                    <div class="wpb_text_column wpb_content_element  footer-datos">
                                                        <div class="wpb_wrapper">
                                                            <p>8COCO$ E.I.R.L. RUC 20606423200<br> Registro SBS:<br> Jr. Jorge Chaves Mza. Q Lote. 21 Dpto. 302, Callao</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="vc_row-full-width vc_clearfix"></div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/html" id="wpb-modifications"></script>
    <script type="text/javascript" id='wpex-core-js-extra'>
        /* <![CDATA[ */
        var wpexLocalize = {
            "isRTL": ""
            , "mainLayout": "full-width"
            , "menuSearchStyle": "disabled"
            , "siteHeaderStyle": "one"
            , "megaMenuJS": "1"
            , "superfishDelay": "600"
            , "superfishSpeed": "fast"
            , "superfishSpeedOut": "fast"
            , "menuWidgetAccordion": "1"
            , "hasMobileMenu": "1"
            , "mobileMenuBreakpoint": "959"
            , "mobileMenuStyle": "sidr"
            , "mobileMenuToggleStyle": "icon_buttons"
            , "scrollToHash": "1"
            , "scrollToHashTimeout": "500"
            , "localScrollUpdateHash": ""
            , "localScrollHighlight": "1"
            , "localScrollSpeed": "1000"
            , "localScrollEasing": "easeInOutExpo"
            , "scrollTopSpeed": "1000"
            , "scrollTopOffset": "100"
            , "lightboxType": "iLightbox"
            , "customSelects": ".woocommerce-ordering .orderby, #dropdown_product_cat, .widget_categories form, .widget_archive select, .single-product .variations_form .variations select, .vcex-form-shortcode select"
            , "responsiveDataBreakpoints": {
                "tl": "1024px"
                , "tp": "959px"
                , "pl": "767px"
                , "pp": "479px"
            }
            , "ajaxurl": ""
            , "loadMore": {
                "text": "Load More"
                , "loadingText": "Cargando/u2026"
                , "failedText": "Failed to load posts."
            }
            , "hasStickyHeader": "1"
            , "stickyHeaderStyle": "standard"
            , "hasStickyMobileHeader": "1"
            , "overlayHeaderStickyTop": "0"
            , "stickyHeaderBreakPoint": "960"
            , "sidrSource": "#sidr-close, #site-navigation"
            , "sidrDisplace": ""
            , "sidrSide": "left"
            , "sidrBodyNoScroll": ""
            , "sidrSpeed": "300"
            , "iLightbox": {
                "auto": false
                , "skin": "total"
                , "path": "horizontal"
                , "infinite": false
                , "maxScale": 1
                , "minScale": 0
                , "width": 1400
                , "height": ""
                , "videoWidth": 1280
                , "videoHeight": 720
                , "controls": {
                    "arrows": true
                    , "thumbnail": true
                    , "fullscreen": true
                    , "mousewheel": false
                    , "slideshow": true
                }
                , "slideshow": {
                    "pauseTime": 3000
                    , "startPaused": true
                }
                , "effects": {
                    "reposition": true
                    , "repositionSpeed": 200
                    , "switchSpeed": 300
                    , "loadedFadeSpeed": 50
                    , "fadeSpeed": 500
                }
                , "show": {
                    "title": true
                    , "speed": 200
                }
                , "hide": {
                    "speed": 200
                }
                , "overlay": {
                    "blur": true
                    , "opacity": "0.85"
                }
                , "social": {
                    "start": true
                    , "show": "mouseenter"
                    , "hide": "mouseleave"
                    , "buttons": false
                }
                , "text": {
                    "close": "Press Esc to close"
                    , "enterFullscreen": "Enter Fullscreen (Shift+Enter)"
                    , "exitFullscreen": "Exit Fullscreen (Shift+Enter)"
                    , "slideShow": "Slideshow"
                    , "next": "Next"
                    , "previous": "Previous"
                }
                , "thumbnails": {
                    "maxWidth": 120
                    , "maxHeight": 80
                }
            }
        };
        /* ]]> */

    </script>
    <script type="text/javascript" src='{{asset('8cocos/dist/themes/Total/assets/js/total.min.js?ver=4.9.1')}}' id='wpex-core-js' defer=""></script>
    <script type="text/javascript" src='{{asset('8cocos/dist/plugins/total-theme-core/inc/wpbakery/assets/js/vcex-front.min.js?ver=1.0')}}' id='vcex-front-js' defer=""></script>
    <script type="text/javascript" src='{{asset('8cocos/dist/js/wp-embed.min.js?ver=5.5.1')}}' id='wp-embed-js' defer=""></script>
    <script type="text/javascript" src='{{asset('8cocos/dist/plugins/js_composer/assets/js/dist/js_composer_front.min.js?ver=6.3.0')}}' id='wpb_composer_front_js-js' defer=""></script>
    <script data-no-minify="1" async="" src="{{asset('8cocos/dist/plugins/wp-rocket/assets/js/lazyload/16.1/lazyload.min.js')}}" type="text/javascript"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0" type="text/javascript"></script>
    <script src="{{asset('8cocos/dist/themes/total-child-theme/cocos-js/script.js')}}" defer="" type="text/javascript"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flickity/2.2.1/flickity.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/flickity/2.2.1/flickity.pkgd.js" type="text/javascript "></script>
    <script src="{{asset('8cocos/dist/plugins/bootstrap/4.3.1/js/bootstrap.min.js')}} " type="text/javascript "></script>
    <script src="{{asset('8cocos/dist/plugins/bootstrap/4.3.1/js/bootstrap.bundle.min.js')}} " type="text/javascript "></script>
    <script type="text/javascript " src="{{asset('8cocos/dist/themes/total-child-theme/cocos-js/ajax.js')}} " defer=" "></script>
    <script src="{{asset('8cocos/dist/plugins/sweetalert2/sweetalert2.min.js')}}" type="text/javascript "></script>
    <script data-no-minify="1 " async=" " src="{{asset('8cocos/dist/plugins/wp-rocket/assets/js/lazyload/16.1/lazyload.min.js')}} " type="text/javascript "></script>
</body>

</html>
