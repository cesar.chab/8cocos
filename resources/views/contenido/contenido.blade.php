@extends('principal')
@section('contenido')
<main id="main" class="site-main clr">
    <div id="content-wrap" class="container clr">
        <div id="primary" class="content-area clr">
            <div id="content" class="site-content clr">
                <article id="single-blocks" class="single-page-article wpex-clr">
                    <div class="single-content single-page-content entry clr">
                        <div id="atf" data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid vc_row-o-full-height vc_row-o-columns-middle vc_row-flex wpex-vc-row-stretched no-bottom-margins">
                            <div class="wpb_column atf-titulo-col vc_column_container vc_col-sm-6">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="wpb_text_column wpb_content_element  titheaderprin">
                                            <div class="wpb_wrapper">
                                                <div id="carouselExampleInterval1" class="carousel slide" data-ride="carousel">
                                                    <div class="carousel-inner">
                                                        <div class="carousel-item active" >
                                                            <h1>El mejor <strong>tipo de cambio</strong><br>para cambiar <strong>dólares</strong> y<br><strong>soles online</strong> en Lima, Perú</h1>
                                                        </div>
                                                        <div class="carousel-item" >
                                                            <h1>Realiza tu cambio de divisas de forma rápida fácil y segura con nosotros.</h1>
                                                        </div>
                                                        <div class="carousel-item">
                                                            <h1>Utiliza nuestros Servicios.</h1>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                            <div class="wpb_column vc_column_container vc_col-sm-5 vc_col-xs-5">
                                                <div class="vc_column-inner">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_text_column wpb_content_element  vc_custom_1586238361281 valores valores--transac">
                                                            <div class="wpb_wrapper">
                                                                <p>Transacciones Exitosas<br><strong>+ 3 mil</strong></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="wpb_column vc_column_container vc_col-sm-5 vc_col-xs-5">
                                                <div class="vc_column-inner">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_text_column wpb_content_element  vc_custom_1586238383033 valores valores--super">
                                                            <div class="wpb_wrapper">
                                                                <p>Registrados en<br><b>SUPERINTENDENCIA<br>DE BANCA, SEGUROS Y AFP</b></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                            <div class="wpb_column vc_column_container vc_col-sm-5 vc_col-xs-5">
                                                <div class="vc_column-inner">
                                                </div>
                                            </div>
                                            <div class="wpb_column vc_column_container vc_col-sm-5 vc_col-xs-5">
                                                <div class="vc_column-inner">
                                                    <div class="wpb_wrapper"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-1">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper"></div>
                                </div>
                            </div>
                            <div class="wpb_column atf-cacl vc_column_container vc_col-sm-5">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="wpb_text_column wpb_content_element  atf-titulo-movil">
                                            <div class="wpb_wrapper">
                                                <p>Cambia Tu<br>Dinero Online</p>
                                            </div>
                                        </div>
                                        <div class="km_calc">
                                            <div class="km_calc-encabezado">
                                                <strong>Tipo de Cambio&nbsp;</strong>&nbsp;del dólar hoy en Perú<br> Compra:&nbsp;
                                                <strong id="valcompra">&nbsp;3.556&nbsp;</strong> Venta: <strong id="valventa">&nbsp;3.59&nbsp;</strong>
                                            </div>
                                            <div class="km_calc-cont">
                                                <div class="p-0 km_calc-cont__field">
                                                    <div class="km_calc-cont__field__dato">
                                                        <div>
                                                            <div>
                                                                <span>Tú Envías</span>
                                                                <input type="text" value="1500" name="valini" id="valini" style="" type="number" autocomplete="off">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="monedas km_calc-cont__field__monedas">
                                                        <div id="inicio" cambio="usd">
                                                            <span>Dólares</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="p-0 km_calc-cont__field">
                                                    <div class="km_calc-cont__field__dato">
                                                        <div>
                                                            <div>
                                                                <span>Tú Recibes</span>
                                                                <input type="text" value="" name="valfin" id="valfin" style="" type="number" autocomplete="off">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="monedas km_calc-cont__field__monedas">
                                                        <img src="8cocos/imagenes/svg/change.svg" class="make-it-slow" id="change">
                                                        <div id="final" cambio="pen">
                                                            <span>Soles</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="subheadertext">
                                                    <div>
                                                        Ahorro Estimado: <span id="montoahorradofinal">S/. 80.28</span>
                                                    </div>
                                                    <div style="display: none;">
                                                        Oferta Válida: <span>3:30 min</span>
                                                    </div>
                                                </div>
                                                <div class="margeboton">
                                                    <div>
                                                        <a href="login.html" class="iopera">Inicia tu operación</a>
                                                    </div>
                                                </div>

                                                <div class="disclaim" style="">
                                                    <div>
                                                        <img id="accionarbotonexeso" data-toggle="tooltip" data-html="true" 
                                                        data-original-title="<b>Precios preferenciales:</b> Puedes contactarnos por un precio preferencial en línea o al correo <a href='mailto:contacto@8cocos.com'>contacto@8cocos.com</a>.<br/><b>Límites:</b> Recuerda confirmar con tu banco, que estás autorizado para realizar una operación del monto deseado No olvides que no aceptamos depósitos en efectivo." 
                                                        src="8cocos/imagenes/svg/masinfo.svg">¿Monto mayor a <b>$10.000</b> o <b>S/ 30.000</b>?
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="wpb_text_column wpb_content_element ">
                                            <div class="wpb_wrapper">
                                            </div>
                                        </div>
                                        <div class="vc_row wpb_row vc_inner vc_row-fluid valores-movil">
                                            <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-xs-5">
                                                <div class="vc_column-inner">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_text_column wpb_content_element  vc_custom_1586272039291 valores valores--transac">
                                                            <div class="wpb_wrapper">
                                                                <p>Transacciones Exitosas<br><strong>+ 3 mil</strong></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-xs-5">
                                                <div class="vc_column-inner">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_text_column wpb_content_element  vc_custom_1586238434651 valores valores--super">
                                                            <div class="wpb_wrapper">
                                                                <p>Registrados en<br><b>SUPERINTENDENCIA<br>DE BANCA, SEGUROS Y AFP</b></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="vc_row-full-width vc_clearfix"></div>
                        <div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid sec-bancos wpex-vc-row-stretched no-bottom-margins">
                            <img src="8cocos/imagenes/cintaBancos/bannerbancos.png">

                        </div>
                        <div class="vc_row-full-width vc_clearfix"></div>
                        <div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-stretch-content="true" class="vc_row wpb_row vc_row-fluid home-banner vc_row-no-padding wpex-vc-row-stretched">
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="wpb_single_image wpb_content_element vc_align_center   banner-img visible-desktop">
                                            <figure class="wpb_wrapper vc_figure">
                                                <div class="vc_single_image-wrapper   vc_box_border_grey"><img src="8cocos/imagenes/banners/bannerl.png" class="vc_single_image-img attachment-full" alt=""></div>
                                            </figure>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="vc_row-full-width vc_clearfix"></div>
                        <div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid home-comofun wpex-vc-row-stretched">
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="wpb_text_column wpb_content_element  home-comofun__titulo">
                                            <div class="wpb_wrapper">
                                                <h2 style="text-align: center;">¿Cómo Funciona?</h2>
                                            </div>
                                        </div>
                                        <div id="ult-carousel-1878740985f5c4edd1e947" class="ult-carousel-wrapper  home-comofun__carr ult_horizontal" data-gutter="15" data-rtl="false">
                                            <div class="ult-carousel-13860173925f5c4edd1e928 ">
                                                <div class="ult-item-wrap" data-animation="animated no-animation">
                                                    <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                                        <div class="ult-item-wrap" data-animation="animated no-animation">
                                                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                                                <div class="vc_column-inner">
                                                                    <div class="wpb_wrapper">
                                                                        <div class="ult-item-wrap" data-animation="animated no-animation">
                                                                            <div class="wpb_single_image wpb_content_element vc_align_center   home-comofun__paso__img">
                                                                                <figure class="wpb_wrapper vc_figure">
                                                                                    <div class="vc_single_image-wrapper   vc_box_border_grey"><img class="vc_single_image-img" src="8cocos/imagenes/tresPasos/cotiza.png"></div>
                                                                                </figure>
                                                                            </div>
                                                                        </div>
                                                                        <div class="ult-item-wrap" data-animation="animated no-animation">
                                                                            <div class="wpb_text_column wpb_content_element  home-comofun__paso">
                                                                                <div class="wpb_wrapper">
                                                                                    <p style="text-align: center;"><strong>Cotiza tu</strong><br><strong>operación</strong></p>
                                                                                    <p style="text-align: center;">Cotiza el monto a cambiar y selecciona en qué cuenta deseas recibirlo.</p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="ult-item-wrap" data-animation="animated no-animation">
                                                    <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                                        <div class="ult-item-wrap" data-animation="animated no-animation">
                                                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                                                <div class="vc_column-inner">
                                                                    <div class="wpb_wrapper">
                                                                        <div class="ult-item-wrap" data-animation="animated no-animation">
                                                                            <div class="wpb_single_image wpb_content_element vc_align_center   home-comofun__paso__img">
                                                                                <figure class="wpb_wrapper vc_figure">
                                                                                    <div class="vc_single_image-wrapper   vc_box_border_grey"><img class="vc_single_image-img" src="8cocos/imagenes/tresPasos/transfiere.png"></div>
                                                                                </figure>
                                                                            </div>
                                                                        </div>
                                                                        <div class="ult-item-wrap" data-animation="animated no-animation">
                                                                            <div class="wpb_text_column wpb_content_element  home-comofun__paso">
                                                                                <div class="wpb_wrapper">
                                                                                    <p style="text-align: center;"><strong>Transfiere a</strong><br><strong>8cocos</strong></p>
                                                                                    <p style="text-align: center;">Transfiere el monto desde tu banca online a la cuenta 8cocos indicada y conserva el comprobante.</p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="ult-item-wrap" data-animation="animated no-animation">
                                                    <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                                        <div class="ult-item-wrap" data-animation="animated no-animation">
                                                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                                                <div class="vc_column-inner">
                                                                    <div class="wpb_wrapper">
                                                                        <div class="ult-item-wrap" data-animation="animated no-animation">
                                                                            <div class="wpb_single_image wpb_content_element vc_align_center   home-comofun__paso__img">
                                                                                <figure class="wpb_wrapper vc_figure">
                                                                                    <div class="vc_single_image-wrapper   vc_box_border_grey"><img class="vc_single_image-img" src="8cocos/imagenes/tresPasos/recibe.png"></div>
                                                                                </figure>
                                                                            </div>
                                                                        </div>
                                                                        <div class="ult-item-wrap" data-animation="animated no-animation">
                                                                            <div class="wpb_text_column wpb_content_element  home-comofun__paso">
                                                                                <div class="wpb_wrapper">
                                                                                    <p style="text-align: center;"><strong>Recibe Tu</strong><br><strong>Cambio</strong></p>
                                                                                    <p style="text-align: center;">Verifica tu operación ingresando el número del comprobante y recibe el dinero en tu cuenta.</p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <script type="text/javascript">
                                            jQuery(document).ready(function($) {
                                                if (typeof jQuery('.ult-carousel-13860173925f5c4edd1e928').slick == "function") {
                                                    $('.ult-carousel-13860173925f5c4edd1e928').slick({
                                                        dots: true
                                                        , autoplay: true
                                                        , autoplaySpeed: 5000
                                                        , speed: 300
                                                        , infinite: false
                                                        , arrows: true
                                                        , nextArrow: '<button type="button" role="button" aria-label="Next" style="color:#333333; font-size:20px;" class="slick-next default"><i class="ultsl-arrow-right4"></i></button>'
                                                        , prevArrow: '<button type="button" role="button" aria-label="Previous" style="color:#333333; font-size:20px;" class="slick-prev default"><i class="ultsl-arrow-left4"></i></button>'
                                                        , slidesToScroll: 3
                                                        , slidesToShow: 3
                                                        , swipe: true
                                                        , draggable: true
                                                        , touchMove: true
                                                        , pauseOnHover: true
                                                        , pauseOnFocus: false
                                                        , responsive: [{
                                                            breakpoint: 1026
                                                            , settings: {
                                                                slidesToShow: 3
                                                                , slidesToScroll: 3
                                                            , }
                                                        }, {
                                                            breakpoint: 1025
                                                            , settings: {
                                                                slidesToShow: 3
                                                                , slidesToScroll: 3
                                                            }
                                                        }, {
                                                            breakpoint: 760
                                                            , settings: {
                                                                slidesToShow: 1
                                                                , slidesToScroll: 1
                                                            }
                                                        }]
                                                        , pauseOnDotsHover: true
                                                        , customPaging: function(slider, i) {
                                                            return '<i type="button" style= "color:#333333;" class="ultsl-record" data-role="none"></i>';
                                                        }
                                                    , });
                                                }
                                            });

                                        </script>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="vc_row-full-width vc_clearfix"></div>
                        <div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-stretch-content="true" class="vc_row wpb_row vc_row-fluid vc_row-no-padding wpex-vc-row-stretched">
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="wpb_text_column wpb_content_element ">
                                            <div class="wpb_wrapper">
                                                <div class="por-que">
                                                    <div class="container">
                                                        <h2>Números que inspiran confianza al<br>momento de cambiar divisas online</h2>
                                                        <div class="subtextoporque">Somos la primera casa de cambio peruana inscrita en la superintendencia de banca y seguros que realiza cambio de divisas online a precios justos, con plena seguridad y desde la comodidad
                                                            de tu hogar u oficina. Estamos convencidos que se pueden hacer las cosas muchísimo más sencillas y, para ello, usamos la tecnología como nuestro principal aliado.</div>
                                                        <div class="row sectpor">
                                                            <div class="col-md-8 col-12 diagramamundo ">
                                                                <div>
                                                                    <img class="" src="8cocos/imagenes/mapa.png" style="width:90%;height:500px">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 col-12 align-self-center">
                                                                <div class="porquemedi align-self-center">
                                                                    <div class="row">
                                                                        <div class="col-md-12 col-9 "><span class="cant">+ 96 mil</span><br>Usuarios Registrados</div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-12 col-9"><span class="cant">+ 404 mil</span><br>Operaciones Realizadas</div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-12 col-9"><span class="cant">+ $626 millones</span><br>Cambiados</div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-12 col-9"><span class="cant">+ $8 millones</span><br>Ahorrados</div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12 col-12 configsboton"><a class="configboton" href="login.html">Inicia tu operación</a></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="vc_row-full-width vc_clearfix"></div>
                        <div class="vc_row wpb_row vc_row-fluid vc_row-no-padding wpex-vc-row-stretched">
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="wpb_text_column wpb_content_element ">
                                            <div class="wpb_wrapper">
                                                <div class="confianza">
                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="col-md-4 col-12 align-self-center order-md-1">
                                                                <h3>Comunidad</h3>
                                                                <p><span class="comentariofn">Me encanta!! Lo uso todo el tiempo sin problemas y me ahorra ir al cambista y andar con dinero en la calle, y su tasa de cambio es muy buena también! Excelente servicios, muchas gracias!!</span><br>
                                                                    <i class="fa fa-facebook-square"></i><span class="nombre">Paola Bellatin</span></p>
                                                            </div>
                                                            <div class="col-md-8 col-12 altco order-md-2 align-self-center">
                                                                <div>
                                                                    <img class="" src="8cocos/imagenes/comunidad_final.png" style="margin-top: 20%;">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="vc_row-full-width vc_clearfix"></div>
                        </div>
                </article>
            </div>
        </div>
    </div>
</main>
@endsection