@extends('principal')
@section('contenido')
<main id="main" class="site-main clr">
    <div id="content-wrap" class="container clr">
        <div id="primary" class="content-area clr">
            <div id="content" class="site-content clr">
                <article id="single-blocks" class="single-page-article wpex-clr">
                    <div class="single-content single-page-content entry clr">
                        <div class="vc_row wpb_row vc_row-fluid no-bottom-margins">
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="wpb_text_column wpb_content_element  nos-titulo">
                                            <div class="wpb_wrapper">
                                                <h1><strong> Nacimos con el sueño de ofrecer la mejor experiencia de <br>cambiar dinero en el Perù.</h1></strong>
                                                <div class="wpb_wrapper_history nos-historia-titulo">
                                                    <h2 style="text-align: center;" class="">Nosotros</h2>
                                                </div>
                                                <p>En plena época de cambios tecnológicos decidimos transformar la forma de comprar y vender dólares. Somos una casa de cambio digital que tiene como objetivo ofrecer una forma rápida y segura
                                                    de cambiar divisas donde quiera que estés. Sabemos que tu tiempo y tu seguridad no tienen precio, por eso contamos con un equipo especializado en tecnología que trabaja para darte el
                                                    mejor servicio.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="vc_row wpb_row vc_row-fluid nos-historia">
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="wpb_text_column wpb_content_element ">
                                            <div class="wpb_wrapper">
                                                <div class="nosotrosimagen">
                                                    <img class="img-fluid" src="8cocos/imagenes/nosotros/info7.png">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="vc_row wpb_row vc_row-fluid">
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="wpb_text_column wpb_content_element  nos-clientes-titulo">
                                            <div class="wpb_wrapper">
                                                <h2 style="text-align: center;">Nuestro Servicio</h2>
                                            </div>
                                        </div><br><br>
                                        <div class="wpb_column vc_column_container vc_col-sm-6">
                                            <div class="vc_column-inner ">
                                                <div class="wpb_wrapper">
                                                    <div class="wpb_text_column wpb_content_element  nos-construye-bloque">
                                                        <div class="wpb_wrapper"><br><br>
                                                            <p><span class="comentariofn" style="font-size: 20px">¡Dile adiòs al efectivo!
                                                                    Compra y vende dòlares
                                                                    donde te encuentres,<b> te
                                                                        transferimos en tus
                                                                        cuentas bancarias o a
                                                                        terceros.</b></span><br>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="wpb_column vc_column_container vc_col-sm-6">
                                            <div class="vc_column-inner ">
                                                <div class="wpb_wrapper">
                                                    <div class="wpb_text_column wpb_content_element  nos-construye-bloque">
                                                        <div class="wpb_wrapper">
                                                            <img src="8cocos/imagenes/nosotros/imagen2.png" class="img-fluid">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="vc_row wpb_row vc_row-fluid nos-construye-row wpex-vc-row-stretched">
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="wpb_text_column wpb_content_element  nos-construye-bloque">
                                            <div class="wpb_wrapper">
                                                <p style="text-align: center;"><a href="tipoperfil.html">Regístrate ahora</a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </div>
    </div>
</main>
@stop
