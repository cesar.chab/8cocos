@extends('principal')
@section('contenido')
<main id="main" class="site-main clr">
    <div id="content-wrap" class="container clr">
        <div id="primary" class="content-area clr">
            <div id="content" class="site-content clr">
                <article id="single-blocks" class="single-page-article wpex-clr">
                    <div class="single-content single-page-content entry clr">
                        <div class="vc_row wpb_row vc_row-fluid">
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="wpb_text_column wpb_content_element ">
                                            <div class="wpb_wrapper">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="vc_row wpb_row vc_row-fluid">
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="wpb_text_column wpb_content_element  title---page-blog">
                                            <div class="wpb_wrapper">
                                                <h1>El blog de 8COCOS <br> <br> ¿Cómo emprender? Te damos 5 pasos para arrancar tu propio negocio </h1>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="vc_row wpb_row vc_row-fluid vc_custom_1584997582398 no-bottom-margins">
                            <div class="wpb_column vc_column_container vc_col-sm-8">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <a href="..\blog_5pasos.html" title="Como emprender" class="wpex-slider-media-link">
                                            <img width="755" height="410" src="8cocos/imagenes/mini.png" class="skip-lazy" alt="" data-no-lazy="1"></a>

                                    </div>
                                </div>
                            </div>
                            <div class="wpb_column wid-calc vc_column_container vc_col-sm-4">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="vc_wp_search wpb_content_element lw__hidden-blog">
                                        </div>
                                        <div class="km_calc">
                                            <div class="km_calc-encabezado">
                                                <strong>Tipo de Cambio&nbsp;</strong>&nbsp;del dólar hoy en Perú<br> Compra:&nbsp;
                                                <strong id="valcompra">&nbsp;3.556&nbsp;</strong> Venta: <strong id="valventa">&nbsp;3.59&nbsp;</strong>
                                            </div>
                                            <div class="km_calc-cont">
                                                <div class="p-0 km_calc-cont__field">
                                                    <div class="km_calc-cont__field__dato">
                                                        <div>
                                                            <div>
                                                                <span>Tú Envías</span>
                                                                <input type="text" value="1500" name="valini" id="valini" style="" type="number" autocomplete="off">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="monedas km_calc-cont__field__monedas">
                                                        <div id="inicio" cambio="usd">
                                                            <span>Dólares</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="p-0 km_calc-cont__field">
                                                    <div class="km_calc-cont__field__dato">
                                                        <div>
                                                            <div>
                                                                <span>Tú Recibes</span>
                                                                <input type="text" value="" name="valfin" id="valfin" style="" type="number" autocomplete="off">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="monedas km_calc-cont__field__monedas">
                                                        <img src="8cocos/imagenes/svg/change.svg" class="make-it-slow" id="change" data-lazy-src="8cocos/imagenes/svg/change.svg">
                                                        <div id="final" cambio="pen">
                                                            <span>Soles</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="subheadertext">
                                                    <div>
                                                        <!-- Ahorro Estimado: <span id="montoahorradofinal">S/. 39</span> -->
                                                    </div>
                                                    <div style="display: none;">
                                                        Oferta Válida: <span>3:30 min</span>
                                                    </div>
                                                </div>
                                                <div class="margeboton">
                                                    <div>
                                                        <a href="/" class="iopera">Inicia tu operación</a>
                                                    </div>
                                                </div>
                                                <div class="disclaim" style="">

                                                </div>
                                            </div>
                                        </div>
                                        <div class="wpb_text_column wpb_content_element  image---calculador">
                                            <div class="wpb_wrapper">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="vc_row wpb_row vc_row-fluid">
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="vc_separator wpb_content_element vc_separator_align_center vc_sep_width_100 vc_sep_pos_align_center vc_sep_color_grey title---section vc_separator-has-text"><span class="vc_sep_holder vc_sep_holder_l"><span class="vc_sep_line"></span></span>
                                            <h4>Más Recientes</h4><span class="vc_sep_holder vc_sep_holder_r"><span class="vc_sep_line"></span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="vc_row wpb_row vc_row-fluid">
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="vc_grid-container-wrapper vc_clearfix">
                                            <div class="vc_grid-container vc_clearfix wpb_content_element vc_basic_grid grid--blog-lw" data-initial-loading-animation="fadeIn">
                                                <div class="vc_grid vc_row vc_pageable-wrapper vc_hook_hover" data-vc-pageable-content="true">
                                                    <div class="vc_pageable-slide-wrapper vc_clearfix" data-vc-grid-content="true">
                                                        <div class="vc_clearfix vc_col-sm-4 vc_grid-item-zone-c-top">
                                                            <div class="vc_grid-item-mini vc_clearfix ">
                                                                <div class="vc_gitem-zone vc_gitem-zone-c">
                                                                    <div class="vc_gitem-zone-mini"></div>
                                                                </div>
                                                                <div class="vc_gitem-animated-block ">
                                                                    <div class="vc_gitem-zone vc_gitem-zone-a grid-post-temp vc_gitem-is-link">
                                                                        <a href="emprendeconalgoqueteapasiona.html" title="¿Qué es 8COCOS y cómo funciona?" class="vc_gitem-link vc-zone-link"></a>
                                                                        <div class="vc_gitem-zone-mini">
                                                                            <div class="vc_gitem_row vc_row vc_gitem-row-position-top">
                                                                                <div class="vc_col-sm-12 grid-post-temp-content vc_gitem-col vc_gitem-col-align-">
                                                                                    <div class="wpb_single_image wpb_content_element vc_align_center">
                                                                                        <figure class="wpb_wrapper vc_figure">
                                                                                            <a href="..\noticias\que-es-8COCOS-y-como-funciona\index.htm" class="vc_gitem-link vc_single_image-wrapper vc_box_border_grey" title="¿Qué es 8COCOS y cómo funciona?">
                                                                                                <img width="755" height="410" src="8cocos/imagenes/blog/prueba1.jpg" class="vc_single_image-img attachment-full" alt=""></a>
                                                                                        </figure>
                                                                                    </div>
                                                                                    <div class="vc_gitem-post-data grid-categorias vc_gitem-post-data-source-post_categories vc_grid-filter vc_clearfix vc_grid-filter-  vc_grid-filter-size-sm vc_grid-filter-center vc_grid-filter-color-grey">
                                                                                        <div class="vc_grid-filter-item vc_gitem-post-category-name"><span class="vc_gitem-post-category-name">Noticias</span></div>
                                                                                    </div>
                                                                                    <div class="vc_gitem-post-meta-field-tiempo_lectura grid-tiempo vc_gitem-align-left"></div>
                                                                                    <div class="vc_custom_heading vc_gitem-post-data vc_gitem-post-data-source-post_title">
                                                                                        <h2 style="text-align: left"><a href="..\noticias\que-es-8COCOS-y-como-funciona\index.htm" class="vc_gitem-link" title="¿Qué es 8COCOS y cómo funciona?">Emprende con algo que te apasiona</a></h2>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="vc_clearfix"></div>
                                                        </div>
                                                        <div class="vc_clearfix vc_col-sm-4 vc_grid-item-zone-c-top">
                                                            <div class="vc_grid-item-mini vc_clearfix ">
                                                                <div class="vc_gitem-zone vc_gitem-zone-c">
                                                                    <div class="vc_gitem-zone-mini"></div>
                                                                </div>
                                                                <div class="vc_gitem-animated-block ">
                                                                    <div class="vc_gitem-zone vc_gitem-zone-a grid-post-temp vc_gitem-is-link">
                                                                        <a href="siempreinvestiga.html" title="" class="vc_gitem-link vc-zone-link"></a>
                                                                        <div class="vc_gitem-zone-mini">
                                                                            <div class="vc_gitem_row vc_row vc_gitem-row-position-top">
                                                                                <div class="vc_col-sm-12 grid-post-temp-content vc_gitem-col vc_gitem-col-align-">
                                                                                    <div class="wpb_single_image wpb_content_element vc_align_center">
                                                                                        <figure class="wpb_wrapper vc_figure">
                                                                                            <a href="..\emprendimiento\como-crear-una-startup-desde-cero\index.htm" class="vc_gitem-link vc_single_image-wrapper vc_box_border_grey" title="¿Cómo crear una startup desde cero?">
                                                                                                <img width="755" height="410" src="8cocos/imagenes/blog/prueba2.jpg" class="vc_single_image-img attachment-full" alt="crear una startup"></a>
                                                                                        </figure>
                                                                                    </div>
                                                                                    <div class="vc_gitem-post-data grid-categorias vc_gitem-post-data-source-post_categories vc_grid-filter vc_clearfix vc_grid-filter-  vc_grid-filter-size-sm vc_grid-filter-center vc_grid-filter-color-grey">
                                                                                        <div class="vc_grid-filter-item vc_gitem-post-category-name"><span class="vc_gitem-post-category-name">Emprendimiento</span></div>
                                                                                    </div>
                                                                                    <div class="vc_gitem-post-meta-field-tiempo_lectura grid-tiempo vc_gitem-align-left"></div>
                                                                                    <div class="vc_custom_heading vc_gitem-post-data vc_gitem-post-data-source-post_title">
                                                                                        <h2 style="text-align: left"><a href="..\emprendimiento\como-crear-una-startup-desde-cero\index.htm" class="vc_gitem-link" title="¿Cómo crear una startup desde cero?">Siempre investiga</a></h2>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="vc_clearfix"></div>
                                                        </div>
                                                        <div class="vc_clearfix vc_col-sm-4 vc_grid-item-zone-c-top">
                                                            <div class="vc_grid-item-mini vc_clearfix ">
                                                                <div class="vc_gitem-zone vc_gitem-zone-c">
                                                                    <div class="vc_gitem-zone-mini"></div>
                                                                </div>
                                                            </div>
                                                            <div class="vc_clearfix"></div>
                                                        </div>
                                                        <div class="vc_clearfix vc_col-sm-4 vc_grid-item-zone-c-top">
                                                            <div class="vc_grid-item-mini vc_clearfix ">
                                                                <div class="vc_gitem-zone vc_gitem-zone-c">
                                                                    <div class="vc_gitem-zone-mini"></div>
                                                                </div>

                                                            </div>
                                                            <div class="vc_clearfix"></div>
                                                        </div>
                                                        <div class="vc_clearfix vc_col-sm-4 vc_grid-item-zone-c-top">
                                                            <div class="vc_grid-item-mini vc_clearfix ">
                                                                <div class="vc_gitem-zone vc_gitem-zone-c">
                                                                    <div class="vc_gitem-zone-mini"></div>
                                                                </div>
                                                                <div class="vc_gitem-animated-block ">
                                                                    <div class="vc_gitem-zone vc_gitem-zone-a grid-post-temp vc_gitem-is-link">
                                                                        <a href="salalmercado.html" title="7 hábitos financieros que te permitirán salir adelante en tiempos de crisis" class="vc_gitem-link vc-zone-link"></a>
                                                                        <div class="vc_gitem-zone-mini">

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="vc_clearfix"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </div>
    </div>
</main>
@stop
