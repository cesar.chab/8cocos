@extends('principal')
@section('contenido')
<div class="main_ayuda">
    <div class="container">
        <div class="section_manuales">
            <h2 style="text-align: center;">Manuales</h2>

            <div class="accordion-item">
                <div class="accordion-item-header">
                    ¿Cómo crear una cuenta?
                </div>
                <div class="accordion-item-body">
                    <div class="accordion-item-body-content">
                        <p>
                            <a target="_blank" rel="noopener noreferrer">Ver manual (pdf)</a>
                        </p>
                    </div>
                </div>
            </div>

            <div class="accordion-item">
                <div class="accordion-item-header">
                    ¿Cómo hacer una operación?
                </div>
                <div class="accordion-item-body">
                    <div class="accordion-item-body-content">
                        <p>
                            <a target="_blank" rel="noopener noreferrer">Ver manual (pdf)</a>
                        </p>
                    </div>
                </div>
            </div>

            <div class="accordion-item">
                <div class="accordion-item-header">
                    ¿Cómo validar mi identidad?
                </div>
                <div class="accordion-item-body">
                    <div class="accordion-item-body-content">
                        <p>
                            <a target="_blank" rel="noopener noreferrer">Ver manual (pdf)</a>
                        </p>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="container">
        <div class="section_manuales">
            <h2 style="text-align: center;">Información básica</h2>

            <div class="accordion-item">
                <div class="accordion-item-header">
                    4 cosas que debes saber antes de transferir
                </div>
                <div class="accordion-item-body">
                    <div class="accordion-item-body-content">
                        <p><strong>No aceptamos efectivo o cheques. </strong><br> Las transferencias serán solo de manera interbancaria y vía online. Por eso, no podemos recibir o enviar dinero en efectivo o en cheques. Solo podemos enviar dinero
                            de una cuenta bancaria a otra cuenta.</p><br>

                        <p><strong>Detalles correctos en tu perfil </strong><br> Cuando introduzcas los datos, por favor, utiliza tu nombre completo (no diminutivos o iniciales), tu domicilio real, tu número telefónico real y una dirección de
                            correo que revises con frecuencia.
                        </p><br>

                        <p><strong>Verificación de tu cuenta </strong><br> Como empresa, necesitamos saber quién realiza las transferencias. En cierto punto, tarde o temprano, tu identidad tendrá que ser verificada por 8Cocos.
                        </p><br>

                        <p><strong>Enviar tu dinero </strong><br> Nuestra plataforma digital tiene todos los certificados de seguridad que son necesarios para que puedas enviar tu dinero con tranquilidad.
                        </p><br>

                    </div>

                </div>
            </div>

            <div class="accordion-item">
                <div class="accordion-item-header">
                    ¿Cómo inicio una transferencia para cambiar dinero online?
                </div>
                <div class="accordion-item-body">
                    <div class="accordion-item-body-content">
                        <p>Puedes ver un tutorial detallado en:&nbsp;
                            <a href="" rel="noreferrer noopener">Video</a><br> Asimismo, puedes contactarnos a&nbsp;<a href="mailto:contacto@8cocos.com">contacto@8cocos.com</a>, por inbox de&nbsp;
                            <a href="https://www.facebook.com/8cocos" target="_blank" rel="noreferrer noopener">facebook</a>&nbsp;o por el livechat de 8cocos.</p>
                    </div>
                </div>
            </div>

            <div class="accordion-item">
                <div class="accordion-item-header">
                    ¿Cuánto cuesta usar 8Cocos?
                </div>
                <div class="accordion-item-body">
                    <div class="accordion-item-body-content">
                        <p>8Cocos no cobra ninguna comisión por el servicio. Solo utiliza el tipo de cambio contemplado en los mercados financieros al momento de realizar la orden de compra. Puedes consultar el tipo de cambio, eligiendo el monto
                            que deseas enviar o recibir para que se calcule el resultado automáticamente. Además, te indicamos cuánto te ahorras aproximadamente en comparación a la tasa promedio de los bancos.</p>
                    </div>
                </div>
            </div>

            <div class="accordion-item">
                <div class="accordion-item-header">
                    Cancelar una transferencia
                </div>
                <div class="accordion-item-body">
                    <div class="accordion-item-body-content">
                        <p>Si por algún motivo deseas cancelar la transferencia que ya realizaste, por favor, escríbenos un correo lo antes posible a <a href="mailto:contacto@8cocos.com">contacto@8cocos.com</a> para coordinar la cancelación y
                            verificar el monto que debemos reembolsarte.</p>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="container">
        <div class="section_manuales">
            <h2 style="text-align: center;">Seguridad</h2>

            <div class="accordion-item">
                <div class="accordion-item-header">
                    Seguridad
                </div>
                <div class="accordion-item-body">
                    <div class="accordion-item-body-content">
                        <p>
                            Nuestras operaciones están enmarcadas dentro del marco regulatorio pertinente establecido por la Superintendencia de Banca y Seguros (SBS). Nuestra licencia es y fue otorgada en la Resolución.</p><br>
                    </div>
                </div>
            </div>

            <div class="accordion-item">
                <div class="accordion-item-header">
                    ¿Qué tan seguro es es usar 8Cocos para cambiar dinero online?
                </div>
                <div class="accordion-item-body">
                    <div class="accordion-item-body-content">
                        <p>La seguridad de tus transacciones está garantizada por el banco en el cual posees una cuenta. Asimismo, somos una empresa constituida formalmente y con la regulación respectiva de la SBS.</p>
                    </div>
                </div>
            </div>

            <div class="accordion-item">
                <div class="accordion-item-header">
                    ¿Cuál es mi privacidad?
                </div>
                <div class="accordion-item-body">
                    <div class="accordion-item-body-content">
                        <p>Tu información otorgada a 8cocos estará completamente segura en el marco de las Políticas de Privacidad de la empresa. Puedes leer más aquí.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="section_manuales">
            <h2 style="text-align: center;">Validación de Identidad</h2>

            <div class="accordion-item">
                <div class="accordion-item-header">
                    Qué necesito para validar mi identidad
                </div>
                <div class="accordion-item-body">
                    <div class="accordion-item-body-content">
                        <p>
                            Te solicitaremos datos básicos, una fotografía por delante y reverso de algún documento de identificación (DNI, Pasaporte o carné de extranjería) y una foto frontal de tu rostro.</p><br>
                    </div>
                </div>
            </div>

            <div class="accordion-item">
                <div class="accordion-item-header">
                    ¿Qué hago si mi validación fue rechazada?
                </div>
                <div class="accordion-item-body">
                    <div class="accordion-item-body-content">
                        <p>Si tu validación de identidad fue rechazada y/o para evitar futuros errores en el proceso, te recomendamos identificar la razón y seguir estos consejos</p><br>

                        <p> <b>1. Documento no legible/ Falta una parte</b><br> – Verifica que se visualice la totalidad del documento en la fotografía.<br> – Ninguna parte debe estar cubierta.
                        </p><br>

                        <p> <b>2. Documento no legible/ Borroso</b><br> – Verifica que la fotografía de tu documento tenga buena resolución.<br> – Todos los datos deben ser totalmente legibles.
                        </p><br>

                        <p> <b>3. Documento no legible/ Documento dañado</b><br> – Verifica que tu documento no esté roto, dañado o tenga agujeros.<br>
                        </p><br>

                        <p> <b>4. Selfie no coincide con documento</b><br> – Verifica que el documento adjunto sea propio.<br> – El documento debe tener una foto actualizada de tu rostro.
                        </p><br>

                        <p>Para volver a habilitar tu validación de identidad contáctanos a contacto@8cocos.com o a través de nuestro chat de ayuda.</p>

                    </div>
                </div>
            </div>

            <div class="accordion-item">
                <div class="accordion-item-header">
                    ¿Porqué es necesario validar mi identidad?
                </div>
                <div class="accordion-item-body">
                    <div class="accordion-item-body-content">
                        <p>La validación de identidad nos permite garantizar la seguridad de nuestros usuarios y su información personal previniendo fraudes y/o suplantaciones de identidad. Esto además, nos asegura el cumplimiento de lo dispuesto
                            en el Decreto de Urgencia No. 007-2020 que aprueba el Marco de Confianza Digital y establece medidas para su fortalecimiento, señalando que las empresas que presten servicios digitales se encuentran obligadas a
                            implementar mecanismos para verificar la identidad de las personas que acceden a sus plataformas.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="section_manuales">
            <h2 style="text-align: center;">Cuenta</h2>

            <div class="accordion-item">
                <div class="accordion-item-header">
                    ¿Cómo edito los datos de mi perfil?
                </div>
                <div class="accordion-item-body">
                    <div class="accordion-item-body-content">
                        <p>
                            Una vez que ya realizaste la primera transferencia, si hay algún error en los datos o si simplemente has cambiado de domicilio, el equipo de soporte al cliente de 8cocos puede ayudarte a corregir esta información. En este caso, necesitarás enviarnos un
                            correo con algún tipo de prueba para demostrar que estos datos son correctos a contacto@8cocos.com</p><br>
                    </div>
                </div>
            </div>

            <div class="accordion-item">
                <div class="accordion-item-header">
                    ¿Cómo cambio mi correo electrónico?
                </div>
                <div class="accordion-item-body">
                    <div class="accordion-item-body-content">
                        <p>Tu dirección de correo electrónico sólo podrá ser cambiada comunicándote con nosotros vía correo electrónico. Solo debes escribirnos a contacto@8cocos.com</p><br>
                    </div>
                </div>
            </div>

            <div class="accordion-item">
                <div class="accordion-item-header">
                    ¿Cómo cambio los datos del destinatario?
                </div>
                <div class="accordion-item-body">
                    <div class="accordion-item-body-content">
                        <p>Si ya has creado una orden de compra y hay datos del destinatario que no son correctos, por favor ponte en contacto con nosotros lo antes posible para que podamos ayudarte. Puedes escribirnos un mail a operaciones@8cocos.com
                            con tus datos telefónicos y te estaremos llamando a la brevedad para solucionar el problema.
                        </p>
                    </div>
                </div>
            </div>

            <div class="accordion-item">
                <div class="accordion-item-header">
                    ¿Olvidaste tu contraseña?
                </div>
                <div class="accordion-item-body">
                    <div class="accordion-item-body-content">
                        <p>Puedes crear una nueva contraseña dándole click a “olvidé mi contraseña“. Recibirás un correo electrónico con las instrucciones a seguir para restablecer tu contraseña.
                        </p>
                    </div>
                </div>
            </div>

            <div class="accordion-item">
                <div class="accordion-item-header">
                    ¿Qué es mi historial de pagos?
                </div>
                <div class="accordion-item-body">
                    <div class="accordion-item-body-content">
                        <p>Todas tus transacciones quedan registradas y ligadas a tu cuenta. Puedes verificar el avance de cada transacción que has colocado. Asimismo, podrás seleccionar transacciones frecuentes para poder realizarlas con más
                            rapidez.
                        </p>
                    </div>
                </div>
            </div>

            <div class="accordion-item">
                <div class="accordion-item-header">
                    ¿Cuál es el tiempo estimado de transferencia?
                </div>
                <div class="accordion-item-body">
                    <div class="accordion-item-body-content">
                        <p>El tiempo estimado para hacer efectiva la transferencia es de unos minutos luego de verificar que el cliente realizó la transferencia de los fondos. Adicionalmente, el tiempo que demore en llegar el dinero a nuestras
                            cuentas se encuentra sujeto a los tiempos de respuesta de los sistemas bancarios.
                        </p>
                    </div>
                </div>
            </div>

            <div class="accordion-item">
                <div class="accordion-item-header">
                    ¿Cómo cancelo el pago?
                </div>
                <div class="accordion-item-body">
                    <div class="accordion-item-body-content">
                        <p>El pago se realizará por transferencias por internet desde una cuenta bancaria o depósitos en oficina.Bajo ninguna circunstancia 8Cocos realizará operaciones con efectivo.
                        </p>
                    </div>
                </div>
            </div>

            <div class="accordion-item">
                <div class="accordion-item-header">
                    ¿Cuál es la prueba de que se realizó el pago?
                </div>
                <div class="accordion-item-body">
                    <div class="accordion-item-body-content">
                        <p>La constancia de tu operación puede ser el Número de operación del comprobante de pago si es de un mismo banco. Si es interbancaria, la constancia será el mismo voucher que puede ser la constancia física (papel recibido
                            después de hacer el depósito) o virtual (enviada por mail al momento de hacer la transferencia).
                        </p>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="container">
        <div class="section_manuales">
            <h2 style="text-align: center;">Pagar con Transferencia</h2>

            <div class="accordion-item">
                <div class="accordion-item-header">
                    Sobre las tarifas interbancarias
                </div>
                <div class="accordion-item-body">
                    <div class="accordion-item-body-content">
                        <p>
                            8cocos no se responsabiliza de los costos interbancarios por las transferencias realizadas. Estos se detallarán automáticamente al momento de realizar la transacción y deberán ser asumidos por el usuario.</p><br>
                    </div>
                </div>
            </div>

            <div class="accordion-item">
                <div class="accordion-item-header">
                    Sobre el tiempo estimado de los bancos
                </div>
                <div class="accordion-item-body">
                    <div class="accordion-item-body-content">
                        <p>El tiempo estimado que demora la transferencia está sujeto al tiempo que demoren en llegar los fondos transferidos a las cuentas de 8cocos. Nosotros no tenemos ningún control sobre el tiempo que pueda llegar a demorar
                            la transferencia de los fondos. 8cocos no realizará ninguna transacción sin antes haber recibido los fondos por parte del cliente. Por lo general, el tiempo total para completar la transacción no debería ser mayor
                            a dos días hábiles.</p>
                    </div>
                </div>
            </div>

            <div class="accordion-item">
                <div class="accordion-item-header">
                    Sobre la validación del depósito
                </div>
                <div class="accordion-item-body">
                    <div class="accordion-item-body-content">
                        <p>La validación del depósito se dará cuando recibamos en nuestras cuentas el dinero y lo validemos con el voucher de la operación. Una vez confirmada la operación en nuestras cuentas recibirás una confirmación vía mail
                            y en nuestro portal web. El proceso de validación demora como máximo un día hábil.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="section_manuales">
            <h2 style="text-align: center;">Tipo de Cambio</h2>

            <div class="accordion-item">
                <div class="accordion-item-header">
                    ¿Qué es el tipo de cambio garantizado?
                </div>
                <div class="accordion-item-body">
                    <div class="accordion-item-body-content">
                        <p>
                            El tipo de cambio cerrado al momento de pactar la operación define el monto que el cliente recibe después de validar la operación. Este tipo de cambio está garantizado por un tiempo de 30 minutos luego de colocada la orden de compra, tiempo durante el
                            cual el usuario deberá realizar la transferencia a las cuentas de 8cocos y enviar el comprobante.</p><br>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="container">
        <div class="section_manuales">
            <h2 style="text-align: center;">Verficación</h2>

            <div class="accordion-item">
                <div class="accordion-item-header">
                    Verificación de tu identidad persona o negocio
                </div>
                <div class="accordion-item-body">
                    <div class="accordion-item-body-content">
                        <p>
                            Para asegurar la seguridad de las transacciones realizadas en la plataforma, se realizará una validación de los datos ingresados al crear tu cuenta previo a que puedas realizar operaciones en 8cocos.com. Para esta validación, podríamos requerir información
                            adicional que será solicitada en el momento oportuno.</p><br>
                    </div>
                </div>
            </div>

            <div class="accordion-item">
                <div class="accordion-item-header">
                    ¿Cómo realiza 8cocos el pago?
                </div>
                <div class="accordion-item-body">
                    <div class="accordion-item-body-content">
                        <p>El pago se realizará a través de transferencias por internet desde una cuenta bancaria o transferencias en agencia bancaria. Bajo ninguna circunstancia 8cocos realizará operaciones con efectivo.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="section_manuales">
            <h2 style="text-align: center;">Técnicas</h2>

            <div class="accordion-item">
                <div class="accordion-item-header">
                    ¿Como subir mi comprobante?
                </div>
                <div class="accordion-item-body">
                    <div class="accordion-item-body-content">
                        <p>
                            Al realizar tu orden de compra, se te solicitará enviar el comprobante de la transacción de fondos realizada. Para hacer esto, adjunta el screenshot del voucher de la transacción en la ventana de confirmación de la orden de compra o también puedes enviarnos
                            el comprobante de pago a contacto@8cocos.com y en el Asunto colocar el número de la operación.</p><br>
                    </div>
                </div>
            </div>

            <div class="accordion-item">
                <div class="accordion-item-header">
                    ¿Problemas con el navegador?
                </div>
                <div class="accordion-item-body">
                    <div class="accordion-item-body-content">
                        <p>Para garantizar el óptimo funcionamiento de 8cocos.com, se recomienda contar la última versión del navegador de tu preferencia. Si aún así tienes problemas de navegación, te recomendamos eliminar la caché y las cookies
                            del navegador. De persistir los problemas, te sugerimos contactarte con nosotros para evaluar la situación.</p>
                    </div>
                </div>
            </div>

            <div class="accordion-item">
                <div class="accordion-item-header">
                    ¿Como eliminar tu cache?
                </div>
                <div class="accordion-item-body">
                    <div class="accordion-item-body-content">
                        <p> <b>En Chrome</b><br> 1. En la barra de herramientas de tu navegador, haz clic en Más.<br> 2. Coloca el cursor sobre la opción Más herramientas y, a continuación, haz clic en Borrar datos de navegación.<br> 3. En el
                            cuadro “Borrar datos de navegación”, haz clic en las casillas de verificación de las opciones Imágenes y archivos almacenados en caché y<br> Cookies y otros datos de sitios y complementos. Usa el menú en la parte
                            superior para seleccionar la cantidad de datos que deseas borrar.<br> 4. Selecciona el origen de los tiempos para borrar todo.<br> 5. Haz clic en Borrar datos de navegación.<br>
                        </p><br>

                        <p> <b>En Mozilla Firefox</b><br> 1. Haz clic en el botón de menú, elige la opción Historial y, por último, Limpiar el historial reciente.<br> 2. Establece el Intervalo de tiempo a borrar a Todo.<br> 3. 3. Haz clic en la
                            flecha que está al lado de Detalles para expandir la lista de elementos del historial.<br> 4. Selecciona Cookies y asegúrate de que no estén seleccionados otros elementos que desees mantener.
                        </p><br>

                        <p> <b>En Internet Explorer</b><br> 1. En Internet Explorer, selecciona el botón Herramientas , ve a Seguridad y selecciona Eliminar el historial de exploración. 2. Activa la casilla Cookies y datos del sitio web y selecciona
                            Eliminar
                        </p><br>

                    </div>
                </div>
            </div>

            <div class="accordion-item">
                <div class="accordion-item-header">
                    ¿Como navegar de manera incognita?
                </div>
                <div class="accordion-item-body">
                    <div class="accordion-item-body-content">
                        <p>Para navegar de manera incógnita en 8cocos.com deberás activar el modo de navegación incógnita dentro de tu navegador de preferencia. Sin embargo, para poder realizar operaciones dentro de 8cocos.com deberás haber ingresado
                            con tus datos de usuario.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="section_manuales">
            <h2 style="text-align: center;">Legal</h2>

            <div class="accordion-item">
                <div class="accordion-item-header">
                    Terminos y Condiciones
                </div>
                <div class="accordion-item-body">
                    <div class="accordion-item-body-content">
                        <p>Revisa nuestros T&C <a target="_blank" href="Imagenes/adjunto/terminos_condiciones.pdf"> aquí.</a> </p><br>
                    </div>
                </div>
            </div>

            <div class="accordion-item">
                <div class="accordion-item-header">
                    Politicas de Privacidad
                </div>
                <div class="accordion-item-body">
                    <div class="accordion-item-body-content">
                        <p>Conoce nuestra política de privacidad <a target="_blank" href="Imagenes/adjunto/politica_privacidad.pdf"> aquí.</p>
                    </div>
                </div>
            </div>                           
        </div>
    </div>
</div> 
<script src="8cocos/dist/js/accordion.js" type="text/javascript "></script>  
@stop
