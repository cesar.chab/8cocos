<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddActivityAssistancesSpecialityEmployeesProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('assistances_speciality_employees_projects', function(Blueprint $table){
            $table->unsignedBigInteger('activity_id')->nullable();
            //$table->foreign('activity_id')->references('id')->on('projects_activities_specialities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('assistances_speciality_employees_projects', function (Blueprint $table) {
           // $table->dropForeign(['activity_id']);
            $table->dropColumn(['activity_id']);
        });
    }
}
