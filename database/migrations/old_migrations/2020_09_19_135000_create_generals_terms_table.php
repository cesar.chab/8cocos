<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGeneralsTermsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('generals_terms', function (Blueprint $table) {
            $table->id();
            $table->integer('credit_check_flag')->default(0);
            $table->integer('base_amount')->default(100);
            $table->integer('calc_discount_on_lines_flag')->default(0);
            $table->boolean('enabled')->default(true);
            $table->integer('partial_discount_flag')->default(1);
            $table->string('name');
            $table->string('description')->nullable();
            $table->integer('prepayment_flag')->default(0);
            $table->integer('created_by');
            $table->integer('last_updated_by');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('generals_terms');
    }
}
