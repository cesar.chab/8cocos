<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMaterialsMaterialsReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('materials_materials_reservations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('material_id');
            $table->text('comment')->nullable();
            $table->string('status_reservation')->default('Pending');
            $table->string('demand_source_name', 30)->nullable();
            $table->integer('demand_source_header_id')->nullable();
            $table->integer('demand_source_line_id')->nullable();
            $table->string('primary_uom_code', 10);
            $table->integer('uom_id');
            $table->float('reservation_quantity', 10, 5);
            $table->float('primary_reservation_quantity', 10, 5);
            $table->integer('supply_source_type_id')->nullable();
            $table->integer('supply_source_header_id')->nullable();
            $table->integer('supply_source_line_id')->nullable();
            $table->integer('supply_source_line_detail')->nullable();
            $table->string('subinventory_code', 10)->nullable();
            $table->integer('subinventory_id')->nullable();
            $table->string('lot_number', 80)->nullable();
            $table->unsignedBigInteger('material_movement_id');
            $table->unsignedBigInteger('reservation_header_id');
            $table->timestamps();
            $table->softDeletes();


            
            $table->foreign('material_id')->references('id')->on('materials_master_materials');
            $table->foreign('material_movement_id')->references('id')->on('materials_materials_movements');
            $table->foreign('reservation_header_id')->references('id')->on('materials_reservations_header');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('materials_materials_reservations');
    }
}
