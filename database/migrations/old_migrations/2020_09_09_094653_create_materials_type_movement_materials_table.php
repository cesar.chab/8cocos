<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMaterialsTypeMovementMaterialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('materials_type_movement_materials', function (Blueprint $table) {
            $table->id();
            $table->string('name')->unique();
            $table->string('use_system')->default('N');
            $table->string('code')->unique();
            $table->string('description')->nullable();
            $table->string('stock');
            $table->boolean('status');
            $table->integer('last_correlative')->default(0);
            $table->integer('flag_system');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('materials_type_movement_materials');
    }
}
