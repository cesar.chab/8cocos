<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdministrativeIncomesExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('administrative_incomes_expenses', function (Blueprint $table) {
            $table->id();
            $table->date('movement_date');
            $table->unsignedBigInteger('project_id');
            $table->unsignedBigInteger('reason_id');
            $table->float('amount', 10,2);
            $table->text('detail');
            $table->timestamps();


            $table->foreign('project_id')->references('id')->on('projects_project_registrations');
            $table->foreign('reason_id')->references('id')->on('general_reasons');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('administrative_incomes_expenses');
    }
}
