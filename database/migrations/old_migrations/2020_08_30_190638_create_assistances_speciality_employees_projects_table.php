<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssistancesSpecialityEmployeesProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assistances_speciality_employees_projects', function (Blueprint $table) {
           $table->unsignedBigInteger("assistance_id");
           $table->unsignedBigInteger("project_id");
           $table->unsignedBigInteger("speciality_id");
           $table->unsignedBigInteger("employee_id");

           $table->foreign("assistance_id")->references("id")->on("assistances_projects");
           $table->foreign("project_id")->references("id")->on("projects_project_registrations");
           $table->foreign("speciality_id")->references("id")->on("general_specialties");
           $table->foreign("employee_id")->references("id")->on("general_employees");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assistances_speciality_employees_projects');
    }
}
