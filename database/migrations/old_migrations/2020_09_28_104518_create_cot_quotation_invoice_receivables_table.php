<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCotQuotationInvoiceReceivablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cot_quotation_invoice_receivables', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('quotation_id');
            $table->unsignedBigInteger('type_doc');
            $table->string('number_invoice')->unique();
            $table->decimal('invoice_amount', 10, 2);
            $table->date('date_issue');
            $table->enum('invoice_status', ['ingresado', 'enviado', 'pagada']);
            $table->timestamps();
            $table->softDeletes(); 

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cot_quotation_invoice_receivables');
    }
}
