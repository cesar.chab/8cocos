<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTypeToGeneralDirectoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('general_directories', function (Blueprint $table) {
            $table->string('type')->default('P')->after('parent')->comment('add type for directory, P -> Proyect, C -> Cotizacion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('general_directories', function (Blueprint $table) {
            $table->dropColumn(['type']);
        });
    }
}
