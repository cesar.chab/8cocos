<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMaterialsOnhandQuantitiesDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('materials_onhand_quantities_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('material_id');
            $table->unsignedBigInteger('project_id');
            $table->date('date_received');
            $table->decimal('primary_transaction_quantity', 10, 5);
            $table->unsignedBigInteger('movement_id');
            $table->unsignedBigInteger('uom_id');
            $table->decimal('quantity', 10, 5);
            $table->timestamps();
            $table->softDeletes();


            $table->foreign('material_id')->references('id')->on('materials_master_materials');
            $table->foreign('project_id')->references('id')->on('projects_project_registrations');
            $table->foreign('movement_id')->references('id')->on('materials_materials_movements');
            $table->foreign('uom_id')->references('id')->on('general_units_measurements');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('materials_onhand_quantities_details');
    }
}
