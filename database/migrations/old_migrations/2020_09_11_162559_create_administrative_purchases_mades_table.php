<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdministrativePurchasesMadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('administrative_purchases_mades', function (Blueprint $table) {
            $table->id();
            $table->date('date');
            $table->string('serie');
            $table->string('number');
            $table->unsignedBigInteger('provider_id');
            $table->decimal('amount', 10,2);
            $table->decimal('IGV', 10,2);
            $table->boolean('declaved');
            $table->timestamps();


            $table->foreign('provider_id')->references('id')->on('general_providers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('administrative_purchases_mades');
    }
}
