<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsProjectRegistrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects_project_registrations', function (Blueprint $table) {
            $table->id();
            $table->string('name', 200)->unique();
            $table->unsignedBigInteger('customer_id');
            $table->string('number_mobile')->nullable();
            $table->date('date_contract');
            $table->unsignedBigInteger('employee_id');
            $table->unsignedBigInteger('priority_id');
            $table->unsignedBigInteger('state_id');
            $table->unsignedBigInteger('type_project_id');
            $table->date('delivery_date');
            $table->float('total_project_cost', 8, 2);
            $table->timestamps();
            $table->softDeletes();


            $table->foreign('customer_id')->references('id')->on('general_customers');
            $table->foreign('employee_id')->references('id')->on('general_employees');
            $table->foreign('priority_id')->references('id')->on('general_fnd_lookups_values');
            $table->foreign('state_id')->references('id')->on('general_fnd_lookups_values');
            $table->foreign('type_project_id')->references('id')->on('general_fnd_lookups_values');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects_project_registrations');
    }
}
