<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGeneralProvidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('general_providers', function (Blueprint $table) {
            $table->id();
            $table->string('provider_name', 200);
            $table->unsignedBigInteger('type_document_id');
            $table->string('number_document', 50)->unique();
            $table->string('address', 200);
            $table->string('phones', 50);
            $table->string('email');
            $table->boolean('status');
            $table->timestamps();
            $table->softDeletes();

            
            $table->foreign('type_document_id')->references('id')->on('general_type_documents');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('general_providers');
    }
}
