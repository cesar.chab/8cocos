<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsActivitiesSpecialitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects_activities_specialities', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->text('description');
            $table->boolean('status');
            $table->unsignedBigInteger('speciality_id');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('speciality_id')->references('id')->on('general_specialties');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects_activities_specialities');
    }
}
