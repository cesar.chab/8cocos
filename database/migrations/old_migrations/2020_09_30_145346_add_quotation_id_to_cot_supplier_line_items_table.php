<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddQuotationIdToCotSupplierLineItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cot_supplier_line_items', function (Blueprint $table) {
            $table->integer('quotation_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cot_supplier_line_items', function (Blueprint $table) {
            $table->dropColumn(['quotation_id']);
        });
    }
}
