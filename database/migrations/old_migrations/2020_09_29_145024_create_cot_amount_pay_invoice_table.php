<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCotAmountPayInvoiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cot_amount_pay_invoice', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('invoice_receivable_id');
            $table->unsignedBigInteger('pay_invoice_header_id');
            $table->decimal('amount_pay', 10, 2);
            $table->timestamps();
            $table->softDeletes();


            $table->foreign('invoice_receivable_id')->references('id')->on('cot_quotation_invoice_receivables');
            $table->foreign('pay_invoice_header_id')->references('id')->on('cot_pay_invoice_header');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cot_amount_pay_invoice');
    }
}
