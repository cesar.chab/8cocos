<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdministrativeProjectsPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('administrative_projects_payments', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('project_id');
            $table->date('payment_date');
            $table->decimal('amount', 10, 2);
            $table->string('number_constancy');
            $table->unsignedBigInteger('responsable_id');
            $table->string('comment');
            $table->unsignedBigInteger('type_payment_id');
            $table->unsignedBigInteger('type_voucher_id');
            $table->string('voucher_number');
            $table->string('path')->nullable();
            $table->string('file_constancy')->nullable();
            $table->timestamps();
            $table->softDeletes();



            $table->foreign('project_id')->references('id')->on('projects_project_registrations');
            $table->foreign('responsable_id')->references('id')->on('general_employees');
            $table->foreign('type_payment_id')->references('id')->on('general_fnd_lookups_values');
            $table->foreign('type_voucher_id')->references('id')->on('general_fnd_lookups_values');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('administrative_projects_payments');
    }
}
