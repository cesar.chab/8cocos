<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdministrativeFixedOutputsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('administrative_fixed_outputs', function (Blueprint $table) {
            $table->id();
            $table->enum('type_expenses', ['office', 'work']);
            $table->unsignedBigInteger('expenses_id');
            $table->string('amount');
            $table->date('date');
            $table->timestamps();
            $table->softDeletes();


            $table->foreign('expenses_id')->references('id')->on('general_expenses_fixeds_concept');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('administrative_fixed_outputs');
    }
}
