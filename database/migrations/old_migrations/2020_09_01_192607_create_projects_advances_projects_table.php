<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsAdvancesProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects_advances_projects', function (Blueprint $table) {
            $table->id();
            $table->string("percentage");
            $table->unsignedBigInteger("pivot_id");
            //$table->unsignedBigInteger("projects_project_specialities");
           //$table->unsignedBigInteger("speciality_id");
          // $table->unsignedBigInteger("employee_id")->nullable();
            $table->timestamps();
            $table->softDeletes();


            $table->foreign("pivot_id")->references("id")->on("projects_project_specialities");


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects_advance_project');
    }
}
