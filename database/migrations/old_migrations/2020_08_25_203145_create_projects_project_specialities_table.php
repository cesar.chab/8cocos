<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsProjectSpecialitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects_project_specialities', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('speciality_id');
            $table->unsignedBigInteger('employee_id');
            $table->boolean('status')->default(true);
            $table->unsignedBigInteger('project_id');
            $table->longText('comments');
            $table->timestamps();
            $table->softDeletes();


            $table->foreign('speciality_id')->references('id')->on('general_specialties');
            $table->foreign('employee_id')->references('id')->on('general_employees');
            $table->foreign('project_id')->references('id')->on('projects_project_registrations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects_project_specialities');
    }
}
