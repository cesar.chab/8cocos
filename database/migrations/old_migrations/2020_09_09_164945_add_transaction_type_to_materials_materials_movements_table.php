<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTransactionTypeToMaterialsMaterialsMovementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('materials_materials_movements', function (Blueprint $table) {
            $table->unsignedBigInteger('type_movement_id');
            $table->unsignedBigInteger('employee_id');


            $table->foreign('type_movement_id')->references('id')->on('materials_type_movement_materials');
            $table->foreign('employee_id')->references('id')->on('general_employees');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('materials_materials_movements', function (Blueprint $table) {
            $table->dropForeign(['type_movement_id', 'employee_id']);
            $table->dropColumn(['type_movement_id', 'employee_id']);

        });
    }
}
