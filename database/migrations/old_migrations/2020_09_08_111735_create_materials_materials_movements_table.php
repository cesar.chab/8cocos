<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMaterialsMaterialsMovementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('materials_materials_movements', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('material_id');
            $table->unsignedBigInteger('project_id');
            $table->decimal('quantity', 10, 5);
            $table->string('transaction_uom');
            $table->decimal('primary_quantity', 10, 5)->default(0);
            $table->decimal('transaction_price', 10, 5)->default(0);
            $table->unsignedBigInteger('uom_id');
            $table->date('transaction_date');
            $table->string('source_code')->default('MISC');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('material_id')->references('id')->on('materials_master_materials');
            $table->foreign('project_id')->references('id')->on('projects_project_registrations');
            $table->foreign('uom_id')->references('id')->on('general_units_measurements');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('materials_materials_movements');
    }
}
