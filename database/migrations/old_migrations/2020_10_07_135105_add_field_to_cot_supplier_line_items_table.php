<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldToCotSupplierLineItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cot_supplier_line_items', function (Blueprint $table) {
            $table->unsignedInteger('quotation_line_id')->nullable()->comment('campo para relacionar quotation_line con esta tabla y poder calcular');
            $table->unsignedInteger('shape_payment_id')->nullable()->comment('campo forma de pago nuevo agregado');
            $table->float('unit_price')->nullable()->comment('precio unitario');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cot_supplier_line_items', function (Blueprint $table) {
            $table->dropColumn(['quotation_line_id','shape_payment_id','unit_price']);
        });
    }
}
