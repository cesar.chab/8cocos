<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCotSupplierCategoryItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cot_supplier_category_items', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->integer('item');
            $table->unsignedBigInteger('header_id');
            $table->timestamps();
            $table->softDeletes();


            $table->foreign('header_id')->references('id')->on('cot_supplier_header_items');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cot_supplier_category_items');
    }
}
