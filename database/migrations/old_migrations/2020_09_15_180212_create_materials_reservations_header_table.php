<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMaterialsReservationsHeaderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('materials_reservations_header', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('project_id');
            $table->integer('destination_project_id');
            $table->text('comment');
            $table->unsignedBigInteger('employee_id');
            $table->string('status_reservation')->default('Pending');
            $table->date('date_reservation');
            $table->date('shipping_date')->nullable();
            $table->date('arrival_date')->nullable();
            $table->integer('type_movement_id')->nullable();
            $table->timestamps();
            $table->softDeletes();


            $table->foreign('project_id')->references('id')->on('projects_project_registrations');
            $table->foreign('employee_id')->references('id')->on('general_employees');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('materials_reservations_header');
    }
}
