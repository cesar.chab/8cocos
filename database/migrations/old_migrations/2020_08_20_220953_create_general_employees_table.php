<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGeneralEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('general_employees', function (Blueprint $table) {
            $table->id();
            $table->string('code')->unique();
            $table->string('name', 50);
            $table->string('last_name', 50);
            $table->unsignedBigInteger('position_id');
            $table->string('apel_name')->comment('field name and last_name');
            $table->boolean('responsable')->comment('field is responsable employee in  any project');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('position_id')->references('id')->on('general_positions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('general_employees');
    }
}
