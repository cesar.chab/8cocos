<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMaterialsMasterMaterialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('materials_master_materials', function (Blueprint $table) {
            $table->id();
            $table->string('name', 200);
            $table->float('unit_cost', 8, 2);
            $table->unsignedBigInteger('unit_measurement_id');
            $table->timestamps();
            $table->softDeletes();


            $table->foreign('unit_measurement_id')->references('id')->on('general_units_measurements');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('materials_master_materials');
    }
}
