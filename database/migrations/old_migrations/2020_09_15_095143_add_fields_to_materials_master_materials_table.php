<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToMaterialsMasterMaterialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('materials_master_materials', function (Blueprint $table) {
            $table->unsignedBigInteger('state_material_id');
            $table->unsignedBigInteger('type_material_id');

            $table->foreign('state_material_id')->references('id')->on('general_fnd_lookups_values');
            $table->foreign('type_material_id')->references('id')->on('general_fnd_lookups_values');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('materials_master_materials', function (Blueprint $table) {
            $table->dropForeign(['type_material_id','state_material_id']);
            $table->dropColumn(['type_material_id','state_material_id']);
        });
    }
}
