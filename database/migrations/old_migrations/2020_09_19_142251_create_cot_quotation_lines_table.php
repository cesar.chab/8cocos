<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCotQuotationLinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cot_quotation_lines', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('header_id');
            $table->string('description')->nullable();
            $table->float('item')->default(0);
            $table->string('uom');
            $table->float('quantity')->default(0);
            $table->float('unit_price')->default(0);
            $table->string('status_line');
            $table->timestamps();

            $table->foreign('header_id')->references('id')->on('cot_quotation_headers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cot_quotation_lines');
    }
}
