<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGeneralFndLookupsValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('general_fnd_lookups_values', function (Blueprint $table) {
            $table->id();
            $table->string('name', 25)->unique();
            $table->unsignedBigInteger('fnd_lookup_id');
            $table->foreign('fnd_lookup_id')->references('id')->on('general_fnd_lookups')->onDelete('cascade');
            $table->string('description', 50);
            $table->boolean('status')->default(true);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('general_fnd_lookups_values');
    }
}
