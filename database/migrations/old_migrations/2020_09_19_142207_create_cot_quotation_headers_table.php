<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCotQuotationHeadersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cot_quotation_headers', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('customer_id');
            $table->unsignedBigInteger('project_id');
            $table->date('date_quotation');
            $table->integer('version');
            $table->integer('source_quotation_id')->nullable();
            $table->unsignedBigInteger('status_quotation_id');
            $table->unsignedBigInteger('terms_id');
            $table->string('notes')->nullable();
            $table->string('reference')->nullable();
            $table->string('customer_to');
            $table->string('terms_note');
            $table->float('value_percent_expenses')->default(0.00);
            $table->float('value_utility')->default(0.00);

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('customer_id')->references('id')->on('general_customers');
            $table->foreign('project_id')->references('id')->on('projects_project_registrations');
            $table->foreign('status_quotation_id')->references('id')->on('general_fnd_lookups_values');
            $table->foreign('terms_id')->references('id')->on('generals_terms');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cot_quotation_headers');
    }
}
