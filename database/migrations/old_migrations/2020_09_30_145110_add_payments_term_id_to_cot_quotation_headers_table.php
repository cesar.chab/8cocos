<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPaymentsTermIdToCotQuotationHeadersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cot_quotation_headers', function (Blueprint $table) {
            $table->integer('payment_term_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cot_quotation_headers', function (Blueprint $table) {
            $table->dropColumn(['payment_term_id']);
        });
    }
}
