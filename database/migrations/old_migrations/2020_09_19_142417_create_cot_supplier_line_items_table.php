<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCotSupplierLineItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cot_supplier_line_items', function (Blueprint $table) {
            $table->id();
            $table->integer('category_id')->nullable();
            $table->integer('item');
            $table->string('description');
            $table->string('uom');
            $table->float('meters');
            $table->unsignedBigInteger('provider_id');
            $table->float('percent_over_utility');
            $table->float('price_vendor');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('provider_id')->references('id')->on('general_providers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cot_supplier_line_items');
    }
}
