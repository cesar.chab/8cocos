<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCotSupplierHeaderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cot_supplier_header_items', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('phase');
            $table->string('building');
            $table->string('departament');
            $table->unsignedBigInteger('line_id');
            $table->timestamps();
            $table->softDeletes();


            $table->foreign('line_id')->references('id')->on('cot_quotation_lines');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cot_supplier_header_items');
    }
}
