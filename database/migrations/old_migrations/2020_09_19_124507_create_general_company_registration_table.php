<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGeneralCompanyRegistrationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('general_company_registration', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('bill_to_name', 150)->nullable();
            $table->string('number_identification', 150)->nullable();
            $table->string('address', 50)->nullable();
            $table->string('city', 50)->nullable();
            $table->string('code_postal', 50)->nullable();
            $table->string('email', 50)->nullable();
            $table->string('currency_code', 3)->nullable();
            $table->decimal('tax_value')->nullable();
            $table->string('desc_long_currency', 50)->nullable();
            $table->string('invoice_number', 25)->nullable();
            $table->string('invoice_serial', 25)->nullable();
            $table->string('invoice_last_number', 25)->nullable();
            $table->string('invoice_bol_number', 25)->nullable();
            $table->string('invoice_bol_serial', 25)->nullable();
            $table->string('invoice_bol_last_number', 25)->nullable();
            $table->string('item_include_tax', 1)->default('Y');
            $table->string('path_image_bg', 250)->nullable();
            $table->string('path_image_logo', 250)->nullable();
            $table->string('longitud', 15)->nullable();
            $table->string('telephone', 50)->nullable();
            $table->tinyInteger('condition')->default(1);
            $table->string('description')->nullable();
            $table->integer('last_updated_by');  
            $table->integer('created_by');   
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('general_company_registration');
    }
}
