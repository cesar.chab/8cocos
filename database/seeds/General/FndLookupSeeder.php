<?php

use Illuminate\Database\Seeder;
use App\Entities\General\{FndLookup, FndLookupValue};
use \Illuminate\Support\Facades\Schema;


class FndLookupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        FndLookupValue::truncate();
        FndLookup::truncate();
        
        $fndlookup=FndLookup::create([
            'name'=>'ESTADO_PROYECTO',
            'description'=>'Para manejar estados',
            'status'=>1
        ]);

                FndLookupValue::create([
                    'code'=>'EP',
                    'name'=>'EP',
                    'description'=>'En Proceso',
                    'status'=>1,
                    'fnd_lookup_id'=>$fndlookup->id
                ]);

                FndLookupValue::create([
                    'code'=>'FIN',
                    'name'=>'FIN',
                    'description'=>'Finalizado',
                    'status'=>1,
                    'fnd_lookup_id'=>$fndlookup->id
                ]);

                FndLookupValue::create([
                    'code'=>'TE',
                    'name'=>'Te',
                    'description'=>'Terminado',
                    'status'=>1,
                    'fnd_lookup_id'=>$fndlookup->id
                ]);


        $fndlookup=FndLookup::create([
            'name'=>'PRIORIDAD_PROYECTO',
            'description'=>'Para manejar prioridades',
            'status'=>1
        ]);

                FndLookupValue::create([
                    'code'=>'ALTO',
                    'name'=>'ALTO',
                    'description'=>'Alto',
                    'status'=>1,
                    'fnd_lookup_id'=>$fndlookup->id
                ]);

                FndLookupValue::create([
                    'code'=>'MEDIO',
                    'name'=>'MEDIO',
                    'description'=>'Medio',
                    'status'=>1,
                    'fnd_lookup_id'=>$fndlookup->id
                ]);

                FndLookupValue::create([
                    'code'=>'BAJO',
                    'name'=>'BAJO',
                    'description'=>'Bajo',
                    'status'=>1,
                    'fnd_lookup_id'=>$fndlookup->id
                ]);

        $fndlookup=FndLookup::create([
            'name'=>'TIPO_PROYECTO',
            'description'=>'Para manejar por tipo',
            'status'=>1
        ]);

                FndLookupValue::create([
                    'code'=>'CONS',
                    'name'=>'CONS',
                    'description'=>'Consultoria',
                    'status'=>1,
                    'fnd_lookup_id'=>$fndlookup->id
                ]);

                FndLookupValue::create([
                    'code'=>'OBRA',
                    'name'=>'OBRA',
                    'description'=>'Obra',
                    'status'=>1,
                    'fnd_lookup_id'=>$fndlookup->id
                ]);

                FndLookupValue::create([
                    'code'=>'SER',
                    'name'=>'SER',
                    'description'=>'Servicio',
                    'status'=>1,
                    'fnd_lookup_id'=>$fndlookup->id
                ]);


                $fndlookup=FndLookup::create([
                    'name'=>'CC_METODO_PAGO',
                    'description'=>'Lista metodo de pago',
                    'status'=>1
                ]);
        
                        FndLookupValue::create([
                            'code'=>'COD',
                            'name'=>'Tarjeta de Credito',
                            'description'=>'Pago con tarjeta de credito',
                            'status'=>1,
                            'fnd_lookup_id'=>$fndlookup->id
                        ]);
        
                        FndLookupValue::create([
                            'code'=>'EFE',
                            'name'=>'Efectivo',
                            'description'=>'Pago con efectivo',
                            'status'=>1,
                            'fnd_lookup_id'=>$fndlookup->id
                        ]);
        
                        FndLookupValue::create([
                            'code'=>'TRANS',
                            'name'=>'Transferencia',
                            'description'=>'Pago por transferencia bancaria',
                            'status'=>1,
                            'fnd_lookup_id'=>$fndlookup->id
                        ]);
        

                $fndlookup=FndLookup::create([
                    'name'=>'CC_TIPO_COMPROBANTE',
                    'description'=>'Lista de tipos de comprobantes',
                    'status'=>1
                        ]);
                
                                FndLookupValue::create([
                                    'code'=>'FAC',
                                    'name'=>'Factura',
                                    'description'=>'Factura',
                                    'status'=>1,
                                    'fnd_lookup_id'=>$fndlookup->id
                                ]);
                
                                FndLookupValue::create([
                                    'code'=>'BOL',
                                    'name'=>'Boleta',
                                    'description'=>'Boleta',
                                    'status'=>1,
                                    'fnd_lookup_id'=>$fndlookup->id
                                ]);
                
                                FndLookupValue::create([
                                    'code'=>'REC',
                                    'name'=>'Recibo',
                                    'description'=>'Recibo por Honorario',
                                    'status'=>1,
                                    'fnd_lookup_id'=>$fndlookup->id
                                ]);


                                $fndlookup=FndLookup::create([
                                    'name'=>'ESTADO_MATERIAL',
                                    'description'=>'Lista de estados de material',
                                    'status'=>1
                                        ]);
                                
                                                FndLookupValue::create([
                                                    'code'=>'NUE',
                                                    'name'=>'Nuevo',
                                                    'description'=>'Nuevo',
                                                    'status'=>1,
                                                    'fnd_lookup_id'=>$fndlookup->id
                                                ]);
                                
                                                FndLookupValue::create([
                                                    'code'=>'USA',
                                                    'name'=>'Usado',
                                                    'description'=>'Usado',
                                                    'status'=>1,
                                                    'fnd_lookup_id'=>$fndlookup->id
                                                ]);
                                
                                                FndLookupValue::create([
                                                    'code'=>'FAL',
                                                    'name'=>'Fallas',
                                                    'description'=>'Fallas',
                                                    'status'=>1,
                                                    'fnd_lookup_id'=>$fndlookup->id
                                                ]);



                                                $fndlookup=FndLookup::create([
                                                    'name'=>'ESTADO_COTIZACION',
                                                    'description'=>'Lista de estados de material',
                                                    'status'=>1
                                                        ]);
                                                
                                                                FndLookupValue::create([
                                                                    'code'=>'DRAFT',
                                                                    'name'=>'Borrado',
                                                                    'description'=>'Borrado',
                                                                    'status'=>1,
                                                                    'fnd_lookup_id'=>$fndlookup->id
                                                                ]);
                                                
                                                                FndLookupValue::create([
                                                                    'code'=>'APRO',
                                                                    'name'=>'Aprobado',
                                                                    'description'=>'Aprobado',
                                                                    'status'=>1,
                                                                    'fnd_lookup_id'=>$fndlookup->id
                                                                ]);
                                                
                                                                FndLookupValue::create([
                                                                    'code'=>'EJEC',
                                                                    'name'=>'En Ejecucion',
                                                                    'description'=>'En Ejecucion',
                                                                    'status'=>1,
                                                                    'fnd_lookup_id'=>$fndlookup->id
                                                                ]);

                                                                
                                                $fndlookup=FndLookup::create([
                                                    'name'=>'TIPO_MATERIAL',
                                                    'description'=>'Lista de Tipo de material',
                                                    'status'=>1
                                                        ]);
                                                
                                                                FndLookupValue::create([
                                                                    'code'=>'AGR',
                                                                    'name'=>'Material Agregado',
                                                                    'description'=>'Material Agregado',
                                                                    'status'=>1,
                                                                    'fnd_lookup_id'=>$fndlookup->id
                                                                ]);
                                                
                                                                FndLookupValue::create([
                                                                    'code'=>'COM',
                                                                    'name'=>'Combustible',
                                                                    'description'=>'Combustible',
                                                                    'status'=>1,
                                                                    'fnd_lookup_id'=>$fndlookup->id
                                                                ]);
                                                
                                                                FndLookupValue::create([
                                                                    'code'=>'HER',
                                                                    'name'=>'Herramientas',
                                                                    'description'=>'Herramientas',
                                                                    'status'=>1,
                                                                    'fnd_lookup_id'=>$fndlookup->id
                                                                ]);
                
                                                                

                Schema::enableForeignKeyConstraints();


    }
}