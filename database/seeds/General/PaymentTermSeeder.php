<?php

use Illuminate\Database\Seeder;
use App\Entities\General\PaymentTerm;

class PaymentTermSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\Schema::disableForeignKeyConstraints();

        PaymentTerm::truncate();

        PaymentTerm::create([
            'name' => '15 Dias',
            'time_days' => '15',
            'status' => true
        ]);

        PaymentTerm::create([
            'name' => '30 Dias',
            'time_days' => '30',
            'status' => true
        ]);

        PaymentTerm::create([
            'name' => '60 Dias',
            'time_days' => '60',
            'status' => true
        ]);

        PaymentTerm::create([
            'name' => '120 Dias',
            'time_days' => '120',
            'status' => true
        ]);

        \Illuminate\Support\Facades\Schema::enableForeignKeyConstraints();

    }
}
