<?php

use Illuminate\Database\Seeder;
use App\Entities\General\Customer;
use \Illuminate\Support\Facades\Schema;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();

        Customer::truncate();

        Customer::create([
            'business_name' => 'Sin uso',
            'type_document_id' => 1,
            'number_document' => '9999',
            'address' => 'S/R',
            'phones' => '000000000',
            'email' => 'sinmail@app.com',
            'status' => 0
        ]);

        Customer::create([
            'business_name' => 'Daka',
            'type_document_id' => 1,
            'number_document' => '7070',
            'address' => 'Av. San Luis, calle piso, casa 14. Valencia',
            'phones' => '234345456',
            'email' => 'daka@gmail.com',
            'status' => 1
        ]);

        Customer::create([
            'business_name' => 'Flores SA',
            'type_document_id' => 1,
            'number_document' => '7075',
            'address' => 'Av. San Luis, calle piso, casa 15. Valencia',
            'phones' => '234345000',
            'email' => 'Flores@gmail.com',
            'status' => 1
        ]);

        Customer::create([
            'business_name' => 'Peloteros SA',
            'type_document_id' => 1,
            'number_document' => '7670',
            'address' => 'Av. Don Visctorio, calle piso, casa 14. Valencia',
            'phones' => '238905456',
            'email' => 'Peloteros@gmail.com',
            'status' => 1
        ]);
        Schema::enableForeignKeyConstraints();
    }
}
