<?php

use Illuminate\Database\Seeder;
use App\Entities\General\Speciality;
use \Illuminate\Support\Facades\Schema;

class SpecialitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        Speciality::truncate();

        Speciality::create([
           'name'   => 'Generica',
           'status' => 1
        ]);

        Speciality::create([
            'name' => 'Arquitectura',
            'status' => 1
        ]);

        Speciality::create([
            'name' => 'Estructuras',
            'status' => 1
        ]);

        Speciality::create([
            'name' => 'Sanitarias',
            'status' => 1
        ]);

        Speciality::create([
            'name' => 'Electricas',
            'status' => 1
        ]);

        Speciality::create([
            'name' => 'Gas',
            'status' => 1
        ]);

        Speciality::create([
            'name' => 'Aire Condicionado',
            'status' => 1
        ]);

        Schema::enableForeignKeyConstraints();
    }
}
