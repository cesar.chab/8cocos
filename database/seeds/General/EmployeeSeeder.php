<?php

use Illuminate\Database\Seeder;
use App\Entities\General\Employee;
use \Illuminate\Support\Facades\Schema;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        Employee::truncate();
        Employee::create([
            'code' => 'AYPN',
            'name' => 'Adriana Yosneth',
            'last_name' => 'Perez Nuñez',
            'position_id' => 1,
            'apel_name' => 'Adriana Yosneth Perez Nuñez',
            'responsable' => TRUE
        ]);

        Employee::create([
            'code' => 'JCMM',
            'name' => 'Juan Carlos',
            'last_name' => 'Machado Machado',
            'position_id' => 1,
            'apel_name' => 'Juan Carlos Machado Machado',
            'responsable' => TRUE
        ]);

        Employee::create([
            'code' => 'LENM',
            'name' => 'Lucinda Estilita',
            'last_name' => 'Nuñez Machado',
            'position_id' => 1,
            'apel_name' => 'Lucinda Estilita Nuñez Machado',
            'responsable' => FALSE
        ]);

        Employee::create([
            'code' => 'PUTA',
            'name' => 'Paulina Urty',
            'last_name' => 'Torrealba Agilar',
            'position_id' => 1,
            'apel_name' => 'Paulina Urty Torrealba Agilar',
            'responsable' => TRUE
        ]);

        Employee::create([
            'code' => 'MACO',
            'name' => 'Marcos Antonio',
            'last_name' => 'Castillos Ortiz',
            'position_id' => 1,
            'apel_name' => 'Marcos Antonio Castillos Ortiz',
            'responsable' => FALSE
        ]);

        Employee::create([
            'code' => 'YYHM',
            'name' => 'Yenny Yoselin',
            'last_name' => 'Hernandez Machado',
            'position_id' => 1,
            'apel_name' => 'Yenny Yoselin Hernandez Machado',
            'responsable' => TRUE
        ]);

        Schema::enableForeignKeyConstraints();
    }
}
