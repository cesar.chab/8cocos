<?php

use Illuminate\Database\Seeder;
use App\Entities\General\TypeDocument;
use \Illuminate\Support\Facades\Schema;

class TypeDocumentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        TypeDocument::truncate();
        
        TypeDocument::create([
            'name'=>'DNI',
            'status'=>1
        ]);

        TypeDocument::create([
            'name'=>'Pasaporte',
            'status'=>1
        ]);

        TypeDocument::create([
            'name'=>'Carnet de Extranjeria',
            'status'=>1
        ]);

        TypeDocument::create([
            'name'=>'RUC',
            'status'=>1
        ]);

        Schema::enableForeignKeyConstraints();
    }
}
