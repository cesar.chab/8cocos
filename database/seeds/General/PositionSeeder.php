<?php

use Illuminate\Database\Seeder;
use App\Entities\General\Position;
use \Illuminate\Support\Facades\Schema;

class PositionSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        Position::truncate();
        
        Position::create([
            'name' => 'Encargado',
            'status' => 1
        ]);

        Position::create([
            'name' => 'Administrativo',
            'status' => 1
        ]);

        Position::create([
            'name' => 'Colaborador',
            'status' => 1
        ]);

        Position::create([
            'name' => 'Ing. Civil',
            'status' => 1
        ]);

        Position::create([
            'name' => 'Arquitecta',
            'status' => 1
        ]);

        Schema::enableForeignKeyConstraints();

    }
}
