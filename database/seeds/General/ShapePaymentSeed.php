<?php

use Illuminate\Database\Seeder;
use App\Entities\General\ShapePayment;
use Illuminate\Support\Facades\Schema;

class ShapePaymentSeed extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
         ShapePayment::truncate();

         ShapePayment::create([
                'name' => 'Factura',
                'description' => 'Pago por factura',
                'percentage' => '18'
         ]);

        ShapePayment::create([
            'name' => 'Recibo Honorario',
            'description' => 'Pago por Recibos honorarios',
            'percentage' => '1'
        ]);

        Schema::enableForeignKeyConstraints();
    }
}
