<?php

use Illuminate\Database\Seeder;

class TermSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\Schema::disableForeignKeyConstraints();

        \App\Entities\General\Term::truncate();

        \App\Entities\General\Term::create([
                'credit_check_flag' => 0,
                'base_amount' => 0,
                'calc_discount_on_lines_flag' => 0,
                'enabled' => 1,
                'partial_discount_flag' => 1,
                'name' => 'Termino 1',
                'description' => 'Termino 1',
                'prepayment_flag' => 0,
                'created_by' => 1,
                'last_updated_by' => 1
        ]);

        \Illuminate\Support\Facades\Schema::enableForeignKeyConstraints();


    }
}
