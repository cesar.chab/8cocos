<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class ClearQuotationSeed extends Seeder
{
    /**
     * Run the database seeds.
     *  Clear quotation and childs relationship tables
     * @return void
     */
    public function run()
    {
            Schema::disableForeignKeyConstraints();

                \App\Entities\Quotations\SupplierLineItem::truncate();
                \App\Entities\Quotations\SupplierCategoryItem::truncate();
                \App\Entities\Quotations\SupplierHeaderItem::truncate();
                \App\Entities\Quotations\QuotationLine::truncate();
                \App\Entities\Quotations\QuotationHeader::truncate();

            Schema::enableForeignKeyConstraints();
    }
}
