<?php

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\Schema;
use App\Entities\Materials\TypeMovementMaterial;

class TypeMovementMaterialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();

        TypeMovementMaterial::truncate();

        TypeMovementMaterial::create([
                'name' => 'A_INGRE INV',
                'use_system' => '0',
                'code' => 'AII',
                'description' => 'Ingreso por Inventario',
                'stock' => 'A',
                'status' => true,
                'last_correlative' => 0,
                'flag_system' => 0,
                'created_at' => date('Y-m-d h:m:s')
        ]);

        TypeMovementMaterial::create([
            'name' => 'E_DEVOLUCION CLIENTE',
            'use_system' => '0',
            'code' => 'EDC',
            'description' => 'a',
            'stock' => 'D',
            'status' => true,
            'last_correlative' => 0,
            'flag_system' => 0,
            'created_at' => date('Y-m-d h:m:s')
        ]);

        TypeMovementMaterial::create([
            'name' => 'F_PRESTAMO',
            'use_system' => '0',
            'code' => 'FPR',
            'description' => 'a',
            'stock' => 'D',
            'status' => true,
            'last_correlative' => 0,
            'flag_system' => 0,
            'created_at' => date('Y-m-d h:m:s')
        ]);

        TypeMovementMaterial::create([
            'name' => 'G _ TRANS',
            'use_system' => '0',
            'code' => 'GTR',
            'description' => 'a',
            'stock' => 'D',
            'status' => true,
            'last_correlative' => 0,
            'flag_system' => 0,
            'created_at' => date('Y-m-d h:m:s')
        ]);

        TypeMovementMaterial::create([
            'name' => 'H _ DEVOLUION CAMPO_ DESUSO',
            'use_system' => '0',
            'code' => 'DCD',
            'description' => 'a',
            'stock' => 'A',
            'status' => true,
            'last_correlative' => 0,
            'flag_system' => 0,
            'created_at' => date('Y-m-d h:m:s')
        ]);

        TypeMovementMaterial::create([
            'name' => 'I _ BAJA',
            'use_system' => '0',
            'code' => 'IBAJ',
            'description' => 'a',
            'stock' => 'D',
            'status' => true,
            'last_correlative' => 0,
            'flag_system' => 0,
            'created_at' => date('Y-m-d h:m:s')
        ]);

        TypeMovementMaterial::create([
            'name' => 'B _ INGRE COM',
            'use_system' => '0',
            'code' => 'BIC',
            'description' => 'Ingreso por Orden de Compra',
            'stock' => 'A',
            'status' => true,
            'last_correlative' => 0,
            'flag_system' => 0,
            'created_at' => date('Y-m-d h:m:s')
        ]);

        TypeMovementMaterial::create([
            'name' => 'C _ SALIDA CAMPO',
            'use_system' => '0',
            'code' => 'CSC',
            'description' => 'Salida para Taller',
            'stock' => 'D',
            'status' => true,
            'last_correlative' => 0,
            'flag_system' => 0,
            'created_at' => date('Y-m-d h:m:s')
        ]);

        TypeMovementMaterial::create([
            'name' => 'D _ DEVOLUION_TALLER',
            'use_system' => '0',
            'code' => 'DDC',
            'description' => 'Devolucion de taller',
            'stock' => 'A',
            'status' => true,
            'last_correlative' => 0,
            'flag_system' => 0,
            'created_at' => date('Y-m-d h:m:s')
        ]);

        TypeMovementMaterial::create([
            'name' => 'J_COMPRA DIRECTA',
            'use_system' => '0',
            'code' => 'JCD',
            'description' => 'a',
            'stock' => 'A',
            'status' => true,
            'last_correlative' => 0,
            'flag_system' => 0,
            'created_at' => date('Y-m-d h:m:s')
        ]);

        TypeMovementMaterial::create([
            'name' => 'K_PEDIDO MATERIAL',
            'use_system' => '0',
            'code' => 'PMA',
            'description' => 'a',
            'stock' => 'A',
            'status' => true,
            'last_correlative' => 0,
            'flag_system' => 0,
            'created_at' => date('Y-m-d h:m:s')
        ]);

        TypeMovementMaterial::create([
            'name' => 'L_APROBACION PEDIDO',
            'use_system' => '0',
            'code' => 'APR',
            'description' => 'a',
            'stock' => 'A',
            'status' => true,
            'last_correlative' => 0,
            'flag_system' => 0,
            'created_at' => date('Y-m-d h:m:s')
        ]);

        TypeMovementMaterial::create([
            'name' => 'Venta',
            'use_system' => '0',
            'code' => 'GUI',
            'description' => 'a',
            'stock' => 'D',
            'status' => true,
            'last_correlative' => 0,
            'flag_system' => 0,
            'created_at' => date('Y-m-d h:m:s')
        ]);

        TypeMovementMaterial::create([
            'name' => 'N_DESPACHO MATERIAL',
            'use_system' => '0',
            'code' => 'DES',
            'description' => 'a',
            'stock' => 'D',
            'status' => true,
            'last_correlative' => 0,
            'flag_system' => 0,
            'created_at' => date('Y-m-d h:m:s')
        ]);

        TypeMovementMaterial::create([
            'name' => 'CSA_CORECC STOCK AUM',
            'use_system' => '0',
            'code' => 'CSA',
            'description' => 'CORRECCIÓN DE STOCK',
            'stock' => 'A',
            'status' => true,
            'last_correlative' => 0,
            'flag_system' => 0,
            'created_at' => date('Y-m-d h:m:s')
        ]);

        TypeMovementMaterial::create([
            'name' => 'CSD_CORRECC STOCK DISM',
            'use_system' => '0',
            'code' => 'CSD',
            'description' => 'CORRECCIÓN DE STOCK PARA DISMINUIR',
            'stock' => 'D',
            'status' => true,
            'last_correlative' => 0,
            'flag_system' => 0,
            'created_at' => date('Y-m-d h:m:s')
        ]);

        TypeMovementMaterial::create([
            'name' => 'SALIDA VARIAS',
            'use_system' => '1',
            'code' => 'SMISC',
            'description' => 'Salida Varias',
            'stock' => 'D',
            'status' => true,
            'last_correlative' => 0,
            'flag_system' => 0,
            'created_at' => date('Y-m-d h:m:s')
        ]);

        TypeMovementMaterial::create([
            'name' => 'Despacho OT',
            'use_system' => 'Y',
            'code' => 'Despacho OT',
            'description' => 'Despacho OT',
            'stock' => 'D',
            'status' => true,
            'last_correlative' => 0,
            'flag_system' => 0,
            'created_at' => date('Y-m-d h:m:s')
        ]);


        TypeMovementMaterial::create([
            'name' => 'MOVE_PROJECT',
            'use_system' => 'Y',
            'code' => 'MO_PRO',
            'description' => 'MOVE_PROJECT',
            'stock' => 'D',
            'status' => true,
            'last_correlative' => 0,
            'flag_system' => 0,
            'created_at' => date('Y-m-d h:m:s')
        ]);

        TypeMovementMaterial::create([
            'name' => 'RECEIPT_PROJECT',
            'use_system' => 'Y',
            'code' => 'RECEIPT',
            'description' => 'RECEIPT_PROJECT',
            'stock' => 'A',
            'status' => true,
            'last_correlative' => 0,
            'flag_system' => 0,
            'created_at' => date('Y-m-d h:m:s')
        ]);

        Schema::enableForeignKeyConstraints();

    }
}
