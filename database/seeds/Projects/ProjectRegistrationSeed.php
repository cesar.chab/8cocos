<?php

use Illuminate\Database\Seeder;
use \Illuminate\Support\Facades\Schema;

class ProjectRegistrationSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();

        \App\Entities\Projects\ProjectRegistration::truncate();

        $project = \App\Entities\Projects\ProjectRegistration::create([
                'name' => 'Jornada Inicial',
                'customer_id' => 1,
                'number_mobile' => '0000000',
                'date_contract' => '2020/01/01',
                'employee_id' => 1,
                'priority_id' => 1,
                'state_id' => 2,
                'type_project_id' => 9,
                'delivery_date' => '2020/01/01',
                'total_project_cost' => 0
        ]);

        \App\Entities\Projects\ProjectSpeciality::create([
            'speciality_id' => 1,
            'employee_id' => 1,
            'status'    => false,
            'project_id' => $project->id,
            'comments' => "registro para generar marcacion de jornada inicial",
            'directory_id' => 0
        ]);



        $project_two =  \App\Entities\Projects\ProjectRegistration::create([
            'name' => 'Jornada Final',
            'customer_id' => 1,
            'number_mobile' => '0000000',
            'date_contract' => '2020/01/01',
            'employee_id' => 1,
            'priority_id' => 1,
            'state_id' => 2,
            'type_project_id' => 9,
            'delivery_date' => '2020/01/01',
            'total_project_cost' => 0
        ]);

        \App\Entities\Projects\ProjectSpeciality::create([
            'speciality_id' => 1,
            'employee_id' => 1,
            'status'    => false,
            'project_id' => $project_two->id,
            'comments' => "registro para generar marcacion de jornada final",
            'directory_id' => 0
        ]);




    }
}
