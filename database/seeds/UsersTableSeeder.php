<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::truncate();

       $user = \App\User::create([
            'name' => 'gustavo',
            'email' => 'gustavoh.2312@gmail.com',
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
            'employee_id' => 1
        ]);

       $user->syncRoles('Administrador');

        $user = \App\User::create([
            'name' => 'julio',
            'email' => 'julio@app.com',
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
            'employee_id' => null
        ]);

        $user->syncRoles('Administrador');

        $user = \App\User::create([
            'name' => 'dairy',
            'email' => 'dairysanabria.90@gmail.com',
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
            'employee_id' => null
        ]);

        $user->syncRoles('Administrador');

        $user = \App\User::create([
            'name' => 'usuario',
            'email' => 'prueba@gmail.com',
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
            'employee_id' => null
        ]);

        $user->syncRoles('Usuario');
    }
}
