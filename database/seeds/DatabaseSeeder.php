<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(RoleSeeder::class);
         $this->call(UsersTableSeeder::class);
         $this->call(PositionSeeder::class);
         $this->call(TypeDocumentSeeder::class);
         $this->call(SpecialitySeeder::class);
         $this->call(FndLookupSeeder::class);
         $this->call(EmployeeSeeder::class);
         $this->call(CustomerSeeder::class);

         $this->call(TypeMovementMaterialSeeder::class);
         $this->call(ProjectRegistrationSeed::class);

         $this->call(PaymentTermSeeder::class);
         $this->call(TermSeeder::class);
         $this->call(ShapePaymentSeed::class);
    }
}
