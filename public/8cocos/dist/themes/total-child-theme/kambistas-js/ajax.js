jQuery.fn.ForceNumericOnly = function() { return this.each(function() { jQuery(this).keydown(function(e) { var key = e.charCode || e.keyCode || 0; return (key == 8 || key == 9 || key == 46 || key == 110 || key == 190 || (key >= 37 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105)); }); }); };
jQuery(document).ready(function() {
    calcular('S')
    jQuery('#change').click(function() {
        var soles = 'Soles'
        var dolares = 'Dólares'
        jQuery('#change').animate({ deg: '360' }, { duration: 400, step: function(now) { jQuery('#change').css({ transform: 'rotate(' + now + 'deg)' }) } })
        jQuery('#change').animate({ deg: '0' }, { duration: 0, step: function(now) { jQuery('#change').css({ transform: 'rotate(' + now + 'deg)' }) } })
        var ini = jQuery('#inicio').attr('cambio')
        if (ini == 'usd') {
            jQuery('#inicio').attr('cambio', 'pen')
            jQuery('#inicio span').html(soles).animate({ opacity: '0.4' }, 'slow').animate({ opacity: '1' }, 'slow')
        } else {
            jQuery('#inicio').attr('cambio', 'usd')
            jQuery('#inicio span').html(dolares).animate({ opacity: '0.4' }, 'slow').animate({ opacity: '1' }, 'slow')
        }
        var final = jQuery('#final').attr('cambio')
        if (final == 'usd') {
            jQuery('#final').attr('cambio', 'pen')
            jQuery('#final span').html(soles).animate({ opacity: '0.4' }, 'slow').animate({ opacity: '1' }, 'slow')
        } else {
            jQuery('#final').attr('cambio', 'usd')
            jQuery('#final span').html(dolares).animate({ opacity: '0.4' }, 'slow').animate({ opacity: '1' }, 'slow')
        }
        calcular('S')
    })
    jQuery('#valini').keyup(function() { calcular('S') })
    jQuery('#valfin').keyup(function() { calcular('R') })

    function calcular(tipo) {
        var exeso = 0
        if (tipo == 'S') { if (jQuery('#inicio').attr('cambio') == 'usd') { if (parseInt(jQuery('#valini').val()) > 10000) { var exeso = 1 } } else if (jQuery('#inicio').attr('cambio') == 'pen') { if (parseInt(jQuery('#valini').val()) > 30000) { var exeso = 1 } } }
        if (tipo == 'R') { if (jQuery('#final').attr('cambio') == 'usd') { if (parseInt(jQuery('#valfin').val()) > 10000) { var exeso = 1 } } else if (jQuery('#final').attr('cambio') == 'pen') { if (parseInt(jQuery('#valfin').val()) > 30000) { var exeso = 1 } } }
        jQuery("#accionarbotonexeso").tooltip({ placement: 'top', delay: { "show": 100, "hide": 100 }, offset: '0 10', html: true, trigger: 'hover' });
        if (exeso > 0) {
            jQuery('#accionarbotonexeso').attr('src', '../../../8cocos/imagenes//imgs/campana.svg')
            jQuery('#accionarbotonexeso').tooltip('hide')
            jQuery('#accionarbotonexeso').tooltip("show")
            setTimeout(function() { jQuery('#accionarbotonexeso').tooltip('hide') }, 3000);
        } else {
            jQuery('#accionarbotonexeso').attr('src', '../../../8cocos/imagenes/svg/masinfo.svg')
            jQuery('#accionarbotonexeso').tooltip('hide')
        }
        if (tipo == 'S') {
            var valorini = jQuery('#valini')
            var valorfin = jQuery('#valfin')
            var amount = valorini.val()
            var monedaini = jQuery('#inicio').attr('cambio')
            var monedafin = jQuery('#final').attr('cambio')
        } else {
            var valorini = jQuery('#valfin')
            var valorfin = jQuery('#valini')
            var monedaini = jQuery('#inicio').attr('cambio')
            var monedafin = jQuery('#final').attr('cambio')
            var amount = valorini.val()
        }
        var monedaini = monedaini ? monedaini.toUpperCase() : ''
        var monedafin = monedafin ? monedafin.toUpperCase() : ''
        if (amount == null || amount == '' || amount < 1) {
            valorfin.val('0')
            jQuery('#montoahorradofinal').html(0)
        } else {
            jQuery.ajax({
                type: 'GET',
                data: { originCurrency: monedaini, destinationCurrency: monedafin, amount: parseInt(amount), active: tipo },
                url: 'https://api.kambista.com/v1/exchange/calculates',
                success: function(data) {
                    var resultadofi = data.exchange
                    valorfin.val(resultadofi)
                    if (monedafin == 'USD') { var ahorro = '$ ' + data.savings.amount } else if (monedafin == 'PEN') { var ahorro = 'S/. ' + data.savings.amount }
                    jQuery('#montoahorradofinal').html(ahorro, 4)
                    jQuery('#valcompra').html(data.tc.bid)
                    jQuery('#valventa').html(data.tc.ask)
                }
            })
        }
    }
    jQuery("#valfin").ForceNumericOnly()
    jQuery("#valini").ForceNumericOnly()

    function round(num, decimales = 2) {
        var signo = num >= 0 ? 1 : -1
        num = num * signo
        if (decimales === 0) return signo * Math.round(num)
        num = num.toString().split('e')
        num = Math.round(+(num[0] + 'e' + (num[1] ? +num[1] + decimales : decimales)))
        num = num.toString().split('e')
        return signo * (num[0] + 'e' + (num[1] ? +num[1] - decimales : -decimales))
    }
})